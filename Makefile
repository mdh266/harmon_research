
clean: 
	find . -name \*.pdf -type f -delete
	find . -name \*.aux -type f -delete
	find . -name \*.log -type f -delete
	find . -name \*.toc -type f -delete
	find . -name \*.dvi -type f -delete
	find . -name \*.bbl -type f -delete
	find . -name \*.gz -type f -delete
	find . -name \*.out -type f -delete
	find . -name \*.nav -type f -delete
	find . -name \*.snm -type f -delete
	find . -name \*.ps -type f -delete
