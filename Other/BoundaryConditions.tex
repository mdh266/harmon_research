\documentclass[10pt]{report}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fullpage}
\usepackage{graphicx}
\numberwithin{equation}{section}

\begin{document}


\chapter{Boundary Conditions}

\noindent
Boundary conditions can be broken into three categories:


\begin{enumerate}
\item Metal Contacts
\item Oxide Layers
\item Semiconductor-electrolyte interface
\end{enumerate}


\noindent
Metal contacts are formed when at the junction of metal and semiconductor and can be grouped into two sub-categories: Ohmic and Shottky.  Ohmic contacts are non-rectifying, meaning they allow for currents to flow in both directions.   Shottky contacts are rectifying, allowing more current flow in one direction than the other.  In addition, Ohmic contacts have a linear I-V characteristics, while Shottky have non-linear characteristics.  This is shown below:

\vspace{3mm}

\begin{center}
%\includegraphics[scale=0.5]{./images/IV.png}
\end{center}

\noindent
The physics of how currents and potentials vary across each boundary is very much dependent upon which contact is used.



\vspace{5mm}



\section{Semiconductor-Metal: Ohmic Contacts}

\subsection{Physics Of Ohmic Contacts}

\subsubsection{Equalibrium}

\noindent
Ohmic contacts are essential for modern electronics and integrated circuits since they provide low resistance currents in both directions. There are two ways to form Ohmic contacts


\begin{enumerate}
\item Choosing a metal with an appropriate work function.
\item Highly doping the semiconductor.
\end{enumerate}

\noindent
The first case leads to currents caused by diffusion, while the later case leads to currents by quantum mechanical tunneling.  While the later case is more common due to the lack of metals with necessary work functions, the former is much easier to understand and will be described next.


\vspace{3mm}


\noindent
In the case of a Metal-n-type semiconductor junction,  in order to form an Ohmic contact, the work function of the metal ($\phi_{M}$) is greater than that of the semiconductor ($\phi_{S}$).  This displayed below,

\vspace{3mm}


\begin{center}
%\includegraphics[scale=0.8]{./images/ohmic_before.png}
\end{center}

% from http://what-when-how.com/electronic-properties-of-materials/semiconductors-electrical-properties-of-materials-part-3/


\vspace{3mm}


\noindent
It is important to note that the Fermi energy of the metal is higher than that of the semiconductor and the bottom of the semiconductors conduction band.  When the two materials are brought together the highly mobile electrons from the metal diffuse onto the semiconductor until the Fermi levels of the materials coincide.  At this point the materials are in thermodynamic equilibrium which implies 


$$ \nabla E_{F} = 0$$


\noindent
Now our junction looks like:


\vspace{3mm}

\begin{center}
%\includegraphics[scale=0.65]{./images/ohmic_after.png}
\end{center}
% from http://www.stanford.edu/class/ee311/NOTES/Ohmic_Contacts.pdf


\vspace{3mm}


\noindent
The things to note are that the conduction band of the semiconductor now lies below
the Fermi level.  This means that we have a build up of electrons  (called an \textbf{accumulation region}) on the semiconductor side the contact due to the diffusion of electrons from the metal.  This build up of electrons on the side of the semiconductor induces a mirror image of positive charge on the metal side of the junction.


\begin{center}
%\includegraphics[scale=0.8]{./images/accumulation.png}
\end{center}


\noindent
This is due to the highly mobile electrons in the metal that can easily "screen out" the charge carriers at the boundary of the metal and semiconductor.  This screening mathematically implies that 


$$ \nabla \cdot E(x,t) = 0   \qquad \forall x \in \partial \Omega_{Ohmic} $$


\noindent
The key thing to note is that in equalibrium our electron density is fixed on the semiconductor side of the Ohmic contact.  


\vspace{5mm}


\subsubsection{Non-Equilibrium}
Applying a bias our device drives it out of equilibrium and splits the Fermi levels of the materials.  In either case the act of applying a basis causes the diffusion of majority carriers into or out of the semiconductor.  Forward biasing the device means that we lower the quasi Fermi level with respect to the metal's quasi Fermi level allowing electrons from the metal to diffuse onto the semiconductor.  This results in an extension of the accumulation further into the semiconductor, where the extra electrons diffuse from the metal into the semiconductor. Reverse biasing means we raise the quasi Fermi level of the semiconductor with respect to the metals and electrons diffuse into the metal from the semiconductor resulting in a smaller accumulation region.


\vspace{5mm}


\subsection{Ohmic Boundary Conditions On Charge Carriers}


\noindent
Since the metal and semiconductors are in thermodynamic equilibrium we know that the  of law mass is valid:


\begin{equation} \label{eq:lawofmass}
 n(x) p(x) = n_{i}^{2}   \qquad \forall x \in \partial \Omega_{\text{Ohmic}} 
\end{equation}


\noindent
Where $\partial \Omega_{\text{Ohmic}}$ is portion of the boundary of the device with an Ohmic contact.  



\vspace{3mm}


\noindent
From Poisson's equation in the semiconductor we know that 


\vspace{1mm}


$$ -\nabla \cdot E(x,t) = q \, \left( \, C(x) - (n(x,t)-p(x,t)) \, \right) $$

\vspace{1mm}


\noindent
Using the fact the the left hand side of the equation is zero from screening (due to highly mobile electrons in the metal), we have boundary condition for the charge carriers at Ohmic contacts known as the \textbf{charge neutrality requirement}:


\vspace{1mm}


\begin{equation}\label{eq:chargeneutrality}
C(x) - (n(x,t)-p(x,t)) = 0
\end{equation}


\vspace{1mm}


 
\noindent
Substituting \eqref{eq:lawofmass} into \eqref{eq:chargeneutrality}   we have the equations:

\vspace{1mm}

$$ n^{2} - C(x)n -n_{i}^{2} = 0,  \qquad x \in \partial \Omega_{Ohmic}$$

$$ p^{2} + C(x)n -n_{i}^{2} = 0, \qquad x \in \partial \Omega_{Ohmic}$$

\vspace{1mm}

\noindent
Solving these equations gives us the Dirichlet boundary conditions:

\begin{equation}
n(x,t ) = \frac{1}{2} \left( C(x) + \sqrt{C(x)+4n_{i}^{2} } \right), \qquad x \in \partial \Omega_{Ohmic}
\end{equation}


\begin{equation}
p(x,t ) = \frac{1}{2} \left(-C(x) + \sqrt{C(x)+4n_{i}^{2} } \right), \qquad x \in \partial \Omega_{Ohmic}
\end{equation}


\vspace{5mm}



\subsection{Ohmic Boundary Conditions For The Potential}

\noindent
We model Ohmic Boundary Conditions as Dirichlet conditions on the potential, chosing (arbitrarily) the first contact to be grounded:


$$ \Phi(x,t) = 0, \qquad \forall x \, \in \partial \Omega_{Ohmic}^{1} $$


$$\Phi(x,t) = \Phi_{\text{bi}} + \Phi_{\text{applied}}, \qquad \forall x \, \in \partial \Omega_{Ohmic}^{2} $$


\noindent
Where $\Phi_{\text{applied}} > 0$ is forward biasing and $\Phi_{\text{applied}} < 0$ is reverse biasing, and $\Phi_{\text{bi}}$ is the potential built into our device.

\vspace{5mm}


\section{Semiconductor Metal: Shottky Contacts}

\vspace{3mm}

\subsection{Physics of Shottky Contacts}

\subsubsection{Equalibrium}


\noindent
Shottky contacts are a metal-semiconductor junction where there is a non-trivial resistance to the current flow. This resistance is due to the \textbf{Shottky barrier} which impedes charge transport carriers into the device, and a \textbf{contact potential} which impedes the transport of the opposite carrier out of the device. Also characteristic to Shottky contacts is that Instead a \textbf{depletion region} formed in the semiconductor near the junction that will play an important role in the formation of the boundary conditions for the charge carriers.


\vspace{3mm}


\noindent
In the case of a n-type semiconductor, a Shottky contact is formed when the work function of the semiconductor is less than the work function of the metal:


\begin{center}
%\includegraphics[scale=0.65]{./images/shottky_before.png}
\end{center}
% Steelman


\noindent 
Since the Fermi level of the semiconductor is higher than that the metal, after the two materials are brought together electrons will flow from the semiconductor to the metal.  As the electrons move onto the metal they leave behind their ionized cores which will make up the semiconductors depletion region.  In this region the semiconductor has been depleted of its free carriers and has only bound positive charges below:


\begin{center}
%\includegraphics[scale=0.65]{./images/depletion.png}
\end{center}
% steelman


\noindent
Electrons flow from the semiconductor onto the metal until thermodynamic equalibrium is achieved, at which point their Fermi levels match.  The band bending at the junction is displayed below:

\begin{center}
%%\includegraphics[scale=0.65]{./images/shottky_after.png}
\end{center}
% steelman

\noindent
It is essential to note that we now have two potentials formed at the contact:


\begin{enumerate}
\item The contact potential (built in potential), $V_{0}$ or $\Phi_{bi}$, which is the difference in the materials work functions and is built into the device.
\item The Shottky barrier $\Phi_{B}$, which is the difference between the metal's work function and the semiconductor's electron affinity.
\end{enumerate}


\noindent
The contact potential inhibits the diffusion of electrons from the semiconductor to the metal, while the Shottky barrier inhibits the diffusion of electrons from the metal to the semiconductor.


\vspace{5mm}


\subsubsection{Non-equalibrium: Biasing }

\noindent
There main ways to get drive a current in across a Shottky contact are:


When we apply a bias, we drive the device out of equalibrium and split the Fermi levels.  When we forward bias our device, we raise the quasi Fermi level of semiconductor with respect to the metal:


\begin{center}
%\includegraphics[scale=0.35]{./images/shottky_forward.png}
\end{center}


\noindent
This in effect lowers the contact potential and allows for the diffusion of electrons from the semiconductor to the metal.  Reverse biasing the device has the effect of lowering quasi Fermi level of the semiconductor to with respect to the increasing the contact potential.  The increased contact potential prevents any diffusion of electrons from the semiconductor to the metal.


\vspace{3mm}

\begin{center}
%\includegraphics[scale=0.35]{./images/shottky_reverse.png}
\end{center}

\vspace{3mm}


\noindent
It is important to see that no amount of biasing will reduce the Shottky barrier, hence no electrons can diffuse from the metal onto the semiconductor.  This property is known as a rectifying effect.

\vspace{5mm}


\subsection{Shottky Boundary Conditions For Charge Carriers}


\noindent
Shottky contacts have the property that they have a non-trivial parasitic resistance.  We can model this resistance as surface recombination and derive boundary conditions for the carriers using the diagram:


\begin{center}
%\includegraphics[scale=0.45]{./images/shottkybanddiagram.png}
\end{center}


\noindent
At $x = 0$ we have the density of electrons as:


$$n = N_{C} e^{(E_{C}-E_{F})/k_{B}T} $$ 


\noindent
With no applied bias ($V = 0$), then density of electrons for $(0 < x < W)$ is the equilibrium density:


$$ n_{e} = N_{C} e^{(E_{C}-E_{F} - q\Phi_{0})/k_{B}T}$$ 


\noindent
The contact potential $\Phi_{0}$ exponentially decreases the number of free electrons.  When we apply a bias we effectively lower the contact potential and thereby increase the density of electrons:


\begin{align*}
n_{\text{applied}} &= N_{C}e^{(E_{C}-E_{F} - q(\Phi_{0}-V)/k_{B}T} \\ 
&= e^{-qV/k_{B}T} \, n_{e}
\end{align*}



\noindent
Therefore injected electrons into the depletion region will be:


\begin{align*}
\delta n &= n_{\text{applied}} - n_{e} \\
&= n_{e} \, (e^{-qV/k_{B}T} - 1) 
\end{align*}



\noindent
Upon being injected into the depletion region, carriers will diffuse towards the metal.  Along the way electrons can can be trapped or recombine with ionized cores.   The average density of excess electrons given at $ 0 \leq x \leq W$ given an injection $\delta n$ at $x = 0$ is:


$$ n(x) = \delta n \, e^{-x/L_{n}}$$



\noindent
Given that the current is solely diffusive we can write:


$$J_{n}(x) = - D_{n} \frac{dn}{dx}$$


\noindent
Which at $x = W$ gives us:



\begin{align}
J_{n} &= \frac{D_{n}}{L_{n}} e^{-qW/k_{B}T} \delta n \\
&= v_{n} (n-n_{e})
\end{align}


\noindent
Where $v_{n}$ is called the \textbf{surface recombination velocity}, which is the ``velocity" of the electrons diffusing across to the metal.


\vspace{3mm}




\noindent
Written in each charge carriers form we have:

\begin{equation}
\hat{\nu} \cdot \mathbf{J}_{n}(x,t) = \, v_{n} \, (n - n_{e})(x,t) \vspace{2mm},  \qquad x \in \partial \Omega_{Shottky} 
\end{equation}

\begin{equation}
\hat{\nu} \cdot \mathbf{J}_{p}(x,t) = \, v_{p} \, (p - p_{e})(x,t),  \qquad x \in \partial \Omega_{Shottky} 
\end{equation}



\vspace{3mm}


\subsection{Shottky Boundary Conditions For The Potential}


\noindent
We model Shottky boundary conditions as Dirichlet conditions on the potential, chosing (arbitrarily) the first contact to be grounded:


$$ \Phi(x,t) = 0, \qquad \forall x \, \in \partial \Omega_{Shottkey}^{1} $$


$$\Phi(x,t) = \Phi_{\text{bi}} - \Phi_{\text{Shottky}} + \Phi_{\text{applied}}, \qquad \forall x \, \in \partial \Omega_{Shottkey}^{2} $$


\noindent
Where $\Phi_{\text{Shottky}}$ is the barrier height and$\Phi_{\text{bi}}$ is the potential built into our device.
$\Phi_{\text{applied}} > 0$ implies forward biasing and $\Phi_{\text{applied}} < 0$ implies reverse biasing.  


\vspace{5mm}


\section{Semiconductor-Oxide }


\subsection{Physics of Oxide: Insulation}
Oxide-layers are ubiquitous semiconductor devices.  The success of Silicon in CMOS technology is not so much due to its abundant supply, but rather its naturally occuring oxide: Silicon-Dioxide.  Oxide layers offer a wide band gap insulator and applied around the boundaries of devices where there the semiconductor is not meant to contact metal or another substrate as show in the diagram of MOSFET below:

\begin{center}
%\includegraphics[scale=0.35]{./images/mosfet.png}
\end{center}

%http://www.doitpoms.ac.uk/tlplib/semiconductors/images/mosfet.jpg


\noindent
An oxide layer, with its wide band gap implies that very few (if any) free carriers can diffuse into this region.  Moreover, because of the wide band gap it would take an extremely large electric field (bias) to move an electron into the conduction band of the oxide layer.  Hence the purpose of the oxide layer is to prevent the movement of free carriers into another domain.  This is often done around the boundaries of semiconductor device such to reduce the effects of surface recombination.  Furthermore, in the case of silicon, a silicon-oxide interface is much less defective than free silicon surface.  For much the same reason, thin oxide layer is often added around the metal contacts of a device to reduce recombination because a silicon-metal interface is much more defective than a silicon-silicon dioxide 
%Physics of solar cells

\subsection{Oxide Boundary Conditions On Charge Carriers}


\noindent
The insulating nature of an oxide layer is meant to prevent the movement of charge carriers across the semiconductor-oxide interface.  Therefore, the boundary conditions are insulating on the current:


\begin{equation}
\textbf{J}(x,t) \cdot \boldsymbol \nu = 0 \qquad \forall x \in \partial \Omega_{\text{oxide}}
\end{equation}


\noindent
Furthermore as will be shown in the next section the effect of the oxide layer on the electric field requires that the interface we have


$$ \textbf{E}(x,t) \cdot \boldsymbol \nu = 0 0 \qquad \forall x \in \partial \Omega_{\text{oxide}}$$


\noindent
This forces the boundary conditions in addition to the above we have that:


\begin{equation}
D \nabla u
\cdot \boldsymbol\nu = 0 \qquad \forall x \in \partial \Omega_{\text{oxide}}
\end{equation}


\subsection{Oxide Boundary Conditions For The Potential}


\vspace{5mm}


\section{Semiconductor-Electrolyte Interface}


\subsection{Physics Of The Semiconductor-Electrolyte Interface}


\noindent
A semiconductor-electrolyte interface is used to drive \textbf{reduction-oxidation (redox) processes}.  Since we only focus on just one interface we are actually only dealing with \textbf{half reactions}.  There are two types of half reactions:  \textbf{reduction and oxidation.}  \textbf{Reduction} is the process where electrons are gained by a substance.  The object that catalyzes this process is called a \textbf{reductant}  An example is:


\vspace{2mm}

\noindent
\textbf{Reduction Process}

$$F_{2} \, + \, 2e^{-}  \quad \rightarrow  \quad 2F^{-}$$

\vspace{2mm}

\noindent
\textbf{Oxidation} is the process where a substance gives up an electron.  The object that catalyzes this process is called a \textbf{oxidant}.  An example is below:

\vspace{2mm}

\noindent
\textbf{Oxidation Process}


$$ H_{2}  \quad \rightarrow \quad 2H^{+} \, + \, 2e^{-} $$


\noindent
When we immerse a $n-$type semiconductor into an electrolyte filled with redox pairs where the reductant $\rho_{R}$ is $OH^{-}$ and the oxidant $\rho_{O}$ is $H^{+}$ there will be a transfer of electrons from the semiconductor to the electrolyte or vice versa. 



\vspace{2mm}


\noindent
If the Fermi-level of the electrolyte $E_{F}^{\text{redox}}$ is less than that of the semiconductor $E_{F}^{\text{sc}}$ then electrons will flow (diffuse) from the semiconductor to the electrolyte leaving behind ionized cores forming a depletion region inside the semiconductor.  This process will continue until enough of a contact potential is built up in the semiconductor which opposes the diffusion of electrons from the semiconductor the electrolyte.  At this point Fermi levels of the two materials align.  This is show below:



\begin{center}
%\includegraphics[scale=0.65]{images/semi-electrolytebandstructure.png} 
\end{center}
%http://www.porous-35.com/electrochemistry-semiconductors-8.html


\noindent
If we are to forward bias the device by an amount $V_{A} > 0$ then we would raise the Fermi level of the semiconductor with respect to the electrolyte Fermi level, ($E_{F}^{\text{sc}} \,  > \, E_{F}^{\text{redox}}$).  This will cause the flow of electrons from the semiconductor into the electrolyte.  This is called the \textbf{forward process.}


\vspace{3mm}


\noindent
If the semiconductor is illuminated electron-hole pairs will be created in the semiconductor.  If the EHP are within or diffuse into the depletion region they are separated by the built in electric field.  The electron is forced away from the interface while the hole is forced towards the interface.  The presence of a hole at the interface will cause the transfer of an electron from the electrolyte onto the semiconductor.  This is called the \textbf{backward process.}


\vspace{3mm}


\noindent
The actual mechanism by which electrons are transferred is by means of tunneling and will be described mathematically in the next section.  It is important to note that in any real device whatever process is occurring at the semiconductor-electrolyte interface\textit{ must have the reverse process occur at the opposite electrode.} However, we will only concentrate on the reactions at the semiconductor-electrolyte interface and assume we are far from the counter electrode.  The schematics of the domain and the forward/backward processes is shown below:


\begin{center}
%\includegraphics[scale=0.5]{images/reactionsinterface.png} 
\end{center}
% Bard's book


\vspace{5mm}




\subsection{Interface Boundary Conditions For Charge Carriers}

\noindent
At the interface we have an interaction of electrons ($n$) or holes $(p)$ at the interface and the specifically absorbed ions (redox pairs) which is either a reductant ($\rho_{r}, \; OH^{2}$) or oxidants $(\rho_{o}, \ H^{+})$.    This is depicted below:



\begin{center}
%\includegraphics[scale=0.65]{images/interfacehelmholtzlayer.png} 
\end{center}
%Roel Van de Krol lecture notes



\vspace{3mm}


\noindent
If we are to forward bias the device by an amount $V_{A} > 0$ split the Fermi in the substances giving rise to the quasi-Fermi levels $E_{F_{n}}$, $E_{F_{p}}$ in the semiconductor and $E_{F_{\text{red}}}$, $E_{F_{\text{ox}}}$ in the electrolyte. 
Electrons will flow from the semiconductor to the electrolyte if there are oxidants at the available energy level; meaning the electron quasi-Fermi level must be higher than the energy of the oxidants.  This gives rise to the \textbf{forward process}.  Electrons can also move form the electrolyte to the semiconductor if the energy of the reductant is higher than the valance band and their a hole it can recombine with.  This is the \textbf{reverse process}   These processes are depicted below:


\begin{center}
%\includegraphics[scale=0.4]{./images/interfacebanddiagram.png}
\end{center}




\noindent
Now it can be seen that flow of electrons from the semiconductor to the electrolyte depends on the number of oxidants that can absorb this extra electron.


\vspace{2mm}



\noindent
This gives us the \textbf{forward relation for electron current}:


$$ \textbf{J}_{n} \cdot \boldsymbol \eta \, = \, k_{f} \, \rho_{o} $$


\noindent
This can be seen in the reaction:


$$ 4H^{+} + 4e^{-} \quad \rightarrow \quad  2H_{2}$$



\noindent
If the semiconductor is illuminated as described before and a hole is present on the semiconductor side of the interface and a reductant is present in the electrolyte than an electron will tunnel through to the valance band.  This can be see as as the \textbf{backward process for hole current}:


$$\textbf{J}_{p} \cdot \boldsymbol \eta \, = \, k_{b} \, \rho_{r} $$

\noindent
This can be seen in the reaction:


$$ 4OH^{-} \, + \, 4e^{+} \quad \rightarrow \quad 2H_{2} O + O_{2}$$


\vspace{3mm}

\noindent
On the electrolyte side of the interface changes in the reductant is due to the gain from the forward process minus the loss by backward process:

$$
\frac{\partial \rho_{r}}{\partial t} 
\quad = \quad ( \textbf{J}_{n} - \textbf{J}_{p}) \cdot \boldsymbol \eta  \quad = \quad 
k_{f} \, \rho_{o} \, - \, k_{b} \, \rho_{r}
$$


\noindent
And the opposite for the oxidant:


$$
\frac{\partial \rho_{o}}{\partial t} 
\quad = \quad  (\textbf{J}_{p} -\textbf{ J}_{r}) \cdot \boldsymbol \eta \quad = \quad 
k_{b} \, \rho_{r} \, - \, k_{f} \, \rho_{o}
$$


\noindent
The conservation of charge will give us the \textbf{reductant current}:


$$\textbf{J}_{r} \cdot \boldsymbol \eta\quad  = \quad -\frac{\partial \rho_{r}}{\partial t} 
\quad = \quad 
k_{b} \, \rho_{r} \, - \, k_{f} \, \rho_{o}
$$


\noindent
And the \textbf{oxidant current}:

$$\textbf{J}_{o} \cdot \boldsymbol \eta \quad = \quad  -\frac{\partial \rho_{o}}{\partial t} 
\quad = \quad 
-k_{b} \, \rho_{r} \, + \, k_{f} \, \rho_{o}
$$



\vspace{3mm}



\noindent
The forward and backward reaction rates $k_{f}$ and $k_{b}$ can be related to the electron and hole densities by the electron and hole transfer rates $k_{et}$ and $k_{ht}$ through a process similar to deriving the Shottky boundary conditions.  We summarize the results as:



\begin{equation}
k_{f} = k_{et} (n - n_{e}) \qquad \text{and} \qquad k_{b} = k_{ht} (p - p_{e})
\end{equation} 



\noindent
Using these we will arrive at the boundary conditions for electrons and holes:

\begin{align}
\textbf{J}_{n} \cdot \boldsymbol \eta \, &= \, k_{et} (n - n_{e}) \, \rho_{o} \\
\textbf{J}_{p} \cdot \boldsymbol \eta \, &= \, k_{ht} (p - p_{e}) \, \rho_{r} 
\end{align}


\noindent
And the boundary conditions for the reductants and oxidants

\begin{align} 
\textbf{J}_{r} \cdot \boldsymbol \eta \quad  &=
\quad  \, k_{ht} (p - p_{e}) \rho_{r} \, - \, k_{et} (n - n_{e}) \, \rho_{o} \\
\textbf{J}_{o} \cdot \boldsymbol \eta \quad &= \quad 
- k_{ht} (p - p_{e}) \, \rho_{r} \, + \, k_{et} (n - n_{e}) \, \rho_{o}
\end{align}



\subsection{Interface Boundary Conditions For The Potential}

%%
\end{document}
