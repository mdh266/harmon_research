\documentclass[10pt]{report}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fullpage}

\usepackage{amsthm}


\begin{document}

\begin{align}
\partial_{t} u \ - \ \boldsymbol \nabla \cdot ( D(\textbf{x}) \boldsymbol \nabla u) \; &= \; f(\textbf{x},t) &&  \Omega \times (0,T]  \\
(-D \boldsymbol \nabla u ) \cdot \boldsymbol \eta_{N} \; &= \; g(u) && \partial \Omega_{N} \times (0,T]  \\
u \; &= \; u_{D}(\textbf{x}) &&  \partial \Omega_{D} \times (0,T] \\
u \; &= \; u_{I}(\textbf{x}) &&  \Omega \times \left\{ t=0 \right\}
\end{align}


Where $0 < D_{*} \leq D(\textbf{x}) \leq D^{*}$.  The boundary condition $g: H^{1}(\Omega) \rightarrow L^{2}(\partial \Omega_{N})$ is Lipschitz function such that,
\begin{equation}
\Vert g(u) - g(v) \Vert_{L_{2}(\partial \Omega_{N})} \; \leq \; L
\Vert u - v \Vert_{H^{1}(\Omega)} \qquad \qquad \forall u,v \in H^{1}(\Omega).  \qquad L > 0.
\end{equation}



Let $\mathcal{T}_{h} = \left\{ \Omega_{e} \right\}_{e}$ be the triangulation of $\Omega$. The boundary $\partial \Omega = \partial \Omega_{D} \cup \partial \Omega_{N}$ is smooth enough such that the trace identity,


\begin{equation}
\Vert v \Vert^{2}_{L^{2}(\partial \Omega)} \, \leq \, C_{tr}(\Omega) \Vert v \Vert^{2}_{H^{1}(\Omega)}, \qquad \qquad 
\forall v \in H^{1}(\Omega) \label{eq:Trace}
\end{equation}


\noindent
holds.  The semidiscrete finite element method is, \newline

Find $u_{h}(t) \in \, H^{1}_{D}(\Omega) + u_{D}$ such that,
\begin{align}
(v,\partial_{t} u_{h})_{\Omega} \ + \ B(v,u_{h}) \; &= \; (v,f)_{\Omega} \ - \ \langle v, g(u_{h}) \rangle_{\partial \Omega_{N}} && \forall v \in V_{h} \quad   a.e. \ t \in (0,T], \label{eq:fem} \\
(v,u_{h}(t=0)_{\Omega} \; &= \; (v,u_{I})_{\Omega} && \forall v \in V_{h} \label{eq:fem_init}.
\end{align}


Where $V_{h}$ is the finite dimensional subspace of the Hilbert space $H_{D}^{1}(\Omega) \; = \; \left\{ v \in H^{1}(\Omega) : v \left\vert_{\partial \Omega_{D}} \right.  =  0 \right\}$.  Let be $V_{H}$ be the such that $v \ \in V_{h}$ then $v \vert_{\Omega_{e}} \in Q_{k}(\Omega_{e})$ where $Q_{k}$ is the tensor product of polynomials of order $k$. The bilinear $B : H^{1} \times H^{1} \rightarrow \mathbb{R}$ in \eqref{eq:fem} is coercive, meaning there exists an $\alpha \in \mathbb{R}$ such that,


\begin{equation}
B(v,v) \; \geq \alpha^{2} \; \Vert v \Vert_{H^{1}}^{2} \qquad \qquad \forall v \in H^{1}(\Omega)
\end{equation}

\bigskip


\textbf{Error Estimate:}
\textit{
If $\alpha^{2} \geq 2 \sqrt{L} C_{tr}$ then the finite element method defined in \eqref{eq:fem} and \eqref{eq:fem_init} has errors such that,}
\begin{equation}
\Vert u - u_{h} \Vert^{2}_{L^{\infty}\left([0,T]; L^{2}(\Omega)\right)}  \; \leq \; Ch^{k},
\end{equation}
\textit{for some constant $C = C(T,\alpha^{2},\Omega,L) > 0$.}

\medskip


\underline{Proof}:



Let $P: H^{1}_{D} \rightarrow V_{h}$ be the projection that satisfies the elliptic projection property,

\begin{equation}
B(v,u - Pu) \; = \; 0 \qquad \qquad \forall v \in V_{h}
\end{equation}


And has the approximation properties,

\begin{equation}
\Vert u - Pu \Vert_{L^{2}}(\Omega) \; \leq \; Ch^{k+1}  \qquad \text{and} \qquad 
\Vert u - Pu \Vert_{H^{1}}(\Omega) \; \leq \; Ch^{k} \label{eq:ApproxProp}
\end{equation}



The function $u(t) \in H^{1} + u_{D}$ also satisfies the finite element method, 
\begin{align}
(v,\partial_{t} u)_{\Omega} \ + \ B(v,u) \; &= \; (v,f)_{\Omega} \ - \ \langle v, g(u) \rangle_{\partial \Omega_{N}} && \forall v \in V_{h} \qquad a.e. \ t \ \in (0,T] \label{eq:weak} \\
(v,u(t=0)_{\Omega} \; &= \; (v,u_{I})_{\Omega} && \forall v \in V_{h} \label{eq:weak_init}
\end{align}



Subtracting the \eqref{eq:fem} from \eqref{eq:weak} we have,

\begin{equation}
(v,\partial_{t} (u - u_{h})) + B(v,u-u_{h}) \; \leq \; \vert \langle v, g(u) -g(u_{h}) \rangle_{\partial \Omega_{N}} \vert \qquad \qquad a.e. \ t
\end{equation} 



Define $e_{I}(t) = u(t) - Pu(t)$ and $e_{A}(t) = Pu(t) - u_{h}(t)$ and using the elliptic projection property then the above becomes,

\begin{equation}
(v,\partial_{t} e_{A}) + B(v, e_{A}) \; \leq \; \vert (v,\partial_{t} e_{I}) \vert \ + \ \vert \langle v, g(u) -g(u_{h}) \rangle_{\partial \Omega_{N}} \vert \qquad \qquad a.e. \ t
\end{equation} 


Taking $v = e_{A}(t)$ and using the coercivity property of $B(\cdot, \cdot)$ we have,

\begin{align}
\frac{d}{dt} \Vert e_{A} \Vert^{2}_{L^{2}(\Omega)}  +  2\alpha^{2} \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2}  \leq \;  2 \, \vert (e_{A},\partial_{t} e_{I}) \vert \ + \ 2 \, \vert \langle e_{A}, g(u) -g(u_{h}) \rangle_{\partial \Omega_{N}} \vert \qquad \qquad a.e. \ t
\end{align} 


We now use Young's inequality to obtain,

\begin{align}
\frac{d}{dt} \Vert e_{A} \Vert^{2}_{L^{2}(\Omega)}  +  2\alpha^{2} \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2}  \leq \; \Vert  e_{A} \Vert_{L^{2}(\Omega)}^{2} + 4 \Vert \partial_{t} e_{I} \Vert^{2}_{L^{2}(\Omega)} + \frac{1}{\delta} \Vert  e_{A} \Vert_{L^{2}(\partial \Omega_{N})}^{2} + \delta \Vert g(u) -g(u_{h}) \Vert_{L^{2}(\partial \Omega_{N})}^{2}
\end{align} 



Using the Lipschitz property of $g(\cdot)$ we have,

\begin{align}
\frac{d}{dt} \Vert e_{A} \Vert^{2}_{L^{2}(\Omega)}  +  2\alpha^{2} \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2}  \leq \; \Vert  e_{A} \Vert_{L^{2}(\Omega)}^{2} + 4 \Vert \partial_{t} e_{I} \Vert^{2}_{L^{2}(\Omega)} + \frac{1}{\delta} \Vert  e_{A} \Vert_{L^{2}(\partial \Omega_{N})}^{2} + \delta L \Vert u -u_{h} \Vert_{H^{1}(\Omega)}^{2}
\end{align} 


Using the Trace identity \eqref{eq:Trace} on the second to last term and the triangle inequality on the last term,

\begin{align}
\frac{d}{dt} \Vert e_{A} \Vert^{2}_{L^{2}(\Omega)}  +  2\alpha^{2} \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2}  \leq \; \Vert  e_{A} \Vert_{L^{2}(\Omega)}^{2} + 4 \Vert \partial_{t} e_{I} \Vert^{2}_{L^{2}(\Omega)} + \left(\frac{C_{tr}^{2}}{\delta} + \delta L \right) \Vert  e_{A} \Vert_{H^{1}(\Omega)}^{2} + \delta L \Vert e_{I} \Vert_{H^{1}(\Omega)}^{2}
\end{align} 



Rearranging terms we have,


\begin{align}
\frac{d}{dt} \Vert e_{A} \Vert^{2}_{L^{2}(\Omega)}  +  \left(2\alpha^{2} - \frac{C_{tr}^{2}}{\delta} - \delta L \right) \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2}  \leq \; \Vert  e_{A} \Vert_{L^{2}(\Omega)}^{2} + 4 \Vert \partial_{t} e_{I} \Vert^{2}_{L^{2}(\Omega)} + \delta L \Vert e_{I} \Vert_{H^{1}(\Omega)}^{2} \label{eq:intermediate}
\end{align} 


Taking $ 2\alpha^{2} - \frac{C_{tr}^{2}}{\delta} - \delta \; = \; \alpha^{2}$ we obtain the quadratic form,


\begin{equation}
L \delta^{2} - \alpha^{2} \delta + C_{tr}^{2} \; = \; 0,
\end{equation}


which has a real positive solution if $\alpha^{2} \, \geq \, 2 \sqrt{L} C_{tr}$.  With these choices then \eqref{eq:intermediate} becomes


\begin{equation}
\frac{d}{dt} \Vert e_{A} \Vert^{2}_{L^{2}(\Omega)}  +  \alpha^{2}  \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2}  \leq \; \Vert  e_{A} \Vert_{L^{2}(\Omega)}^{2} + 4 \Vert \partial_{t} e_{I} \Vert^{2}_{L^{2}(\Omega)} + \delta \left( \alpha^{2}, C_{tr} \right) L \Vert e_{I} \Vert_{H^{1}(\Omega)}^{2} 
\end{equation}


Integrating over $(0,t)$ with $t \leq T$ yields,

\begin{align}
\Vert e_{A}(t) \Vert^{2}_{L^{2}(\Omega)}  + \alpha^{2} \int_{0}^{t}   \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2} ds  &\leq \; \int_{0}^{t}  \Vert  e_{A} \Vert_{L^{2}(\Omega)}^{2} ds \\ 
 & \quad + \int_{0}^{t} \left( \Vert e_{A}(0) \Vert^{2}_{L^{2}(\Omega)} + 4 \Vert \partial_{t} e_{I} \Vert^{2}_{L^{2}(\Omega)} + \delta \left( \alpha^{2}, C_{tr} \right) L \Vert e_{I} \Vert_{H^{1}(\Omega)}^{2}  \right) ds 
\end{align}


Using a Gromwall's inequality, the approximation properties \eqref{eq:ApproxProp} and the fact the  $\Vert e_{A}(0) \Vert_{L^{2}(\Omega)}^{2} \; = \; 0$ we have,


\begin{equation}
\Vert e_{A}(t) \Vert_{L^{2}(\Omega)}^{2}  + \alpha^{2}\int_{0}^{t} \Vert e_{A} \Vert_{H^{1}(\Omega)}^{2} ds  \; \leq \; C(t,\alpha^{2}, \Omega,L) \, h^{2k}
\end{equation}



where the dependency on constants dependency on $\Omega$ comes from the Trace theorem constant $C_{tr}(\Omega)$.  Then taking the maximum over all $t \in (0,T)$ we have,

\begin{equation}
\Vert e_{A} \Vert_{L^{\infty}([0,T];L^{2}(\Omega))}  \; \leq \; C(T,\alpha^{2}, \Omega,L) \, h^{k}
\end{equation}



Using the triangle inequality $\Vert u - u_{h}  \Vert_{L^{\infty}([0,T];L^{2}(\Omega))}  \; \leq \; \Vert e_{A} \Vert_{L^{\infty}([0,T];L^{2}(\Omega))} + \Vert e_{I} \Vert_{L^{\infty}([0,T];L^{2}(\Omega))}$ gives the desired results.


\end{document}