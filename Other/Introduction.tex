\documentclass[10pt]{report}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fullpage}
\usepackage{graphicx}
\numberwithin{equation}{section}

\begin{document}

\chapter{Introduction}


\section{A Historical Perspective}



\noindent
With the invention of the bipolar transistor in 1948, Bardeen, Brattain, and Shockley opened up the doors to world in the modern era.  Suddenly a new plethora of inventions and technology was at our doorstep.   Technology that would soon touch on every aspect of our lives and at the heart of this new technology is the semiconductor. 



\vspace{3mm}


\noindent
The semiconductor also plays a critical role in the production of energy in solar cells.  The ability for a solar cell to create power is due to the \textbf{photovoltaic effect}.  This was first observed by A. E. Bacquerel in 1839 using Selenium and only had an efficiency of 1\%. The underlying mechanism of the photovoltaic effect is that photons excite electrons in the valance band to the conduction band, producing two mobile carriers called an electron and hole.  In In metals the excited electron will disipate its kinetic energy and relax back to the valance band.  This process can also be described as the electron and hole recombining.  In a solar cell the semiconductor has a built in electric field that separates charges before they can recombine and uses them to drive a current through the device.  The two requirements to that contribute to the photovoltaic effect are:

\vspace{2mm}

\begin{enumerate}
\item  Charge carriers created by photon absorption.
\item  Separation of charge carriers by built in electric field.
\end{enumerate}

\vspace{2mm}


\noindent
The first realistic solar cell was invented in 1954 by Chapin, Fuller, and Pearson, achieving an efficiency rate of 6\%.  Starting with the energy crisis of 1973 interest in using solar cells as ways to generate power has increased.  Efficienies have risen dramatically pushing theoretical limits (at present moment record is approximately 44\%), while prices have dropped from \$100 a Watt in 1971 to \$0.62 a Watt in 2012.  


\vspace{3mm}


\noindent
Over the last 40 years solar cells have been intensely optimized and designed for maximum efficiency, but not necessary for optimal Watt/Price ratio.  New designs that use heterostructures, concentrators, thin films, ribbons, etc. have led to amazing technology and despite recent price drops, problems still remain with large scale deployment.  The biggest of these two challenges being:


\vspace{2mm}

\begin{enumerate}
\item High cost to Watt ratio.  (efficiency vs. price)
\item Lack of ability to store generated energy.
\end{enumerate}


\vspace{2mm}

\noindent
The next generation solar cells need to be able to over come both these challenges.  The solar cells discussed in this document will hopefully be used to overcome both challenges and the computational tools developed will be used in the aid of developing and designing these devices.


\vspace{5mm}



\section{Semiconductor Physics}


\noindent
Semiconductors are materials where at thermodynamic equilibrium the Fermi level lies within a moderate band gap.  This is contrast to metals, where the Fermi level lies within the conduction band giving rise to highly mobile electrons.  And also insulators where the Fermi level lies within a large band gap and does not easily give rise to free carriers.

%
%\begin{center}
%\includegraphics[scale=0.6]{../images/bandgap.png} 
%\end{center}
%%http://upload.wikimedia.org/wikipedia/commons/c/c7/Isolator-metal.svg


\noindent
From a purely classical point of view, we only have two types of charge carriers, electrons  with negative charge and holes with positive charge.  Holes are not real particles, rather broken bonds of electrons being freed from the lattice. However, in practice, they are almost always treated as positively charge particles.  \footnote{These are actually the electron and hole densities, traditional in semiconductor physics they are measured in units of cm$^{-3}$.}  We often think of \textbf{free electrons} $(n)$ as living in the conduction band and \textbf{free holes} $(p)$ live in the valance band.  Electrons and holes can exist in opposite bands, but in this case they are not free to move about the last.  



\vspace{3mm}


\noindent
Every semiconductor has an intrinsic density $(n_{i}$) or a natural density of free electrons and free holes\footnote{We will just refer to these as electrons and holes and refer to non-free carriers as \textbf{bound carriers.}} which at equilibrium satisfies 


$$ n_{e} = n_{i} \qquad p_{e} = n_{i}$$


\noindent
This gives us the important equation for equilibrium densities called the \textbf{law of mass}\footnote{This equation holds for intrinsic as well as extrinsic semiconductors.}:

\begin{equation}\label{eq:lawofmass}
n_{e} \, p_{e} \; = \; n_{i}^{2}
\end{equation}



\noindent
The whole power of semiconductors is the ability to \textbf{``dope"} them, i.e.  add electrons and holes so they have more free carriers than they would normally.  This is achieved by replacing one atom in a unit cell of the crystal lattice.  Take for instance Silicon:


\vspace{3mm}

%
%\begin{center}
%\includegraphics[scale=0.75]{../images/doping.png} 
%\end{center}
%%http://pveducation.org/pvcdrom/pn-junction/doping

\vspace{3mm}



\noindent
If we substitute one of the Silicon atom with Phosphorous (P has one more atom than Si), it has an extra electron and the material is called `\textbf{`n-doped"} or \textbf{``n-type."}  While if we substitute an Si atom with Boron  (B has one less atom than Si), the new material has an extra hole and is called \textbf{``p-doped"} or \textbf{``p-type".}


\vspace{2mm}


\noindent
The effects of doping a semiconductor raises or lowers the density of electrons or hole depending on the type of doping.  In intrinsic materials the Fermi level lies near the middle of the band gap, but in doped semiconductors the conduction band and valance band are moved closer of further away from the Fermi level depending on the doping, as can be seen below:

\vspace{3mm}

%
%\begin{center}
%\includegraphics[scale=0.75]{../images/doping_bandstructure.png} 
%\end{center}
%%https://www.dur.ac.uk/cmp/research/groups/aim/themes/solarcellsprinciple/

\vspace{3mm}


\noindent
Mathematically we can model this the density of free electrons in the conduction band as:


\begin{equation}
n = N_{C} \,  e^{-(E_{C}-E_{F})/k_{B} T}
\end{equation}


\noindent
And the density of free holes in the valance band as:


\begin{equation}
p = N_{V} \, e^{-(E_{F}-E_{V})/k_{B} T}
\end{equation}


\noindent
Where $E_{C}$ and $E_{V}$ are the energy levels of the bottom of the conduction band and the top of the valance band respectively.  And $N_{C}$ and $N_{V}$ are the densities the states of the conduction and valance band respectively.\footnote{We have implicitly decided to describe electrons and holes as classical charge carriers by using Boltzmann statistics. In reality, these carriers are quantum objects and must be treated using Fermi statistics, however, at room temperature $(E_{C} - E_{F}) >> k_{B} T$ and $(E_{F} - E_{V}) >> k_{B} T$ meaning Fermi statistics can be well approximated by Boltzmann statistics.} \footnote{The density of states of each band can be calculated from first principles and are characteristic to each material.} They can also be expressed in terms of the intrinsic carrier densities and intrinsic energy levels as:

\begin{equation}
n = n_{i} \,  e^{(E_{F}-E_{i})/k_{B} T}
\end{equation}


\begin{equation}
p = n_{i} \, e^{(E_{i}-E_{F})/k_{B} T}
\end{equation}


\noindent
It is important to note that a requirement to be in \textbf{equilibrium} is that the Fermi level must be constant and as a consequence no transport of carriers or currents.


\vspace{3mm}


\noindent
Using the law of mass \eqref{eq:lawofmass} we can arrive at another important identity relating the intrinsic density to the band gap $E_{g}$ of our material:


\begin{equation}
n_{i} = \sqrt{N_{C} N_{V}}e^{-E_{g}/2k_{B} T}
\end{equation}






\vspace{3mm}


\subsection{Charge Generation And Separation}


\noindent
The \textbf{photoelectric effect} is the generation of free charge carriers by absorption of photon. This phenomenon is readily observed in both semiconductors and metals.  In metals the fact that the Fermi level lies within the conduction band means that photons will be absorbed by free electrons.  These free electrons will quickly dissipate the kinetic energy they gained as heat or radiation as shown below:

\vspace{2mm}
%
%\begin{center}
%\includegraphics[scale=0.65]{../images/photoelectriceffect.png}
%\end{center}
%%http://www.kennislink.nl/upload/115438_962_1091691423656-photon_absorption_large.jpg

\vspace{2mm}


\noindent
In semiconductors, photons with energy less than the band gap pass through the crystal while photons with energy greater than or equal to the band gap are absorbed.  The photons energy is used to promote electrons from the valance to the conduction band leaving behind a hole in the valance band.  This is shown below:

\vspace{2mm}


%\begin{center}
%\includegraphics[scale=0.65]{../images/semiconductor_photo_effect.png}
%\end{center}
%%http://www.rp-photonics.com/img/tpa.png
\vspace{2mm}


\noindent
Due to the existence of the bandgap, the electron will not dissipate the kinetic energy and return to the valance band, this is known as \textbf{electron-hole recombination}.  The time it takes its for this to happen (\textbf{recombination time}) is long enough for the electrons and holes to be separated by an electric field and used to generate currents.


\vspace{2mm}


\noindent
In order to generate currents and subsequently power for a solar cell one must have an efficient mechanism for separating \textbf{photogenerated electron and hole pairs}.  This is provided by built in electric fields or potentials which occur at \textbf{rectify junctions}.  These three ways to create such junctions:

\vspace{2mm}

\begin{enumerate}
\item \textbf{Shottky Contacts} \, : \, Potentials caused by semiconductor-metal junctions.
\item \textbf{P-N Junctions} \, : \, Potentials caused by gradients in densities.
\item \textbf{Semiconductor-Electrolyte Interface} \, : \, Potentials due to electrochemical reactions.
\end{enumerate}

\vspace{2mm}

\noindent
Shottky contacts are not used in photovoltaic device and are often referred to as \textbf{parasitic resistance}. P-N junctions form the  backbone of \textbf{solid state solar cell}s.  Semiconductor-electrolyte interfaces are used in \textbf{photo-electrochemical solar cells}.  We will discuss these further in the following sections


\vspace{5mm}




\subsection{P-N Junctions: Equilibrium}


\noindent
At the heart of a semiconductor devices is the p-n junction shown below:
%
%\begin{center}
%\includegraphics[scale=0.7]{../images/pn2.png} 
%\end{center}
%%http://ecee.colorado.edu/~bart/book/book/chapter4/ch4_2.htm#fig4_2_2



\noindent
When the two materials are brought together, electrons diffuse into p-type material and holes diffuse into the n-type semiconductor.  The diffusion of free carriers leaves behind the ionized cores of the atoms which now have the opposite sign of the carriers that left them.  This process continues until an electric field is built up from the ionized cores that opposes the diffusion of \textbf{``minority
carriers"} across the junction.  When this happens the material is in equilibrium as displayed below:

%
%\begin{center}
%\includegraphics[scale=0.5]{../images/Pn-junction-equilibrium.png} 
%\end{center}

%http://en.wikipedia.org/wiki/File:Pn-junction-equilibrium.png


\noindent
The area of the semiconductor where there are only ionized cores and no free carriers is called the ``depletion region."  The important part of a pn junction is that doping gradients have built in potentials corresponding to them.  


\vspace{3mm}

\noindent
In terms of the band structure, a p-n at equilibrium is shown below:
%
%\begin{center}
%\includegraphics[scale=0.7]{../images/pn-bandstructure.png} 
%\end{center}
%%http://ecee.colorado.edu/~bart/book/book/chapter4/ch4_2.htm#fig4_2_3

\noindent
This image also shows the two mechanisms for classical charge transport in semiconductors: drift that is caused by electric fields and diffusion which is caused by density gradients.  We can often times think of this as a one dimensional process and mathematically write the transport of carriers as current densities:\footnote{$J_n$ and $J_p$ are called steady-state current densities, with densities of $A/(C \, cm^{2})$ and we have dropped the charge of a carrier.}


\begin{equation}%\label{eq:electroncurrent}
J_{n}(x) = - \mu_{n} \, n(x) \, E(x) \, - \, D_{n} \, \frac{dn}{dx} 
\end{equation}


\begin{equation}%\label{eq:holecurrent}
J_{p}(x) = \mu_{p} \, p(x) \, E(x) \, - \, D_{p} \, \frac{dp}{dx} 
\end{equation}


\noindent
In thermal equilibrium these two transport processes oppose each other so that there is no current through out the device.  This implies that $J_n = 0$ and $J_p = 0$.  Using this and the fact that electric field is negative the gradient of the potential $E(x) = - \frac{d}{dx} \Phi$ and the Einstein relations that $\frac{D}{\mu} = \frac{k_{B} T}{q}q$ we can recover an equation for the "built in potential" also called the ``contact potential":



\begin{equation} \label{eq:contactpotential}
\Phi_{0} = \frac{k_{B} T}{q} \ln \left(\frac{N_{a}N_{d}}{n_{i}^{2}} \right)
\end{equation}


\noindent
Where $N_{a}$ and $N_{d}$ are the initial doping densities of holes and electrons respectively. The contact potential can also be related to the width of the depletion region $W = x_{n} + x_{p}$ by using the \textbf{``depletion region approximation"} to arrive at:\footnote{we solve Poisson's equation assuming there are no free carriers with in the depletion region and that the electric field is zero outside the depletion region.}


\begin{equation}\label{eq:DepletionWidthToPotential}
W = \left[ \frac{2\epsilon \Phi_{0}}{q} \left(\frac{1}{N_a} + \frac{1}{N_d}\right) \right]^{1/2}
\end{equation}


\noindent
The distance into which the depletion region penetrates the $n-$type and $p-$type regions are $x_{n}$ and $x_{p}$ respectively and can be shown to be:


\begin{equation}
x_{n} \quad = \quad \sqrt{\frac{2 \epsilon \Phi_{0}}{q} \left[ \frac{N_{d}}{N_{a}(N_{a} + N_{d})} \right]}
\end{equation}


\begin{equation}
x_{p} \quad = \quad \sqrt{\frac{2 \epsilon \Phi_{0}}{q} \left[ \frac{N_{a}}{N_{d}(N_{a} + N_{d})} \right]}
\end{equation}



\noindent
Which shows us that depletion region extends farther into lighter doped regions that it does the heavier doped region.  This explains the desire for highly doped thin $n$ regions and moderately doped thick $p$ regions in Si based solar cells. Since mobility of electrons is higher in $p-$type Si than holes in $n-$type Si we make the depletion region extend further an captures more photogenerated electrons in the base as well as having a short emitter so that most of it is the depletion region to capture photogenerated holes.




\vspace{3mm}


\subsection{P-N Junctions: Non-Equilibrium}

\noindent
Driving a device out of equilibrium entails enforcing a potential across the device. In forward bias we would impose a positive potential across our device depicted below:

%
%\begin{center}
%\includegraphics[scale=0.7]{../images/ForwardBias.png} 
%\end{center}
%%http://upload.wikimedia.org/wikipedia/commons/a/a8/PnJunction-Diode-ForwardBias.PNG

\noindent
A forward bias forces holes and electrons into the depletion region where they recombine with ionized cores thereby shrinking the depletion region.  While in reverse bias holes and electrons are drawn away from the depletion region.  These carries leave behind their ionized cores and this causes the depletion region to grow:

%
%\begin{center}
%\includegraphics[scale=0.7]{../images/ReverseBias.png} 
%\end{center}
%%http://upload.wikimedia.org/wikipedia/commons/a/a8/PnJunction-Diode-ReverseBias.PNG


\noindent
Physically, the applied potential does not cause the carriers to drift into or away from the depletion region.  A forward bias acts to lower the contact potential and allows more carriers to diffuse electrons from the n-type material across to the p-type material and vice versa.  This processes is called \textbf{``minority carrier injection"} as the currents are caused by minority carriers diffusing into oppositely doped material.  A reverse bias acts to raise the contact potential and not allow the injection of minority carriers across the depletion region.  This process is displayedd below:
%
%\begin{center}
%\includegraphics[scale=0.7]{../images/biasedpn.png} 
%\end{center}
%
%%http://ecee.colorado.edu/~bart/book/book/chapter4/gif/fig4_2_4.gif


\noindent
Out of equilibrium, the Fermi-level to be no longer constant (called \textbf{``Fermi levels splitting"}) and thus cause the flow charge carriers.  In steady-state we can still define \textbf{Quasi Fermi-levels} $E_{F_{n}}$ and $E_{F_{p}}$ for electrons and holes as shown above.  In forward bias, $E_{F_{n}} - E_{F_{p}} > 0$ and a current flows through the p-n junction.  In reverse bias, $E_{F_{n}} - E_{F_{p}} < 0$ and very little current flows through the device in the other direction.  We can define densities in quasi-equilibrium as 


\begin{equation} 
n = N_{C} \, e^{-(E_{C} - E_{F_{n}})/k_{B} T_{n}}
\end{equation}

\begin{equation}
p = N_{V} \, e^{-(E_{F_{p}} - E_{V})/k_{B} T_{p}}
\end{equation}


\noindent
Using \eqref{eq:lawofmass} we can obtain 


$$n p \, = \, n_{i}^{2} \, e^{\Delta \mu /k_{B}}$$


\noindent
Where $\Delta \mu = E_{F_{n}} - E_{F_{p}}$ and is often referred to as the \textbf{``chemical potential".}  If the quasi Fermi levels are depend on $x$ then we can define so called \textbf{``local quasi Fermi levels".  
}

\vspace{3mm}


\noindent
The result of the current on the bias leads to an equation of the form:

\begin{equation}\label{eq:DiodeEquation}\
J = J_{0} \left( e^{q \, V_{\text{Applied}} / k_{B} T} \right)
\end{equation}

\noindent
Which is the equation for an ideal diode and has an IV curve displayed below:

%\begin{center}
%\includegraphics[scale=0.7]{../images/idealdiode.png} 
%\end{center}
%%http://cnx.org/content/m1008/latest/

\noindent
Where the small negative current at reverse bias is do to thermionic emission. 


\vspace{5mm}


\section{Solid State Solar Cells}


\subsection{Physics Of Solar Cells}

\noindent
Traditional solid state solar cells (Si and GaAs based) are modeled as a p-n junctions with metal contacts on each end.  While the optimal design depends on the materials used and whether it is a monocrystaline or polycrystaline cell, traditional solar cells have the basic physical operations shown below

%
%\begin{center}
%\includegraphics[scale=0.7]{../images/solarcell.png} 
%\end{center}
%%http://www.solarpower2day.net/solar-cells/
%

\noindent
The top layer of the cell is usually coated with a protective anti-reflective layer that is designed to maximize photon capture.  The next layer is called the \textbf{``emitter"}; in Si-based cells it is usually a highly doped ($10^{19} \, cm^{-3}$) thin layer ($0.5 \mu \, m)$ of n-type, while in GaAs-based cells it is a highly doped ($10^{8} cm^{-3}$), thin layer ($0.5 \mu \, m$) of p-type.\footnote{In Si it is always a $n^{+}-p$ junction for reasons to be discussed, but in GaAs cells it be either $p^{+}-n$ or $n^{+}-p$.  In practice the former preforms better.} Running across the emitter are the metal contacts called the \textbf{``fingers"} or \textbf{"front contact"}.  These are designed based on the material type to maximize capture of photo-generated charge carriers, minimize recombination with minority carriers, while at not limit the surface area that light can hit.  The next layer is the junction and the layer below that is the called the \textbf{``base."}  The  base is the opposite doping of the emitter; in Si based cells it a think ($300 \mu \, m$) moderately doped ($10^{16} cm^{-3}$) p-type, while in GaAs cells it is a moderately doped ($10^{17} cm^{-3}$) and moderately thick ($4 \mu \, m$) n-type material.  Finally at the bottom of the cell is the \textbf{``rear contact"} which is a metal contact which is designed to maximize capture of photo-generated charge carriers and minimize rear surface recombination.    A typical device is shown below:
%
%\begin{center}
%\includegraphics[scale=0.5]{../images/design.png} 
%
%\end{center}
%%http://pveducation.org/sites/default/files/PVCDROM/Solar-Cell-Operation/Images/SCELL.jpg




\vspace{3mm}


\noindent
When light enters through the top of a solar cell, the photons travel through the emitter and base.  Whether the light is absorbed or reflected depends on the frequency or wavelength of the light and hence its energy:

$$ E = \hbar \omega $$


\noindent
Where $\hbar$ is Planck's constant and $\omega = 2 \, \pi \, f$ is the angular frequency of the light.  High energy light that is greater than the band gap ($E >> E_{g}$) is usually absorbed by exciting electrons in the conduction band.  The electrons then lose that energy dissipating it as heat and re-radiating light back out.\footnote{GaAs has a direct band gap, while Si has an indirect band gap, this not only gives GaAs a higher photon absorption coefficent than Si, but also dissipate energy as light rather than heat.  This is one reason GaAs is a preferred material for concentrated solar panels.  However, the lower cost of Si more compensates the added efficency of GaAs for terrestrial energy production.}   Low energy light, light with energy less than the band gap $(E < E_{g})$ usually travels through the crystal unabsorbed and is reflected back out by the rear contact. 


\vspace{3mm}


\noindent
Light with energy equal to or slightly above the band gap $(E \gtrsim E_{g})$ is absorbed by electrons in the valance band \textit{of both n-type and p-type materials} and promotes them to electrons in the conduction band on their respective sides of the junction.  This electron is now free to move about the crystal and leaves behind a hole in the valance band \textit{of both the n-type and the p-type material}.  This process of one photon can creating an electron-hole pair is called "electron-hole pair generation" and is shown below (where the photon is absorbed in the p-type):


%\begin{center}
%%\includegraphics[scale=0.95]{../images/solarcellbandstructure.png} 
%\end{center}
%%http://upload.wikimedia.org/wikipedia/commons/thumb/8/86/BandDiagramSolarCell-en.gif/300px-BandDiagramSolarCell-en.gif


\noindent
Normally when light is absorbed by a material in a semiconductor the excited electron in the conduction band relaxes back to the valance band by dissipating its kinetic energy as light or heat.  This process is called ``electron-hole recombination" and can occur in a variety of different processes.  Solar cells are designed to separate the generated electron and hole pair before recombine and use them to generate a current.


\vspace{2mm}


\noindent
After the electron-hole pair are generated they cause a small current by randomly diffusing \textit{in the same direction}:


\begin{align*}
& J_{n, \, \text{diff}} = - D_{n} \, \frac{dn}{dx} \\
& J_{p, \, \text{diff}} = - D_{p} \, \frac{dp}{dx} \\
\end{align*}


\noindent
The electrons and holes diffuse for on average for a period $\tau$\footnote{For most processes the recombination time for electrons and holes is the same, exceptions lie in ``trap-assisted recombination" and other process that involve more carriers.} before recombining.  The average length they can travel before recombining is called the \textbf{``diffusion length"} and is given by 

\begin{align*}
& L_{n} =  \sqrt{\tau D_{n}} \\
& L_{p} =  \sqrt{\tau D_{p}} \\
\end{align*}


 

\subsection{Performance Of Solid State Solar Cell}


\noindent
The process of charge separation necessary for a solar cell to generate power will occur if the generated electrons-hole pairs are created within a diffusion length of the junction.\footnote{The difference in sizes of the of the layers in Si cells and GaAs cells has to do with the fact in Si $L_{n}$ for electrons in p-type is larger than $L_{p}$ in n-type.  In GaAs they are roughly the same.}  As an example take the electron-pair generation of the last figure.  The electron in the p-type material that is able to diffuse into the junction is swept away by the electric field and injected into the n-type.  While the hole on the p-type will be forced back into the p-type material when it diffuses into the junction.   The extra electron in n-type will then will then cause the diffusion of an electron from the semiconductor to the metal and the analogous process on the p-side (provided it does not recombine with an electron-hole pair created before reaching the contact).  



\vspace{3mm}


\noindent
To summarize\footnote{We have been using what is called the ``depletion region approximation" which says there are very few free carriers in the depletion region, meaning little recombination in this region.  In reality this is not the case, but it allows for easy analytic analysis.}, in the p-n type region currents are mostly caused by the diffusion $J_{n, \, \text{diff}}
$ and $J_{p, \, \text{diff}}$ and run in the same direction.  While in the junction the currents are mostly due to drift and flow in opposite directions:


\begin{align*}
& J_{n, \, \text{diff}} = - \mu_{n} n E  \\
& J_{p, \, \text{diff}} = + \mu_{p} p E  \\
\end{align*}


\noindent
The key ingreediant of any solar cell is the electric field used to separate the electron-hole pair, such a junction is called a \textbf{``rectifying junction"} as it allows current in one direction much more easily than the other.



\vspace{3mm}


\noindent
In order for a solar cell to generate power we need have a p-n junction in forward bias.  This is because in order to generate power, the overall power delivered to the device must be negative.  By forward biasing the cell this means that when there is no sunlight there will be a small (positive) current called a \textbf{``dark current"} which is due to the injection of minority carriers into the depletion region and subsequent recombination of electrons and holes.\footnote{Interestingly this means that with no sun, solar cells actually consume energy and emit light!}  However, when cell is illuminated a large (negative) current is created which flows in the opposite direction to the dark current and is called the \textbf{``illuminated current."}  For IV curves in practice, we rotate the axis such that the illuminated current is positive and the dark current negative.  The IV curve for solar cell is given below:


\begin{center}
%\includegraphics[scale=0.65]{../images/solarcellIV.png} 
\end{center}
%http://taconixtechnologies.com/images/solar_water_diagram.jpg


\noindent
The efficiency $(\eta)$ of the solar cell is defined as the ratio of the maximum power generated by the device over power absorbed from the sun


\begin{equation}\label{eq:efficency}
\eta = \frac{J_{M} \, V_{M}}{E_{S} \, A}
\end{equation}


\noindent
Where $E_{S}$ is the flux of photons per unit area, $A$ is the surface area of the device, $J_{M}$ and $V_{M}$ are the current density and voltage associated with the maximum power output of the device.


\vspace{3mm}

\noindent
A solar cell is more of current generating device rather than a voltage generator.  The current density output of the solar cell is proportional to the concentration of light, i.e. higher photon flux leads to higher currents.  For many devices the current output is sufficient, around 10-100mA for a solar cell with area 1 cm$^{2}$.  However, the voltage delivered by the device is limited by the band gap $V_{\text{OC}} \leq q \, E_{g}$ and is limited to a range of 0.5 -1.0 Volts.  Therefore, a single cell does not deliver much power, however solar cells are connected in series to create a ``module" which can deliver a significant voltage.  Every device still has a limited efficiency called the \textbf{``theoretical maximum efficiency."}


\vspace{3mm}




\section{Photoelectrochemical Solar Cells}


\vspace{2mm}


\noindent
Photoelectrochemical solars cells (PECs) are new generation of photovoltaic devices.  They have many promising features over traditional solar cells including:


\begin{enumerate}
\item Low cost materials and fabrication
\item Allow for flexible/bendable panels
\item Can be used to directly produce hydrogen
\item Can be used to convert $CO_{2}$ into useful chemicals
\end{enumerate}


\noindent
However, some of the challenges they are facing include:


 \begin{enumerate}
\item Lower efficiency 
\item Shorter lifetime
\item Lack of developed theory
\end{enumerate}


\noindent
What we shall discuss is the formation of a PECs that can be used to directly produce hydrogen.  The physical process of separating photo-generated electron-hole pairs is the same the conventional solar cells, but the means of achieving the necessary electric field are vastly different.  A simple depiction of the device is shown below:


\begin{center}
%\includegraphics[scale=0.65]{../images/pecdiagram.png} 
\end{center}
%http://electrochem.cwru.edu/encycl/fig/p06/p06-f01.png


\noindent
To form a PECs we take a semiconductor with a metal contact at one end and the other end is immersed in an electrolyte that has an appropriate counter electrode in it as well.  If we use a $n-type$ semiconductor as an example, and the system is allowed to achieve equilibrium, the semiconductor will have a bandstructure depicted below:

\vspace{3mm}

\begin{center}
%\includegraphics[scale=0.65]{../images/semi-electrolytebandstructure.png} 
\end{center}
%http://www.porous-35.com/electrochemistry-semiconductors-8.html

\vspace{3mm}

\noindent
This device has a built in ``depletion region" that occurs near the interface of the semiconductor-electrolyte interface.  While this generates an electric field that is used to achieve the same effect of separating electron-hole pairs as p-n junction, its formation and mathematical analysis is completely different from that of a p-n junction.  The details will be discussed further in chapter on boundary conditions.

\vspace{3mm}

\noindent
The PECs operates when a photon striking the semiconductor generates an electron hole pair within the depletion region or within a diffusion length of it, the hole will be swept towards the electrolyte and the electron will be forced in the opposite direction eventually causing a diffusive current across the metal. The hole in the depletion region will be carried towards the semiconductor-electrolyte interface and when it reaches this boundary will be used as a catalyst to oxidize (remove an electron) a species in the solution. On the other end the current at that is created will drive through an external circuit and eventually reach the counter electrode where it will cause a reduction reaction (donating electron) to a species in the solution.


\vspace{3mm}


\noindent
A diagram of how a watering splitting device would look physically is shown below:


\begin{center}
%\includegraphics[scale=0.65]{../images/watersplitting.png} 
\end{center}
%http://electrochem.cwru.edu/encycl/fig/p06/p06-f03.png

\noindent
The details of its operation will be further discussed in the chapter devoted to boundary conditions. 


\vspace{5mm}


\section{Need For New Computational Tools}


\noindent
Analytic solutions for the drift-diffusion equations exist in 1D with the assumptions of:


\vspace{2mm}


\begin{enumerate}
\item The depletion approximation.
\item Linear recombination.
\item Simple mobility, diffusivity, and carrier lifetimes.
\end{enumerate}

\vspace{2mm}

\noindent
Computational methods have been used for design semiconductor devices have been used for many decades. The design of new photovoltaic devices with complicated geometries, boundary conditions, and material coefficients requires new computational tools to solve these problems.  The presentation and application of these new tools are the topic of this document.

%
\end{document}