\documentclass[10pt]{report}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{color}
\numberwithin{equation}{section}



\begin{document}

\tableofcontents

\chapter{Gummel Iteration With LDG And Reactive Interfaces}

\section{Model Problem}

We shall present the LDG formulation for the drift diffusion equations in a semiconductor-electrolyte device in 1D. We model this as four charge carriers: electrons, $\rho_{p}$, holes, $\rho_{p}$, reductants, $\rho_{r}$, and oxidants $\rho_{o}$.   We take the domain for the semiconductor to be $[-1, \, \Sigma]$ and the domain for the electrolyte to be $[\Sigma, \, +1]$.


\vspace{3mm}

\noindent
The drift diffusion-Poisson equations for electrons and holes in semiconductors

\begin{align}
\frac{\partial \rho_{n}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( -\mu_{n}  \, E \,  \rho_{n}
\, - \,  D_{n} \, \frac{\partial \rho_{n}}{ \partial x} \right) \; &= \; 0 \nonumber \\ 
\frac{\partial \rho_{p}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( \mu_{p}  \, E \,  \rho_{p}
\, - \,  D_{p} \, \frac{\partial \rho_{p}}{ \partial x} \right) \; &= \; 0 \label{eq:OriginalFormulationSemi} \\
- \frac{\partial}{\partial x} \, \left( \, \epsilon_{r}^{S} \frac{\partial}{\partial x}   \, \Phi \, \right) \; = \; \frac{q}{\epsilon_{0}} \, \left( \, C \,  - \, ( \, \rho_{n}\, - \, \rho_{p} \, ) \, \right)& \label{eq:OriginalFormSemiPoisson}
\end{align}

\vspace{2mm}

\noindent
The Nernst-Planck-Poisson equations for reductants and oxidants in electrolytes

\begin{align}
\frac{\partial \rho_{r}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( -\mu_{r}  \, E \,  \rho_{r}
\, - \,  D_{r} \, \frac{\partial \rho_{r}}{ \partial x} \right) \; &= \; 0 \nonumber \\ 
\frac{\partial \rho_{o}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( \mu_{o}  \, E \,  \rho_{o}
\, - \,  D_{o} \, \frac{\partial \rho_{o}}{ \partial x} \right) \; &= \; 0  \label{eq:OriginalFormulationElectro} \\
- \frac{\partial}{\partial x} \, \left( \, \epsilon_{r}^{E} \frac{\partial}{\partial x}   \, \Phi \, \right) \; = \; \frac{q}{\epsilon_{0}} \, \left( \, - (\, \rho_{r}\, - \, \rho_{o} \, ) \, \right)& \label{eq:OriginalFormeElectroPoisson}
\end{align}

\vspace{2mm}

\noindent
The boundary conditions for the carriers and potential  are Dirichlet conditions at $x = -1$ and $x = +1$:

\begin{align}
\rho_{n}(-1, \, t) \; = \; N_{D} \qquad & \qquad \rho_{r}(+1, \, t) \; = \; \rho_{r}^{\infty}  \\
 \qquad \rho_{p}(-1, \, t) \; = \; N_{A}  \qquad & \qquad \rho_{o}(+1, \, t ) \; = \; \rho_{o}^{\infty} \\
\Phi(-1, \, t) \; = \; 0 \qquad & \qquad \Phi(+1, \, t) = \Phi_{\text{applied}}
\end{align}


\vspace{2mm}


\noindent
We model the reactive-interface conditions at  $\Sigma$ as boundary conditions on the current:


\begin{align}
J_{n} \, \cdot \, \boldsymbol \eta \; = \; \left( \, -\mu_{n}  \, E \,  \rho_{n}\, - \,  D_{n} \, \frac{\partial \rho_{n}}{ \partial x} \, \right) \, \cdot \, (+1) \quad &= \quad \left[ k_{et} \, ( \rho_{n} \, - \, \rho_{n}^{e} )  \; \rho_{o} \right] \\
J_{p} \, \cdot \, \boldsymbol \eta \; = \; \left( \, \mu_{p}  \, E \,  \rho_{p}
\, - \,  D_{p} \, \frac{\partial \rho_{p}}{ \partial x} \, \right) \, \cdot \, (+1) \quad &= \quad \left[ k_{ht} ( \rho_{p} \, - \, \rho_{p}^{e} ) \, \rho_{r} \right] \\
J_{r} \, \cdot \, \boldsymbol \eta \; = \; \left( \, -\mu_{r}  \, E \,  \rho_{r} \, - \,  D_{r} \, \frac{\partial \rho_{r}}{ \partial x} \, \right) \, \cdot \, (+1)  \quad &= \quad \left[ +k_{ht} ( \rho_{p} \, - \, \rho_{p}^{e} ) \, \rho_{r} \, - \, k_{et} \, ( \rho_{n} \, - \, \rho_{n}^{e} ) \, \rho_{o}  \right]   \\
J_{o} \, \cdot \, \boldsymbol \eta \; = \; \left( \, \mu_{o}  \, E \,  \rho_{o} \, - \,  D_{o} \, \frac{\partial \rho_{o}}{ \partial x} \, \right) \, \cdot \, (+1)  \quad &= \quad \left[ -k_{ht} ( \rho_{p} \, - \, \rho_{p}^{e} ) \, \rho_{r} \, + \, k_{et} \, ( \rho_{n} \, - \, \rho_{n}^{e} ) \, \rho_{o}  \right] 
\end{align}

The $(+1)$ has to do with the fact that we are taking the normal direction $\boldsymbol \eta$ to be from the semiconductor to the electrolyte as was done in the original paper.  Additionally, at the interface we define the potential and displacement electric field to be continuous:


\begin{equation}\label{eq:PoissonInterfaceBC}
\llbracket \, \Phi \, \rrbracket_{\Sigma} \quad = \quad 0, \qquad \text{and}  \qquad 
\llbracket \,  \epsilon_{r} \, E \, \cdot \, \boldsymbol \eta \, \rrbracket_{\Sigma} \quad  = \quad  0 
\end{equation}


\newpage

\section{Gummel Iteration}


In this section we discuss the general procedure to solve our system of equations and numerical methods used.  For the drift-diffusion equations, we employ a forward Euler scheme for the temporal variables while we apply a local discontinuous Galerkin (LDG) scheme for the spatial variables.   In order to solve for the potential and electric field we employ a mixed continuous Galerkin (CG) scheme.  This allows us to simultaneously solve for the potential and electric field. 


We will focus on the general procedure to solve these systems of equations as well as how time stepping is incorporated.  However, we will not discuss the discretization for the spatial variables here, instead we will discuss those details in a later section.   Our model represents a system of nonlinear differential equations in different spatial domains that are coupled nonlinearly through the interface conditions.  Employing a explicit time stepping method, we eliminate one of the nonlinearities and use an iterative procedure (Gummel) to overcome the other. 

Our model involves solving two Poisson's equation and four drift diffusion equations. We first reformulate our model as one Poisson's equation and four drift-diffusion equations:


\begin{align}
- \frac{\partial}{\partial x} \, \left( \, \epsilon_{r} \frac{\partial}{\partial x}   \, \Phi \, \right) \;  &= \; f\qquad \qquad \text{on} \qquad \left[ -1, \, +1 \right] \nonumber \\ 
\frac{\partial \rho_{n}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( -\mu_{n}  \, E \,  \rho_{n}
\, - \,  D_{n} \, \frac{\partial \rho_{n}}{ \partial x} \right) \; &= \; 0 \qquad \qquad \text{on} \qquad \left[ -1, \, \Sigma \right] \nonumber \\ 
\frac{\partial \rho_{p}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( \mu_{p}  \, E \,  \rho_{p}
\, - \,  D_{p} \, \frac{\partial \rho_{p}}{ \partial x} \right) \; &= \; 0 \qquad \qquad \text{on} \qquad \left[ -1, \, \Sigma \right] \nonumber \\
\frac{\partial \rho_{r}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( -\mu_{r}  \, E \,  \rho_{r}
\, - \,  D_{r} \, \frac{\partial \rho_{r}}{ \partial x} \right) \; &= \; 0 \qquad \qquad \text{on} \qquad \left[ \Sigma , \, +1 \right] \nonumber \\ 
\frac{\partial \rho_{o}}{\partial t} \, + \, \frac{\partial}{\partial x} \left( \mu_{o}  \, E \,  \rho_{o}
\, - \,  D_{o} \, \frac{\partial \rho_{o}}{ \partial x} \right) \; &= \; 0 \qquad \qquad \text{on} \qquad \left[ \Sigma , \, +1 \right] \nonumber
\end{align}

\noindent
Where the function $\epsilon_{r}$ and $f$ are defined as:

\begin{align}
\epsilon_{r}(x) \; &= \;  \left\{ \begin{array}{cc}
\epsilon_{r}^{S}  &  \qquad \qquad \qquad \qquad \qquad \text{on} \quad   [ -1, \, \Sigma ] \\
\epsilon_{r}^{S} & \qquad \qquad \qquad \qquad \qquad \text{on} \quad   [ \Sigma , \, +1 ]
								\end{array}\right. \\
f(x,t) \; &= \;  \left\{ \begin{array}{cc}
\frac{q}{\epsilon_{0}} \, \left( \, C \,  - \, ( \, \rho_{n}\, - \, \rho_{p} \, ) \, \right) & \quad \text{on} \quad   [ -1, \, \Sigma ] \\
\frac{q}{\epsilon_{0}} \, \left( \,  - \, ( \, \rho_{r}\, - \, \rho_{o} \, ) \, \right) & \quad \text{on} \quad [ \Sigma , \, +1 ]
								\end{array}\right.
\end{align}


\vspace{3mm}


In order to solve this nonlienar system we use the Gummel iteration which decouples the equations into linear equations of Poisson type and drift diffusion type.  The iterative procedure in this method comes solving Poisson's equation given the current carrier densities, then substitute the updated electric field values into the drift diffusion equation and solve these to update the for the new carrier density values. The new carrier densities values are substituted into Poisson's equation and the process is repeated until steady state is achieved.   Therefore at every time step $t_{k} \, \rightarrow \, t_{k+1}$ we preform one step of the Gummel iteration.


While the Gummel iteration method allows us to decouple Poisson's equation and the drift-diffusion equations, we are still presented with the challenge of solving the system of differential equations which are nonlinearly coupled at the interface. However, since we employ an explicit time stepping method, we effectively remove the nonlinearity by using the densities of the previous time steps. 


Due to the fact that the drift-diffusion-Poisson system is nonlinearly coupled, the step size $\Delta t$ to maintain $L_{2}$ stability of the carrier densities is not fixed, but rather a function of time.  We use an adaptive time stepping technique where we calculate $\Delta t$ at each time step.  We label the variables at time $t_{k}$ as $ \left(\,  \Phi^{k}, \, E^{k}, \, \rho_{n}^{k} \, , \, \rho_{p}^{k} \, , \, \rho_{r}^{k} \, , \, \rho_{o}^{k} \, \right)$.  We describe our algorithm below:


\vspace{3mm}


\noindent
\textbf{Gummel Algorithm}

\vspace{2mm}

\noindent
Set $k \, = \, 0, \; t_{0} \, = \, 0$.  

\vspace{2mm}

\noindent
Construct initial conditions $\left( \, \rho_{n}^{0} \, , \, \rho_{p}^{0} \, , \, \rho_{r}^{0} \, , \, \rho_{o}^{0} \, \right)$

\vspace{2mm}


\noindent
Compute the potential $\Phi^0$  and electric field $E^0$

\begin{align}
		- \frac{\partial}{\partial x} \, \left( \, \epsilon_{r} \frac{\partial}{\partial x}   \, \Phi^{\textcolor{red}{0}} \, \right) \;  &= \; f \left( \, \rho_{n}^{0} \, , \, \rho_{p}^{0} \, , \, \rho_{r}^{0} \, , \, \rho_{o}^{0} \, \right) \qquad \qquad \text{in} \qquad \left( -1, \, +1 \right) \nonumber \\ 
		\Phi^{k \, + \, 1} \quad  &= \quad 0 \qquad \qquad x \; = \; -1 \nonumber \\ 
		\llbracket \, \Phi^{\textcolor{red}{0}} \, \rrbracket \, = \, 0, \qquad & \qquad 
\llbracket \,  \epsilon_{r} \, E^{\textcolor{red}{0 }} \, \cdot \, \boldsymbol \eta \, \rrbracket \, = \, 0 \qquad \text{at} \quad \Sigma \nonumber  \\
		\Phi^{\textcolor{red}{0}} \quad &= \quad  \Phi_{\text{applied}} \qquad \qquad x \; = \; +1 \nonumber \\
		\end{align}

\noindent
Compute $\Delta \, t_{0}$ using the CFL condition:

	
	\begin{equation}
		\Delta \, t_{0} \quad = \quad \min_{i \; = \; n, \, p, \, r, \, o} \quad  \frac{2 \, D_{i}  }{\left[ \,  \mu_{i} \, \Vert \, \textcolor{red}{E^{0}} \,  \Vert_{L^{\infty}([-1, \, 1] )} \, + \,  \frac{ 2 \, D_{i} \,  (\, 1 \, + \, \mathcal{P}_{\text{DG}} \, )^{2}}{\Delta x} \, \right]^{2} }
	\end{equation}
	


\vspace{2mm}


\noindent  {\textcolor{green}{\% Begin Loop \%}}

\vspace{2mm}


\noindent
\textbf{while}  ($ \, t_{k } \, < \, T \, $),  ( \& for  $k\ge 0$ )

		\begin{enumerate}

		\item Compute $\Delta \, t_{k}$ using the CFL condition:
	
	\begin{equation}
		\Delta \, t_{k} \quad = \quad \min_{i \; = \; n, \, p, \, r, \, o} \quad  \frac{2 \, D_{i}  }{\left[ \,  \mu_{i} \, \Vert \, \textcolor{red}{E^{k}} \,  \Vert_{L^{\infty}([-1, \, 1] )} \, + \,  \frac{ 2 \, D_{i} \,  (\, 1 \, + \, \mathcal{P}_{\text{DG}} \, )^{2}}{\Delta x} \, \right]^{2} }
	\end{equation}
	
		
	
		\item Solve for the densities $ \left(\, \rho_{n}^{k \, + \, 1} \, , \, \rho_{p}^{k \, + \, 1} \, , \, \rho_{r}^{k \, + \, 1} \, , \, \rho_{o}^{k \, + \, 1} \, \right)$ using a forward Euler scheme
				
				
	\begin{align}
\rho^{k \, + \, 1}_{n} \; &= \;  \rho_{n}^{k} \, - \, \Delta t_{k} \,  \frac{\partial}{\partial x} \left( -\mu_{n}  \, E^{\textcolor{red}{k }} \,  \rho_{n}^{k} \, - \,  D_{n} \, \frac{\partial \rho_{n}^{k}}{ \partial x} \right)  \qquad \qquad \text{in} \qquad \left( -1, \, \Sigma \right) \nonumber \\ 
\rho^{k \, + \, 1}_{p} \; &= \;  \rho_{p}^{k} \, - \, \Delta t_{k} \, \frac{\partial}{\partial x} \left( \mu_{p}  \, E^{\textcolor{red}{k }} \,  \rho_{p}^{k}\, - \,  D_{p} \, \frac{\partial \rho_{p}^{k}}{ \partial x} \right) \qquad \qquad \text{in} \qquad \left( -1, \, \Sigma \right) \nonumber \\
\rho^{k \, + \, 1}_{r} \; &= \;  \rho_{r}^{k} \, - \, \Delta t_{k} \, \frac{\partial}{\partial x} \left( -\mu_{r}  \, E^{\textcolor{red}{k }} \,  \rho_{r}^{k}
\, - \,  D_{r} \, \frac{\partial \rho_{r}^{k}}{ \partial x} \right)  \qquad \text{in} \qquad \left(\Sigma , \, +1 \right) \nonumber \\
\rho^{k \, + \, 1}_{o } \; &= \;  \rho_{o}^{k} \, - \, \Delta t_{k} \, \frac{\partial}{\partial x} \left( \mu_{o}  \, E^{\textcolor{red}{k }} \,  \rho_{o}^{k}
\, - \,  D_{o} \, \frac{\partial \rho_{o}^{k}}{ \partial x} \right)\qquad \text{in} \qquad \left(\Sigma , \, +1 \right) \nonumber \\	
	\end{align}

	Subject to the boundary conditions:
	
	\begin{align}
	\rho_{p}^{k} \; &= \; N_{A} \qquad x = -1 \nonumber \\
\rho_{n}^{k}  \; &= \; N_{D} \qquad x = -1 \nonumber \\
\left( \, -\mu_{n}  \, E^{\textcolor{red}{k }} \,  \rho_{n}^{k}\, - \,  D_{n} \, \frac{\partial \rho_{n}^{k}}{ \partial x} \, \right) \quad &= \quad \left[ \, k_{et} \, ( \rho_{n}^{k} \, - \, \rho_{n}^{e} )  \; \rho_{o}^{k} \, \right] \qquad x \; = \; \Sigma  \nonumber \\
\left( \, \mu_{p}  \, E^{\textcolor{red}{k }} \,  \rho_{p}^{k}
\, - \,  D_{p} \, \frac{\partial \rho_{p}^{k} }{ \partial x} \, \right) \quad &= \quad \left[  \, k_{ht} ( \rho_{p}^{k} \, - \, \rho_{p}^{e} ) \, \rho_{r}^{k} \,  \right] \qquad x \; = \; \Sigma \nonumber \\
\left( \, -\mu_{r}  \, E^{\textcolor{red}{k }} \,  \rho_{r}^{k} \, - \,  D_{r} \, \frac{\partial \rho_{r}^{k } }{ \partial x} \, \right)  \quad &= \quad \left[ \, k_{ht} ( \rho_{p}^{k} \, - \, \rho_{p}^{e} ) \, \rho_{r}^{k} \, - \, k_{et} \, ( \rho_{n}^{k} \, - \, \rho_{n}^{e} ) \, \rho_{o} ^{k}  \, \right]  \qquad x \; = \; \Sigma \nonumber \\
\left( \, \mu_{o}  \, E^{\textcolor{red}{k }} \,  \rho_{o}^{k } \, - \,  D_{o} \, \frac{\partial \rho_{o}^{k}}{ \partial x} \, \right)  \quad &= \quad \left[ \,  -k_{ht} ( \rho_{p}^{k} \, - \, \rho_{p}^{e} ) \, \rho_{r}^{k} \, + \, k_{et} \, ( \rho_{n}^{k} \, - \, \rho_{n}^{e} ) \, \rho_{o}^{k}  \, \right] \qquad x \; = \; \Sigma \nonumber  \\
\rho_{r}^{k} \; &= \; \rho_{r}^{\infty} \qquad x = +1 \nonumber \\ 
 \rho_{o}^{k} \; &= \; \rho_{o}^{\infty} \qquad x = +1 \nonumber
	\end{align}

\


\item Solve Poisson's equation using time steps densities $\left( \, \rho_{n}^{k+1} \, , \, \rho_{p}^{k+1} \, , \, \rho_{r}^{k+1} \, , \, \rho_{o}^{k+1} \, \right)$:
		\begin{align}
		- \frac{\partial}{\partial x} \, \left( \, \epsilon_{r} \frac{\partial}{\partial x}   \, \Phi^{\textcolor{red}{k+1 }} \, \right) \;  &= \; f \left( \, \rho_{n}^{\textcolor{red}{k+1 }} \, , \, \rho_{p}^{\textcolor{red}{k+1 }} \, , \, \rho_{r}^{\textcolor{red}{k+1 }} \, , \, \rho_{o}^{\textcolor{red}{k+1 }} \, \right) \qquad \qquad \text{in} \qquad \left( -1, \, +1 \right) \nonumber \\ 
		\Phi^{\textcolor{red}{k+1 }} \quad  &= \quad 0 \qquad \qquad x \; = \; -1 \nonumber \\ 
		\llbracket \, \Phi^{\textcolor{red}{k+1 }} \, \rrbracket \, = \, 0, \qquad & \qquad 
\llbracket \,  \epsilon_{r} \, E^{\textcolor{red}{k +1}} \, \cdot \, \boldsymbol \eta \, \rrbracket \, = \, 0 \qquad \text{at} \quad \Sigma \nonumber  \\
		\Phi^{\textcolor{red}{k+1 }} \quad &= \quad  \Phi_{\text{applied}} \qquad \qquad x \; = \; +1 \nonumber \\
		\end{align}
		


 
\end{enumerate} 
 


\noindent  {\textcolor{green}{\% End Loop \%}}
 


\newpage


\section{Semi-discrete LDG Formulation}

\noindent
We formulate our weak problem using the inner product notation:

\vspace{2mm}

$$ (u, \,v )_{I_{i}} \quad = \quad \int_{x_{i-1/2}}^{x_{i+1/2}} \, u(x) \, v(x) \, dx, \qquad \text{where} \qquad I_{i} = [x_{i-1/2}, \, x_{i+1/2}] $$

\vspace{2mm}


\noindent
Taking our test functions $v_{n}, \, v_{p} \, \in L_{2}(-1, \, \Sigma)$ and $v_{r}, \, v_{o} \,  \in L_{2}(\Sigma, \, 1)$. 
Dividing our domain $[-1,  \, 1]$ up into $2N$ points, where the $x_{0-1/2} = -1$ and $x_{N+1/2} = 1$ and the interface occurs at $x_{N+1/2} = \Sigma$.  So that the interface is the right boundary of the $N-$th element and the left boundary of the $(N+1)-$th element.   Writing our drift diffusion equations in the mixed form for $\ell \, = \, n, \, p, \, r, \, o$ for a general carrier with charge sign $s_{\ell}$:

\begin{align}
\frac{\partial \rho_{\ell}}{\partial t} \, &+ \, \frac{\partial}{\partial x} \left( \, s_{\ell} \, \mu_{\ell} \, E \,  \rho_{\ell}
\, + \, q_{\ell} \, \right) \; = \; 0 \label{eq:primary} \\
q_{\ell} \, &+ \, D_{\ell} \, \frac{\partial \rho_{\ell}}{\partial x} \; = \; 0 \label{eq:auxillary}
\end{align}


\noindent
We formulate the weak form of our equation just the primary variables \eqref{eq:primary} and not those for the auxillary variables \eqref{eq:auxillary}.  For the semiconductor $[-1, \, \Sigma]$ we have:


\begin{align} \label{eq:weakElectrons}
(v_{n} \, , \, \partial_{t} \, \rho_{n})_{I_{i}} \, - \, (\partial_{x} \, v_{n} \, , \, -\mu_{n} \, E \, \rho_{n} \, + \, q_{n} \, )_{I_{i}} \, &+ \, ( \, v_{n} \, )_{i+1/2}^{-} (-\mu_{n} \, \widehat{E \rho_{n}})_{i+1/2} \, - \, ( \, v_{n} \, )_{i-1/2}^{-} (-\mu_{n} \, \widehat{E \rho_{n}} )_{i-1/2} \, \nonumber \\
&+ \, ( \, v_{n} \, )_{i+1/2}^{-} (\widehat{q_{n}})_{i+1/2} \, - \, ( \, v_{n} \, )_{i-1/2}^{-} (\widehat{q_{n}} )_{i-1/2} \; = \; 0
\end{align}

\begin{align} \label{eq:weakHoles}
(v_{p}\, , \, \partial_{t} \, \rho_{p})_{I_{i}} \, - \, (\partial_{x} \, v_{p}\, , \, \mu_{p}\, E \, \rho_{p}\, + \, q_{p}\, )_{I_{i}} \, &+ \, ( \, v_{p} \, )_{i+1/2}^{-} (\mu_{p}\, \widehat{E \rho_{p}})_{i+1/2} \, - \, ( \, v_{p} \, )_{i-1/2}^{-} (\mu_{p}\, \widehat{E \rho_{p}} )_{i-1/2} \, \nonumber \\
&+ \, ( \, v_{p} \, )_{i+1/2}^{-} (\widehat{q_{p}})_{i+1/2} \, - \, ( \, v_{p} \, )_{i-1/2}^{-} (\widehat{q_{p}} )_{i-1/2} \; = \; 0
\end{align}

\noindent
For the electrolyte $[\Sigma, \, +1]$ we have:

\begin{align} \label{eq:weakReductants}
(v_{r}\, , \, \partial_{t} \, \rho_{r})_{I_{i}} \, - \, (\partial_{x} \, v_{r}\, , \, -\mu_{r}\, E \, \rho_{r}\, + \, q_{r}\, )_{I_{i}} \, &+ \, ( \, v_{r} \, )_{i+1/2}^{-} (-\mu_{r}\, \widehat{E \,\rho_{r}})_{i+1/2} \, - \, ( \, v_{r} \, )_{i-1/2}^{-} (-\mu_{r}\, \widehat{E \, \rho_{r}} )_{i-1/2} \, \nonumber \\
&+ \, ( \, v_{r} \, )_{i+1/2}^{-} (\widehat{q_{r}})_{i+1/2} \, - \, ( \, v_{r} \, )_{i-1/2}^{-} (\widehat{q_{r}} )_{i-1/2} \; = \; 0
\end{align}

\begin{align} \label{eq:weakOxidants}
(v_{o}\, , \, \partial_{t} \, \rho_{o})_{I_{i}} \, - \, (\partial_{x} \, v_{o}\, , \, \mu_{o}\, E \, \rho_{o}\, + \, q_{o}\, )_{I_{i}} \, &+ \, ( \, v_{o} \, )_{i+1/2}^{-} (\mu_{o}\, \widehat{E \rho_{o}})_{i+1/2} \, - \, ( \, v_{o} \, )_{i-1/2}^{-} (\mu_{o}\, \widehat{E \rho_{o}} )_{i-1/2} \, \nonumber \\
&+ \, ( \, v_{o} \, )_{i+1/2}^{-} (\widehat{q_{o}})_{i+1/2} \, - \, ( \, v_{o} \, )_{i-1/2}^{-} (\widehat{q_{o}} )_{i-1/2} \; = \; 0
\end{align}

\newpage

\section{Semi-discrete LDG Fomulation With Interface}


\noindent
On the interior of our domain, we will have the same formulation as above, however near the boundaries or interface we must change the fluxes to enforce the conditions.


\vspace{2mm}

\noindent
\textbf{Semiconductor side}
On the Semiconductor side we will have the interface occur at the right end point of the $N-$th element or at $x_{N+1/2}$.

\vspace{2mm}

\noindent
For $i \, \neq \, N$ we are away from the interface and can define the fluxes using upwinding and LDG fluxes:

\begin{align} 
(v_{n} \, , \, \partial_{t} \, \rho_{n})_{I_{i}} \, - \, (\partial_{x} \, v_{n} \, , \, -\mu_{n} \, E \, \rho_{n} \, + \, q_{n} \, )_{I_{i}} \, &+ \, 
\textcolor{blue}{( \, v_{n} \, )_{i+1/2}^{-} (-\mu_{n} \, \widehat{E \rho_{n}})_{i+1/2} } \, - \, (\, v_{n} \, )_{i-1/2}^{+} (\, -\mu_{n} \, \widehat{E \, \rho_{n}} \, )_{i-1/2} \, \nonumber \\
&+ \, 
\textcolor{blue}{ ( \, v_{n} \, )_{i+1/2}^{-} \, ( \,\widehat{q_{n}} \,)_{i+1/2}}
\, - \, ( \, v_{n} \, )_{i-1/2}^{+} \, ( \, \widehat{q_{n}} \, )_{i-1/2} \; = \; 0
\end{align}

\begin{align} 
(v_{p}\, , \, \partial_{t} \, \rho_{p})_{I_{i}} \, - \, (\partial_{x} \, v_{p}\, , \, \mu_{p}\, E \, \rho_{p}\, + \, q_{p} \, )_{I_{i}} \, &+ \, 
\textcolor{blue}{( \, v_{p} \, )_{i+1/2}^{-} \, (\, \mu_{p}\, \widehat{E \,\rho_{p} }\, )_{i+1/2} }\, - \, ( \, v_{p} \, )_{i-1/2}^{+} \, (\, \mu_{p}\, \widehat{E \,  \rho_{p}} \, )_{i-1/2} \, \nonumber \\
&+ \, 
\textcolor{blue}{( \, v_{p} \, )_{i+1/2}^{-} (\, \widehat{q_{p}} \, )_{i+1/2} }
\, - \, ( \, v_{p} \, )_{i-1/2}^{+} (\, \widehat{q_{p}} \, )_{i-1/2} \; = \; 0
\end{align}



\vspace{2mm}

\noindent
For $i = N$ we replace the fluxes at the boundary of the interface (blue) with the interface conditions (red).


\begin{align} 
(v_{n} \, , \, \partial_{t} \, \rho_{n})_{I_{i}} \, - \, (\partial_{x} \, v_{n} \, , \, -\mu_{n} \, E \, \rho_{n} \, + \, q_{n} \, )_{I_{i}} \, &- \, ( \, v_{n} \, )_{i-1/2}^{+} (\, -\mu_{n} \, \widehat{E \, \rho_{n}} \, )_{i-1/2} \,
- \, ( \, v_{n} \, )_{i-1/2}^{+} \, ( \, \widehat{q_{n}} \, )_{i-1/2} \; \nonumber \\
&+ \textcolor{red}{ ( \, v_{n} \, )^{-}_{N+1/2} \, k_{et} \, \left[ \, (\rho_{n})_{N+1/2} \, - \, \rho_{n}^{e} \right]  \, (\rho_{o}^{j-1})_{N+1/2} }  \; = \; 0
\end{align}

\begin{align} 
(v_{p}\, , \, \partial_{t} \, \rho_{p})_{I_{i}} \, - \, (\partial_{x} \, v_{p}\, , \, \mu_{p}\, E \, \rho_{p}\, + \, q_{p} \, )_{I_{i}} \, &- \, ( \, v_{p} \, )_{i-1/2}^{+} \, (\, \mu_{p}\, \widehat{E \,  \rho_{p}} \, )_{i-1/2} 
\, - \, ( \, v_{p} \, )_{i-1/2}^{+} (\, \widehat{q_{p}} \, )_{i-1/2} \; = \; 0 \nonumber \\
&+ \textcolor{red}{ ( \, v_{p} \, )^{-}_{N+1/2} \, k_{ht} \, \left[ \, (\rho_{p})_{N+1/2} 
- \, \rho_{p}^{e} \right]  \, (\rho_{r}^{j-1})_{N+1/2} }  \;= \; 0 
\end{align}

\vspace{5mm}

\noindent
\textbf{Electrolyte side}
On the electrolyte side we will have the interface occur at the left end point of the $(N+1)-$th element or at $x_{(N+1)-1/2}$.

\vspace{2mm}

\noindent
For $i \, \neq \, N+1$ we are away from the interface and can define the fluxes using upwinding and LDG fluxes:

\begin{align} 
(v_{r} \, , \, \partial_{t} \, \rho_{r})_{I_{i}} \, - \, (\partial_{x} \, v_{r} \, , \, -\mu_{r} \, E \, \rho_{r} \, + \, q_{r} \, )_{I_{i}} \, &+ \, (\, v_{r} \,)_{i+1/2}^{-} (-\mu_{r} \, \widehat{E \rho_{r}})_{i+1/2}  \, - \, \textcolor{blue}{(\, v_{r} \,)_{i-1/2}^{+} (\, -\mu_{r} \, \widehat{E \, \rho_{r}} \, )_{i-1/2} }\, \nonumber \\
&+ \, 
(\, v_{r} \,)_{i+1/2}^{-} \, ( \,\widehat{q_{r}} \,)_{i+1/2}
\, - \, \textcolor{blue}{ (\, v_{r} \,)_{i-1/2}^{+} \, ( \, \widehat{q_{r}} \, )_{i-1/2}} \; = \; 0
\end{align}

\begin{align} 
(v_{o}\, , \, \partial_{t} \, \rho_{o})_{I_{i}} \, - \, (\partial_{x} \, v_{o}\, , \, \mu_{o}\, E \, \rho_{o}\, + \, q_{o} \, )_{I_{i}} \, &+ \, 
(\, v_{o} \,)_{i+1/2}^{-} \, (\, \mu_{o}\, \widehat{E \,\rho_{o} }\, )_{i+1/2} \, - \, \textcolor{blue}{(\, v_{o} \,)_{i-1/2}^{+} \, (\, \mu_{o}\, \widehat{E \,  \rho_{o}} \, )_{i-1/2} }\, \nonumber \\
&+ \, 
(\, v_{o} \,)_{i+1/2}^{-} (\, \widehat{q_{o}} \, )_{i+1/2} 
\, - \, \textcolor{blue}{(\, v_{o} \,)_{i-1/2}^{+} (\, \widehat{q_{o}} \, )_{i-1/2}} \; = \; 0
\end{align}



\vspace{2mm}

\noindent
For $i = N+1$ we replace the fluxes at the boundary of the interface (blue) with the interface conditions (red).

\begin{align} 
(v_{r}\, , \, \partial_{t} \, \rho_{r})_{I_{N+1}} \, &- \, (\partial_{x} \, v_{r}\, , \, -\mu_{r}\, E \, \rho_{r}\, + \, q_{r} \, )_{I_{N+1}} \, 
+ \, (\, v_{r} \,)_{(N+1)+1/2}^{-} \, (\, \mu_{r}\, \widehat{E \,\rho_{r} }\, )_{(N+1)+1/2} \, \nonumber \\
&+ (\, v_{r} \,)_{(N+1)+1/2}^{-} (\, \widehat{q_{r}} \, )_{(N+1)+1/2} \nonumber \\
&- \textcolor{red}{ (\, v_{r} \,)_{(N+1)-1/2}^{+} \, \left[ \, +k_{ht} \, (\, \rho_{p} \, - \, \rho_{p}^{e} \,) \, \rho_{r} \, - \, k_{et} \, ( \,\rho_{p} \, - \, \rho_{p}^{e} ) \, \rho_{o} \,  \right]_{(N+1)-1/2} }
\; = \; 0
\end{align}

\begin{align} 
(v_{o}\, , \, \partial_{t} \, \rho_{o})_{I_{N+1}} \, &- \, (\partial_{x} \, v_{o}\, , \, \mu_{o}\, E \, \rho_{o}\, + \, q_{o} \, )_{I_{N+1}} \, 
+ \, (\, v_{o} \,)_{(N+1)+1/2}^{-} \, (\, \mu_{o}\, \widehat{E \,\rho_{o} }\, )_{(N+1)+1/2} \, \nonumber \\
&+ (\, v_{o} \,)_{(N+1)+1/2}^{-} (\, \widehat{q_{o}} \, )_{(N+1)+1/2} \nonumber \\
&- \textcolor{red}{ (\, v_{o} \,)_{(N+1)-1/2}^{+} \, \left[ \, - \, k_{ht} \, (\, \rho_{p} \, - \, \rho_{p}^{e} \,) \, \rho_{r} \, + \, k_{et} \, ( \,\rho_{p} \, - \, \rho_{p}^{e} ) \, \rho_{o} \,  \right]_{(N+1)-1/2} }
\; = \; 0
\end{align}



\newpage






\end{document}
