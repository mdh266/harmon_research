\documentclass[10pt]{report}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fullpage}
\usepackage{graphicx}
\numberwithin{equation}{section}


\begin{document}

\chapter{Finite Elements Methods For Poisson's Equation}


\section{Finite Element Methods}


\noindent
The finite element method is a method for approximating solutions to
boundary value problems that has gained much popularity in the realm of 
numerical methods for partial differential equations.  While finite element methods can be applied to a range of boundary value problems we focus our attention on Poisson's equation:

\begin{equation}\label{eq:Poisson}
-\nabla \, \cdot \, \left(\epsilon \, \nabla \, \Phi \right) \, = \, f \qquad \text{in} \quad \Omega
\end{equation}


\noindent
Where $\epsilon(x)$ is bounded positive-definite function. And the boundary conditions are:


\begin{align}
\Phi \, &= \, \Phi_{D} \qquad \text{on} \quad \partial \Omega_{D} \label{eq:DirichletBC} \\
-\nabla \Phi \, \cdot \, \boldsymbol \eta  &= 0 \qquad \text{on} \quad \partial \Omega_{N} \label{eq:NeumannBC} \\
 \partial \Omega = \partial \Omega_{D} \, \cup \, \partial \Omega_{N} \quad & \quad \partial \Omega_{D} \, \cap \, \partial \Omega_{N} = \emptyset
\end{align}


\noindent
Where $\partial \Omega_{D}$ correspond to contacts or interfaces where potentials (voltages) are applied and $\partial \Omega_{N}$ corresponds to oxides that act as insulators to charge flow.



\vspace{2mm}


\noindent
Finite element methods seek weak solutions of differential equation $\Phi \in H^{1}(\Omega)$ by multiplying our equation by a test function $v \in H^{1}(\Omega)$ and integrating by parts over entire the domain

$$ \int_{\Omega} \, - v \, \nabla  \, \cdot \, \left(\epsilon \, \nabla \, \Phi \right) \, dx \, =  \, \int_{\Omega} \, v \, f \, dx $$

\noindent
Becomes


\vspace{2mm}


\begin{equation}
\int_{\Omega} \, \nabla  \, v  \, \cdot \, \left(\epsilon \, \nabla \, \Phi \right) \, \, dx \, 
-\int_{\partial \Omega_{D}} \, v \, \, \left(\epsilon \, \nabla \, \Phi \, \cdot \, \boldsymbol \eta \right) \, dx \,  
-\int_{\partial \Omega_{N}} \, v \, \, \left(\epsilon \, \nabla \, \Phi \, \cdot \, \boldsymbol \eta \right) \, dx \,  
=  \, \int_{\Omega} \, v \, f \, dx 
\end{equation}


\vspace{2mm}


\noindent
Using the condition of \eqref{eq:NeumannBC} we have require the Neumann boundary condition to be zero:


\vspace{2mm}


$$
\int_{\Omega} \,  \,  \nabla \, v \, \cdot \, \left(\epsilon \, \nabla \, \Phi \right) \,  dx \, 
-\int_{\partial \Omega_{D}} \,  \,  v \,\left(\epsilon \, \nabla \, \Phi \, \cdot \, \boldsymbol \eta \right) \,dx \,  
=  \, \int_{\Omega} \, v \, f \,   dx 
$$

\noindent
In order to make our problem well-defined, we require that $v = 0$ on $\partial \Omega_{D}$.\footnote{In finite element methods, Neumann (natural) boundary conditions are enforced by the formulation of problem, while Dirichlet (essential) boundary conditions are enforced by restricting our test space.}  Poisson's equation then becomes:


\begin{center}
Find $\Phi-\Phi_{D} \in V = \{ \phi \in H^{1}(\Omega): \phi = 0 \quad \text{on} \quad \partial \Omega_{D} \}$. Such that for all $v \, \in \, V$ we have:
\end{center}


\begin{equation}
\int_{\Omega} \, \epsilon(x) \, \left(  \nabla \, \Phi \cdot \,
 \nabla \, v \, \right) \, dx \, 
=  \, \int_{\Omega} \,  f \, v \, dx 
\end{equation}

\vspace{3mm}

\noindent
In order to solve this problem we must project our infinite dimensional test space $V$ on to a finite dimensional one $V_{h} = \text{span}\{\psi_{1}, \psi_{2}, \ldots , \psi_{N}\}$, where $N$ is the dimension of our vector space, our problem then transforms into the finite dimensional problem:

\begin{center}
Find $\Phi_{h}-\Phi_{D} \in V_{h}$ such that $\forall \,  v_{h} \, \in  V_{h}$:
\end{center}

\begin{equation}\label{eq:PoissonWeakForm}
\int_{\Omega} \, \epsilon(x) \, \left(  \nabla \, v_{h} \, \cdot \,  \nabla  \, \Phi_{h} \, 
\right) \, dx \, 
=  \, \int_{\Omega}  \, v_{h} \,  f \, dx 
\end{equation}

\vspace{3mm}


\noindent
We can rewrite this in bilinear the form of 

\begin{center}
Find $\Phi_{h}-\Phi_{D} \in V_{h}$ such that $\forall \,  v_{h} \, \in  V_{h}$:
\end{center}

\begin{equation}\label{eq:PoissonBilinear}
a \left( v_{h} \, , \Phi_{h} \, \right) \,  
=  F(v_{h})
\end{equation}


\noindent
Where 

$$a\left( \, v_{h} \, , \Phi_{h} \, \right) \, =
  \int_{\Omega} \, \epsilon(x) \, \left( \nabla \, v_{h} \, ,  \nabla  \, \Phi_{h} \,
 \right) \, dx \, $$ 
 
 
\noindent
And 


$$ F(v_{h})  =  \, \int_{\Omega} \, v_{h} \,  f  \, dx $$


\noindent
By writing defining $\Phi_{D} = \sum \, \Phi_{j} \, \psi_{j}$ and $v = \psi_{i}$ we arrive at linear system of equations:

$$ A^{T} \textbf{x} \, = \, \textbf{F} $$


\noindent
Where 

$$ A_{i,j} \, = \,   \int_{\Omega} \, \epsilon(x) \, \left(  \nabla  \, \psi_{i} \, \cdot \, \nabla \, \psi_{j} \right) \, dx \, $$


$$ \textbf{F}_{i} \, =  \, \int_{\Omega} \,  \psi_{i} \,  f \,dx $$


$$ \textbf{x}_{i} \, = \Phi_{i} $$



\noindent
In order existence and uniqueness of this problem we invoke the \textit{Lax-Milgram theorem}, and show $a$ is bounded by Cauchy-Schwarts:



\begin{align*}
|a(\psi, \Phi)| &= |\int_{\Omega} \epsilon(x) \nabla \psi \cdot \nabla \Phi \, dx |  \\
&\leq \Vert \epsilon \Vert_{L_{\infty}} \, \Vert \nabla \psi \Vert|^{2}_{L_{2}(\Omega)} \, \Vert \nabla \Phi \Vert^{2}_{L_{2}} \,  \\
&\leq \Vert \epsilon \Vert_{L_{\infty}} \,  \Vert  \psi \Vert|_{H^{1}(\Omega)} \, \Vert \Phi \Vert_{H^{1}(\Omega)} 
\end{align*}


\noindent
And coercivity by Poincare's inequality:

\begin{align*}
|a(\Phi, \Phi)| &= \, |\int_{\Omega} \epsilon(x) \, \left( \nabla \Phi \right)^{2}, dx |  \\
&\geq \,\text{inf}_{\Omega} | \epsilon | \, \Vert \nabla \Phi \Vert^{2}_{L_{2}(\Omega)} \\
&\geq \, \text{inf}_{\Omega} | \epsilon | \, \Vert \Phi \Vert_{H^{1}(\Omega)}^{2}
\end{align*}



\vspace{5mm}



\section{Mixed Finite Element Methods}


\noindent
The problem with the general finite element approximation of $\Phi$ is that $\nabla \Phi$ must come from post processing and we cannot control the accuracy or continuity of $\nabla \Phi$.  This is necessary not only for use in solving the drift diffusion equation, but also when we are working with a semiconductor-electrolyte system as will be explained in the next section.


\vspace{3mm}


\noindent
One way to overcome these limitations is to use a mixed finite element approach to solve Poisson's equation.  In the mixed formulation we are solving the problem as in \eqref{eq:Poisson}, but we define define the a new variable related to the electric field as:


$$ \textbf{E}= -\epsilon \, \nabla \, \Phi $$


\noindent
Then \eqref{eq:Poisson} becomes the system:


\begin{align}
\nabla \, \cdot \, \textbf{E} \, &= f \label{eq:DivEq}\\
\epsilon^{-1} \, \textbf{E} + \nabla \, \Phi &= 0 \label{eq:Efield}
\end{align}


\noindent
With the boundary conditions:


\begin{align}
\Phi \, &= \, \Phi_{D} \qquad \text{on} \quad \partial \Omega_{D} \\ 
\textbf{E} \cdot \boldsymbol \eta &= 0 \qquad \text{on} \quad \partial \Omega_{N} \label{eq:SystemNeumannBC} \\
 \partial \Omega = \partial \Omega_{D} \, \cup \, \partial \Omega_{N} \quad & \quad \partial \Omega_{D} \, \cap \, \partial \Omega_{N} = \emptyset
\end{align}


\vspace{2mm}


\noindent
Now, mixed finite element methods seek weak solutions of both $\Phi$ and $\textbf{E}$ by multiplying our \eqref{eq:DivEq} by a test function $v \in L_{2}(\Omega)$ \eqref{eq:Efield} by a test function $\textbf{p} \in H(\text{div}, \Omega)$ then integrating by parts over entire the domain for just \eqref{eq:Efield}:



  
\begin{align}
&\int_{\Omega}  \, v \, \left( \nabla  \, \cdot \, \textbf{E} \right) \, dx \, =  \, \int_{\Omega}  \,  v \, f \, dx  \\
&\int_{\Omega} \,  \textbf{p} \, \cdot \,  \epsilon^{-1} \, \textbf{E} \, dx \, 
+ \, \int_{\partial \Omega_{N}} \, \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right) \, \Phi  \, dx  \,
+ \, \int_{\partial \Omega_{D}} \, \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right) \, \Phi  \, dx  
-\int_{\Omega} \,  \left(\nabla \, \cdot \, \textbf{p} \right) \, \Phi \, dx = 0
\end{align}


\noindent
If we enforce the boundary conditions and the restrict $\textbf{p}
\cdot \boldsymbol \eta = 0$, on $\partial \Omega_{N}$, so that \textbf{p} and \textbf{E} are in the space function space, then we have the weak formulation:


\vspace{2mm}

\begin{center}
Find $(\Phi \, , \textbf{E} \, )\, \in \, L_{2}(\Omega) \, 
\times\, V$ such that for all $(v, \textbf{p}) \, \in \, L_{2}(\Omega) \, 
\times\, V$:
\end{center}


\begin{align}
&\int_{\Omega} \, v  \, \left( \nabla  \, \cdot \, \textbf{E} \right) \, dx \, =  \, \int_{\Omega} \, v  \, f \, dx  \\
&\int_{\Omega} \, \textbf{p} \, \cdot \, \epsilon^{-1} \, \textbf{E} \, dx \, 
+ \int_{\Omega} \, \left(\nabla \, \cdot \, \textbf{p} \right)\, \Phi \, dx 
\, = \, 
\int_{\partial \Omega_{N}} \, \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right) \, \Phi_{D} \,  \, dx  \, 
\end{align}


\vspace{2mm}


\noindent
Where $V  \, = \,  \{ \, \boldsymbol \phi \, \in \, H(\text{div}\,; \, \Omega) : \, \boldsymbol \phi \, \cdot \, \boldsymbol \eta = 0 \quad \text{on} \quad \partial \Omega_{N} \} $


\vspace{2mm}


\noindent
Now we wish to project our functions onto a finite dimensional vector space.  However, we allow $\Phi$ and \textbf{E} to be in different vector spaces. Therefore we write:

\vspace{2mm}


$$ \Phi = \sum_{J} \Phi_{J} \, \psi_{J} ,  \qquad \textbf{E} = \sum_{J} E_{J} \boldsymbol \Upsilon_{J} $$


\vspace{2mm}


\noindent
Where $\psi \in L_{2}(\Omega)$ and we enforce that $\boldsymbol \Upsilon \in V$ be continuous across element nodes.  We also take $\textbf{p} = \boldsymbol \Upsilon_{I}$ and $v = \psi_{I}$ this changes the weak formulation into:


$$ \sum_{I} E_{I} \, \underbrace{\left( \int_{\Omega} (\nabla \cdot \boldsymbol \Upsilon_{I}) \,
 \psi_{J} \, dx \right)}_{A_{10}} = 
\underbrace{\int_{\Omega} \,f \, \psi_{J} \, dx}_{F}$$


\noindent
And

$$  \epsilon^{-1} \, \sum_{I} E_{i }\underbrace{\int_{\Omega} \boldsymbol \Upsilon_{I} \cdot \boldsymbol \Upsilon_{J} \, dx}_{A_{00}}
+\sum_{I} \Phi_{I} \,  \underbrace{\int_{\Omega} \, \psi _{I}
\, \left( \nabla \cdot \boldsymbol \Upsilon_{J} \right)}_{A_{01}}
=  \, \underbrace{\int_{\partial \Omega} \, \Phi_{D} \, \boldsymbol \Upsilon_{I}
\cdot \, \boldsymbol \, \boldsymbol \eta \, ds}_{V} = 0
$$

\vspace{2mm}

\noindent
Then we write the two linear systems as one larger linear system:


\vspace{2mm}


\begin{equation}
\left[ \begin{matrix}
 A_{10} & 0 \\
\epsilon^{-1} A_{00} & -A_{01} \\
\end{matrix} \right]
\left[ \begin{matrix}
\Phi_{DOF} \\
E_{DOF}
\end{matrix} \right] =
\left[ \begin{matrix}
0 \\
F  \\      
\end{matrix}\right]  + 
\left[ \begin{matrix}
V\\
0 \\
\end{matrix} \right]
\end{equation}

\vspace{2mm}

\noindent
However, this time we cannot use the \textit{Lax-Milgram theorem} to prove existence and uniqueness of the solutions.


\newpage


\section{Interface Conditions}


\noindent
Now we are facing a situation when we have to solve Poisson's equation for both $\Phi$ and $\boldsymbol E$ but where $\epsilon$ and $f$ have values/forms on each subdomain.  This is shown below:

%
%\begin{center}
%\includegraphics[scale=0.5]{../images/domaindecomp.png}
%\end{center}


\noindent
In the first domain we have that:


\begin{equation}
-\nabla \, \cdot \, \left(\epsilon_{1} \, \nabla \, \Phi \right) \, = \, f_{1} \qquad \text{in} \quad \Omega_{1}
\end{equation}

\noindent
With boundary conditions,

\begin{align}
\Phi \, &= \, \Phi_{1,D} \qquad \text{on} \quad \partial \Omega_{1,D}  \nonumber \\
-\nabla \Phi \, \cdot \, \boldsymbol \eta  &= 0 \qquad \text{on} \quad \partial \Omega_{1,N} \nonumber \\
\partial \Omega_{1} &= \partial \Omega_{1,D} \, \cup \, \partial \Omega_{1,N} \quad \quad \partial \Omega_{1,D} \, \cap \, \partial \Omega_{1,N} = \emptyset
\end{align}


\noindent
And in the second domain we have:


\begin{equation}
-\nabla \, \cdot \, \left(\epsilon_{2} \, \nabla \, \Phi \right) \, = \, f_{2} \qquad \text{in} \quad \Omega_{2}
\end{equation}

\noindent
With boundary conditions,


\begin{align}
\Phi \, &= \, \Phi_{2,D} \qquad \text{on} \quad \partial \Omega_{2,D}  \nonumber \\
-\nabla \Phi \, \cdot \, \boldsymbol \eta  &= 0 \qquad \text{on} \quad \partial \Omega_{2,N} \nonumber \\
\partial \Omega_{2} &= \partial \Omega_{2,D} \, \cup \, \partial \Omega_{2,N} \quad \quad \partial \Omega_{2,D} \, \cap \, \partial \Omega_{2,N} = \emptyset
\end{align}


\noindent
And the interface conditions 

\begin{align}
\Sigma \, &= \, \partial \Omega_{1} \cap \Omega_{2} \nonumber \\
\llbracket \, \Phi \, \rrbracket \, &= \, 0 \qquad \text{on} \quad \Sigma\nonumber \\
\llbracket \,  \epsilon_{i} \, \nabla \, \Phi \, \cdot \, \boldsymbol \eta \rrbracket \, &= \, 0 \qquad \text{on} \quad \Sigma\nonumber \nonumber
\end{align}


\vspace{2mm}


\noindent
We formulate a mixed method finite element method for solving this problem  for $i = 1, 2$:


\begin{align}
\nabla \, \cdot \, \textbf{D} \quad &=  \quad f_{i} \qquad \text{in} \quad \Omega_{i} \nonumber \\
 \textbf{D} \; + \;  \epsilon_{i} \, \nabla \, \Phi \quad &= \quad 0 \qquad  \text{in} \quad \Omega_{i} \nonumber \\
\textbf{E} \quad &= \quad \epsilon^{-1}_{i} \, \textbf{D} \nonumber \\
\end{align}


\noindent
With the boundary/interface conditions:

\begin{align}
\Phi \, &= \, \Phi_{1,D} \qquad \text{on} \quad \partial \Omega_{1,D}  \nonumber \\
\Phi \, &= \, \Phi_{2,D} \qquad \text{on} \quad \partial \Omega_{2,D}  \nonumber \\
\textbf{D} \cdot \boldsymbol \eta &= 0 \qquad \text{on} \quad \partial \Omega_{1,N} \cup \Omega_{2,N} \nonumber \\ 
\llbracket \, \Phi \, \rrbracket \, &= \, 0 \qquad \text{on} \quad \Sigma \nonumber \\
\llbracket \, \textbf{D} \, \cdot \, \boldsymbol \eta \, \rrbracket \, &= \, 0 \qquad \text{on} \quad \Sigma\nonumber \nonumber
\end{align}


\vspace{2mm}


\noindent
Now, as before we seek weak solutions of both $\Phi$ and $\textbf{D}$ by multiplying our equations by test functions $v \in L_{2}(\Omega)$ and  $\textbf{p} \in H(\text{div}, \Omega)$ then integrating by parts over entire the domain


\vspace{2mm}

  
\begin{align}
\int_{\Omega_{1} \cup \Omega_{2}} \, v \, \nabla  \, \cdot \textbf{E}  \, dx \, &=  \, \int_{\Omega_{1} \cup \Omega_{2}} \, v \, f_{i}  \, dx  \\
\int_{\Omega_{1} \cup \Omega_{2}} \, \textbf{p} \, \cdot \, \textbf{D} \, \, dx \, 
&+ \, \int_{\partial \Omega_{1,N} \cup \partial \Omega_{2,N} } \, \ \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right) \, Phi \, dx 
+ \, \int_{\partial \Omega_{1,D}} \, \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right)  \, \Phi_{1,D} \, dx  \nonumber \\
&+ \int_{\partial \Omega_{2,D}} \, \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right)  \, \Phi_{2,D} \, dx  
+ \int_{\Sigma} \llbracket \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right) \, \Phi \rrbracket  \, dx  \nonumber \\
&-\int_{\Omega_{1} \cup \Omega_{2}} \,  \left(\nabla \, \cdot \, \textbf{p} \right) \, \Phi \, dx \,  = \, 0
\end{align}

\vspace{2mm}

\noindent
Restricting $\textbf{p}$ such that $\textbf{p} \in V = \left\{ \textbf{q}
\in H(\text{div}, \Omega): \quad \textbf{q} \cdot \boldsymbol \eta = 0 \, \text{ on } \, \partial \Omega_{N} \, \text{ and } \, \llbracket \, \textbf{q} \, \cdot \, \boldsymbol \eta \, \rrbracket = 0 \, \text{ on } \ \Sigma \right\}$.  Then \textbf{p} and \textbf{D} are in the space function space and have the weak formulation:


\vspace{2mm}

\begin{center}
Find $(\Phi \, , \textbf{D} \, )\, \in \, L_{2}(\Omega) \, 
\times\, V$ such that for all $(v, \textbf{p}) \, \in \, L_{2}(\Omega) \, 
\times\, V$:
\end{center}


\begin{align}
&\int_{\Omega_{1} \cup \Omega_{2}} \, v \, \nabla  \, \cdot \textbf{D} \, dx \, =  \, \int_{\Omega_{1} \cup \Omega_{2}}  \, v  \, f_{i}\, dx  \nonumber \\
&\int_{\Omega_{1} \cup \Omega_{2}} \, \textbf{p} \, \cdot \, \textbf{D} \,  dx \,
-\int_{\Omega_{1} \cup \Omega_{2}} \, \left(\nabla \, \cdot \, \textbf{p} \right) \, \Phi  \, dx \,  = 
- \int_{\partial \Omega_{1,D}} \, \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right) \, \Phi_{1,D}  \, dx 
- \int_{\partial \Omega_{2,D}}\, \left( \textbf{p} \, \cdot \, \boldsymbol \eta \right)  \, \Phi_{2,D} \, dx  \nonumber
\end{align}


\vspace{2mm}

\noindent
As can be seen, the formulation of our problem is actually the same as the case without the interface. The only difference is change in the right hand side of system, and the new restriction that our basis functions for $V$ must be continuous across the interface.






\end{document}

  