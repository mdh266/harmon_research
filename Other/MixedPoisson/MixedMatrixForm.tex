\documentclass[10pt]{report}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fullpage}
\usepackage{graphicx}
\numberwithin{equation}{section}

\begin{document}


\chapter{Mixed Method Matrix Calculations}


\section{Problem}

\noindent
Solving the Poisson's equation for the potential:


\begin{equation} \label{eq:PoissonOrig}
-\frac{\partial}{\partial x} \left( \epsilon_{k} \, \frac{\partial}{\partial x} \Phi(x,t) \right) \quad = \quad  f
\end{equation}

\noindent
Over the interval $[a,b]$ where

\begin{equation}
\epsilon_{k} \quad = \left\{ \begin{array}{lc}
\epsilon_{1} \qquad & \text{if} \quad x \; \in \; [a, \, \Sigma) \\
\epsilon_{2} \qquad & \text{if} \quad x \;  \in \; (\Sigma, \, b] 
\end{array} \right.
\end{equation}

 

\noindent
With the boundary conditions:


\begin{equation}
\Phi(a,t) \quad = \quad  \Phi_{\text{left}}(t), \qquad \Phi(b,t) \quad  = \quad \Phi_{\text{right}}(t)
\end{equation}


\noindent
And the interface conditions that:

\begin{align}
& \left[\, \Phi \, \right]_{\Sigma} \quad = \quad 0 \nonumber \\
& \left[ \, \epsilon_{k} \frac{\partial \Phi}{\partial x} \, \right]_{\Sigma} \quad = \quad 0 \nonumber \\
\end{align}


\vspace{3mm}

\noindent
We define the ``displacement electric field" as \footnote{We use quotation marks since the real displacement field is $\textbf{D} \; = \; \epsilon_{r} \, \epsilon_{0} \, \textbf{E}$.}


\begin{equation}
\textbf{D}(x,t)  \; =  \; - \epsilon_{k} \, \frac{\partial}{\partial x} \Phi(x,t)
\end{equation}

\noindent
And the electric field as:

\begin{equation}
\textbf{E} = \epsilon^{-1}_{k} \, \textbf{D}
\end{equation}


\vspace{2mm}


\noindent
Now we reformulate \eqref{eq:PoissonOrig} in the mixed form:


\begin{align}
&\frac{\partial}{\partial x} \textbf{D}  \quad = \quad f \nonumber \\
&  \textbf{D} \, + \, \epsilon_{k} \, \frac{\partial}{\partial x} \Phi(x,t) \quad = \quad 0 \nonumber \\
&\textbf{E} \quad = \quad \epsilon^{-1}_{k} \, \textbf{D} 
\end{align}



\noindent
Now we form our mesh $[a, \, b] \; = \; \cup_{i = 1}^{N} \, I_{i}$ where $I_{i} = [x_{i}, \, x_{i-1} ]$



\vspace{3mm}


\noindent
Testing with $v \in L_{2} (\mathbb{R})$, and $\textbf{p} \in H_{(div)}(\mathbb{R})$ we obtain the weak formulation:


\begin{align}
&\int_{x_{i-1}}^{x_{i}} \, v \, \frac{\partial \textbf{D}}{\partial x}  \, dx \; = \; \int_{x_{i-1} }^{x_{i}} \, v \, f_{i} \, dx  \\
&\int_{x_{i-1}}^{x_{i}} \, \left( \, \textbf{p} \, \cdot \, \textbf{D} \, \right) \, dx \, + \, \epsilon_{k} \, \int_{x_{i-1}}^{x_{i}} \, \textbf{p} \, \cdot \, \frac{\partial \Phi}{\partial x} \, dx \; = \; 0 
\end{align}


\vspace{2mm}


\noindent
And after integration by parts only on the second equation we have:


\vspace{2mm}


\begin{align}
&\int_{x_{i-1}}^{x_{i}} \, v \, \frac{\partial \textbf{D}}{\partial x}  \, dx \; = \; \int_{x_{i-1} }^{x_{i}} \, v \, f_{i} \, dx  \\
&\int_{x_{i-1}}^{x_{i}} \, \left( \, \textbf{p} \, \cdot \, \textbf{D} \, \right) \, dx \, - \, \epsilon_{k} \, \int_{x_{i-1}}^{x_{i}} \, \frac{\partial \textbf{p}}{\partial x} \, \Phi \, dx \, + \, p_{i}^{-} \, \Phi_{i} \, - \, p_{i-1}^{+} \, \Phi_{i-1} \; = \; 0 
\end{align}


\vspace{2mm}


\noindent
Multiplying the second equation by $\epsilon_{k}^{-1} $ we obtain:


\begin{align}
&\int_{x_{i-1}}^{x_{i}} \, v \, \frac{\partial \textbf{D}}{\partial x}  \, dx \; = \; \int_{x_{i-1} }^{x_{i}} \, v \, f_{i} \, dx  \\
&\int_{x_{i-1}}^{x_{i}} \, \epsilon_{k}^{-1} \, \left( \, \textbf{p} \, \cdot \, \textbf{D} \, \right) \, dx \, - \,  \int_{x_{i-1}}^{x_{i}} \, \frac{\partial \textbf{p}}{\partial x} \, \Phi \, dx \, + \, \epsilon_{k}^{-1} \, p_{i}^{-} \, \Phi_{i} \, - \, \epsilon_{k}^{-1} \, p_{i-1}^{+} \, \Phi_{i-1} \; = \; 0 
\end{align}


\noindent
Now summing over all the elements we have in the first equation:


\vspace{2mm}


\begin{equation}
\sum_{i = 1}^{N} \,  \int_{x_{i-1}}^{x_{i}} \, v \, \frac{\partial \textbf{D}}{\partial x}  \, dx \; = \; \sum_{i = 1}^{N} \, \int_{x_{i-1} }^{x_{i}} \, v \, f_{k} \, dx
\end{equation}



\vspace{2mm}


\noindent
Which becomes:


\vspace{2mm}


\begin{equation}
\int_{a}^{b} \, v \, \frac{\partial \textbf{D}}{\partial x}  \, dx \; = \; \int_{a}^{b} \, v \, f_{k} \, dx
\end{equation}


\vspace{2mm}

\noindent
While the taking the sum in the second equation leads to:

\vspace{2mm}


\begin{equation}
\sum_{i=1}^{N} \, \int_{x_{i-1}}^{x_{i}} \, \epsilon_{k}^{-1} \, \left( \, \textbf{p} \, \cdot \, \textbf{D} \, \right) \, dx \, - \, \sum_{i=1}^{N} \, \, \int_{x_{i-1}}^{x_{i}} \, \frac{\partial \textbf{p}}{\partial x} \, \Phi \, dx \, + \, \sum_{i=1}^{N} \,  \epsilon_{k}^{-1} \, \left[ p_{i}^{-} \, \Phi_{i} \, - \, p_{i-1}^{+} \, \Phi_{i-1} \right] \; = \; 0 
\end{equation}

\vspace{2mm}


\noindent
Which after noting the continuity of $\textbf{p}$ across the elements and the interface we arrive at:

\vspace{2mm}


\begin{equation}
\int_{a}^{b} \, \epsilon_{k}^{-1} \, \left( \, \textbf{p} \, \cdot \, \textbf{D} \, \right) \, dx \, - \, \int_{a}^{b} \, \frac{\partial \textbf{p}}{\partial x} \,  \Phi \, dx \, + \, \epsilon_{k}^{-1} \, p(b) \, \Phi(b) \, - \,  \epsilon_{k}^{-1} \, p(a) \, \Phi(a) \; = \; 0 
\end{equation}

\vspace{2mm}


\noindent
Summarizing we now have the weak formulation:

\vspace{2mm}

\begin{align}\label{eq:PoissonWeak}
&\int_{a}^{b} \, v \, \frac{\partial \textbf{D}}{\partial x}  \, dx \; = \; \int_{a}^{b} \, v \, f_{k} \, dx \nonumber \\
&\int_{a}^{b} \, \epsilon_{k}^{-1} \, \left( \, \textbf{p} \, \cdot \, \textbf{D} \, \right) \, dx \, - \, \int_{a}^{b} \, \frac{\partial \textbf{p}}{\partial x} \,  \Phi \, dx \, + \,  \epsilon_{k}^{-1} \, p(b) \, \Phi(b) \, - \,  \epsilon_{k}^{-1} \, p(a) \, \Phi(a) \; = \; 0 
\end{align}


\vspace{5mm}


\section{Matrix Vector Formulation}


\noindent
Now we take $v \, = \, \psi_{I} \; \in L_{2}([a, \,b ])$ and $\textbf{p} \, = \, \Upsilon_{I} \; \in H^{1}(\text{div}, \, ([a, \,b ])$, we use the expansions:

\vspace{2mm}

\begin{equation}
\Phi \, = \, \sum_{J = 1}^{N} \, \Phi_{J} \, \psi_{J} \qquad \text{and} \qquad \textbf{p} \, = \, \sum_{J = 1}^{N} \, D_{J} \, \Upsilon_{J} 
\end{equation}

\vspace{2mm}


\noindent
Plugging this into \eqref{eq:PoissonWeak} we have:

\vspace{2mm}

\begin{align}
&\int_{a}^{b} \, \psi_{I} \, \frac{\partial}{\partial x}  \left( \sum_{J = 1}^{N} \, D_{J} \, \Upsilon_{J} \right) \, dx \; = \; \int_{a}^{b} \, \psi_{I} \, f_{k} \, dx \nonumber \\
& \epsilon_{k}^{-1} \, \int_{a}^{b} \, \Upsilon_{I} \, \cdot \, \left( \sum_{J = 1}^{N} \, D_{J} \, \Upsilon_{J} \right) \, dx \, - \, \int_{a}^{b} \, \frac{\partial \Upsilon_{I}}{\partial x} \,  \left( \sum_{J = 1}^{N} \, \Phi_{J} \, \psi_{J} \right) \, dx \, + \,\ epsilon_{k}^{-1} \, \Upsilon_{I}(b) \, \Phi(b) \, - \, \epsilon_{k}^{-1} \, \Upsilon_{I}(a) \, \Phi(a) \; = \; 0 
\end{align}

\vspace{2mm}

\noindent
Pulling the sums out of the expression:

\vspace{2mm}


\begin{align}
&\sum_{J = 1}^{N} \, D_{J} \, \left( \int_{a}^{b} \, \psi_{I} \, \frac{\partial}{\partial x}  \Upsilon_{J} \right)  \, dx \; = \; \int_{a}^{b} \, \psi_{I} \, f_{k} \, dx \nonumber \\
& \epsilon_{k}^{-1} \, \sum_{J = 1}^{N} \, D_{J} \left( \, \int_{a}^{b} \, \Upsilon_{I} \, \cdot \, \Upsilon_{J} \, dx \right) \, - \, \sum_{J = 1}^{N} \, \Phi_{J} \, \left( \int_{a}^{b} \, \frac{\partial \Upsilon_{I}}{\partial x} \, \psi_{J}  \, dx \, \right) + \, \epsilon_{k}^{-1} \,\Upsilon_{I}(b) \, \Phi(b) \, - \, \epsilon_{k}^{-1} \, \Upsilon_{I}(a) \, \Phi(a) \; = \; 0 
\end{align}


\vspace{2mm}

\noindent
Which we can rewrite as a matrix vector problem:

\vspace{2mm}

\begin{equation}
\left[ \begin{matrix}
\epsilon^{-1} A_{00} & -A_{10} \\
 A_{01} & 0 \\
\end{matrix} \right]
\left[ \begin{matrix}
\textbf{D}_{DOF} \\
\boldsymbol \Phi_{DOF} \\
\end{matrix} \right] =
\left[ \begin{matrix}
0 \\
\textbf{F}  \\      
\end{matrix}\right]  + 
\left[ \begin{matrix}
\epsilon^{-1} \,\textbf{V}\\
0 \\
\end{matrix} \right]
\end{equation}


\noindent
Where the matrices are


\begin{equation}
A_{00}(I,J) \; = \; \int_{a}^{b} \, \Upsilon_{I} \, \cdot \, \Upsilon_{J} \, dx 
\end{equation}

\begin{equation}
A_{10}(I,J) \; = \; \int_{a}^{b} \, \frac{\partial \Upsilon_{I}}{\partial x} \, \psi_{J}  \, dx \,
\end{equation}

\begin{equation}
A_{01}(I,J) \; = \; \int_{a}^{b}  \, \psi_{I} \, \frac{\partial \Upsilon_{J}}{\partial x} \, dx \,
\end{equation}


\begin{equation}
\textbf{V}(I) \; = \;  \Upsilon_{I}(b) \, \Phi(b) \, - \, \Upsilon_{I}(a) \, \Phi(a)
\end{equation}

\begin{equation}
\textbf{F}(I) \; = \; \int_{a}^{b} \, \psi_{I} \, f \, dx
\end{equation}

\vspace{2mm}


\begin{equation}
\epsilon^{-1} \; = \; \left[ \begin{matrix}
\epsilon_{1}^{-1} & & & & & & & \\
& \epsilon_{1}^{-1} & & & & & &\\
& & \ddots & & & & & \\
& & & \epsilon_{1}^{-1} & & & &  \\
& & & & \epsilon_{2}^{-1} & & & \\
& & & & & \ddots & & \\
& & & & & & \epsilon_{2}^{-1} & \\
& & & & & & & \epsilon_{2}^{-1} 
\end{matrix} \right] 
\end{equation}



\chapter{Poisson's Equation In Semiconductor-Electrolytes}

 
\section{Problem}

\noindent
Solving the Poisson's equation for the potential:


\begin{equation} \label{eq:PoissonSemiOrig}
-\frac{\partial}{\partial x} \left( \epsilon_{k} \, \frac{\partial}{\partial x} \Phi(x,t) \right) \quad = \quad  \frac{q}{\epsilon_{0}} \left[ C_{k}(x) \, + \, s \, u(x,t) \right]
\end{equation}
 

\noindent
Over the interval $[a,b]$ where

\begin{equation}
\epsilon_{k} \quad = \left\{ \begin{array}{lc}
\epsilon_{1} \qquad & \text{if} \quad x \; \in \; [a, \, \Sigma) \\
\epsilon_{2} \qquad & \text{if} \quad x \;  \in \; (\Sigma, \, b] 
\end{array} \right.
\end{equation}


\noindent
And the background is the doping profile of the semiconductor $C_{1}(x)$ and zero on the electrolyte side:

\begin{equation}
C_{k} \quad = \left\{ \begin{array}{lc}
C_{1} \qquad & \text{if} \quad x \; \in \; [a, \, \Sigma) \\
0 \qquad & \text{if} \quad x \;  \in \; (\Sigma, \, b] 
\end{array} \right.
\end{equation}

 

\noindent
With the boundary conditions:


\begin{equation}
\Phi(a,t) \quad = \quad  \Phi_{\text{left}}(t), \qquad \Phi(b,t) \quad  = \quad \Phi_{\text{right}}(t)
\end{equation}


\noindent
And the interface conditions that:

\begin{align}
& \left[\, \Phi \, \right]_{\Sigma} \quad = \quad 0 \nonumber \\
& \left[ \, \epsilon_{k} \frac{\partial \Phi}{\partial x} \, \right]_{\Sigma} \quad = \quad 0 \nonumber \\
\end{align}


\noindent
Where $s$ is the sign of the force on the carrier due to the electric field and $u$ is a general carrier.  In case of bipolar devices, $u = n - p$ and $s = -1$.  

\vspace{2mm}


\noindent
We define the electric field as:

\begin{equation}
\textbf{E} \quad = \quad - \frac{\partial \Phi}{\partial x}
\end{equation}


\vspace{2mm}


\noindent
And the ``displacement electric field" as:


\begin{equation}
\textbf{D} \quad = \quad \epsilon_{k} \, \textbf{E}
\end{equation}


\vspace{2mm}


\noindent
Now we reformulate \eqref{eq:PoissonSemiOrig} in the mixed form:


\begin{align} \label{eq:PoissonSemiMixed}
&\frac{\partial}{\partial x} \textbf{D}  \quad = \quad  \frac{q}{\epsilon_{0}} \left[ C_{k}(x) \, + \, s \, u(x,t) \right] \nonumber \\
& \textbf{D} \, + \, \epsilon_{k} \, \frac{\partial}{\partial x} \Phi(x,t) \quad = \quad 0 \nonumber \\
&\textbf{E} \quad = \quad \epsilon^{-1}_{k} \, \textbf{D} 
\end{align}

\vspace{10mm}



\section{Non-Dimensional Formulation}


\noindent
We define the following scales using the general charge carrier $u$.
 
 
 \begin{itemize}
  \item $\widehat{x} \; = \; x / L$
  \item $\widehat{t} \; = \;  t / \tau $, where $\tau$ is the characteristic time
  \item $\widehat{\Phi} \; = \;\Phi / U_{T}$, where $U_{T}$ is the thermal voltage $U_{T} \; = \;k_{B}T/q$
  \item $\widehat{C} \; = \;C/C^{*}$
  \item $\widehat{u} \; = \;u/C^{*}$
  \item These gives us the important relations:
  $$\frac{\partial}{\partial \widehat{x}} \; = 
  					\; L \, \frac{\partial}{\partial x} \hspace{20mm} \widehat{\textbf{E}} \; = \;\frac{L}{U_{T}} \textbf{E} \hspace{20mm}$$
 \end{itemize}
 
 
 \noindent
 Substituting these into \eqref{eq:PoissonSemiMixed} and dropping the hats for ease of notation we are left with
 
 
\begin{equation}
\frac{1}{L} \, \frac{\partial}{\partial x} \left(  -\epsilon_{k} \,\frac{U_{T}}{L} \, \frac{\partial}{\partial x} \, \Phi  \right) \quad = \quad  \frac{q}{\epsilon_{0}} \, C^{*} \, \left[ C_{k}(x) \, + \, s \, u(x,t) \right] \nonumber
\end{equation}


\noindent
Which simplifies to:


\begin{equation}
\frac{\epsilon_{0} \, U_{T} }{q \, L^{2} \, C^{*}} \, \frac{\partial}{\partial x}   \, \left(  -\epsilon_{k} \, \frac{\partial}{\partial x} \, \Phi  \right) \quad = \quad   \left[ C_{k}(x) \, + \, s \, u(x,t) \right] \nonumber
\end{equation}

\vspace{2mm}


\noindent
Now we define $\lambda \; = \; \frac{\epsilon_{0}U_{T}}{L^{2}qC^{*}}$ and our definition of $\textbf{D}$ and $\textbf{E}$ then we have the non-dimensional forms:

\vspace{2mm}

\begin{align}
& \frac{\partial}{\partial x}   \, \textbf{D}  \quad = \quad  \frac{1}{\lambda} \; \left[ C_{k}(x) \, + \, s \, u(x,t) \right] \nonumber \\
& \textbf{D} \, +  \, \epsilon_{k} \, \frac{\partial}{\partial x} \Phi(x,t) \quad = \quad 0 \nonumber \\
&\textbf{E} \quad = \quad \epsilon^{-1}_{k} \, \textbf{D} 
\end{align}


\vspace{5mm}


\section{Mixed FEM}


\noindent
Now we form our mesh $[a, \, b] \; = \; \cup_{i = 1}^{N} \, I_{i}$ where $I_{i} = [x_{i}, \, x_{i-1} ]$



\vspace{3mm}


\noindent
$v \in L_{2} (\mathbb{R})$, and $\textbf{p} \in H_{(div)}(\mathbb{R})$ and following the same steps as in the previous chapter, but substituting $f \; = \; \frac{1}{\lambda} \, \left[C_{k}(x) \, + \, s \, u(x,t) \right]$ we will arrive at:



\begin{align}\label{eq:PoissonSemiWeak}
&\int_{a}^{b} \, v \, \frac{\partial \textbf{D}}{\partial x}  \, dx \; = \; \frac{1}{\lambda} \, \int_{a}^{b} \, v \,  \left[C_{k}(x) \, + \, s \, u(x,t) \right] \, dx \nonumber \\
&\int_{a}^{b} \, \epsilon_{k}^{-1} \, \left( \, \textbf{p} \, \cdot \, \textbf{D} \, \right) \, dx \, - \, \int_{a}^{b} \, \frac{\partial \textbf{p}}{\partial x} \,  \Phi \, dx \, + \, \epsilon^{-1}_{k} \, p(b) \, \Phi(b) \, - \, \epsilon^{-1}_{k} \,  p(a) \, \Phi(a) \; = \; 0 
\end{align}



\vspace{2mm}


\noindent
Now we take $v \, = \, \psi_{I}$ and $\textbf{p} \, = \, \Upsilon_{J}$, we use the expansions:

\vspace{2mm}

\begin{equation}
\Phi \, = \, \sum_{J = 0}^{DGDOF} \, \Phi_{J} \, \psi_{J} \qquad \text{and} \qquad \textbf{p} \, = \, \sum_{J = 1}^{MXDOF} \, D_{J} \, \Upsilon_{J} 
\end{equation}

\begin{equation}
C(x) = \sum_{J=0}^{DGDOF} \, C_{J} \, \psi_{J}(x) \qquad u(x,t) = \sum_{J=0}^{DGDOF} \, u_{J} \, \psi_{J}(x)
\end{equation}

\vspace{2mm}

\noindent
Plugging this into \eqref{eq:PoissonSemiWeak} we arrive at:

\vspace{2mm}

\begin{align}
&\int_{a}^{b} \, \psi_{I} \, \frac{\partial}{\partial x}  \left( \sum_{J = 1}^{MXDOF} \, D_{J} \, \Upsilon_{J} \right) \, dx \; = \; \frac{1}{\lambda} \, \int_{a}^{b} \, \psi_{I} \, \left[ \left(\sum_{J = 0}^{DGDOF} \, C_{J} \, \psi_{J}(x) \right) + s \left( \sum_{J = 0}^{DGDOF} \, u_{J}\, \psi_{J}(x) \right) \, \right] \, dx \nonumber \\
& \epsilon_{k}^{-1} \, \int_{a}^{b} \, \Upsilon_{I} \, \cdot \, \left( \sum_{J = 1}^{MXDOF} \, D_{J} \, \Upsilon_{J} \right) \, dx \, - \, \int_{a}^{b} \, \frac{\partial \Upsilon_{I}}{\partial x} \,  \left( \sum_{J = 0}^{DGDOF} \, \Phi_{J} \, \psi_{J} \right) \, dx \, + \, \epsilon^{-1}_{k} \, \Upsilon_{I}(b) \, \Phi(b) \, - \, \epsilon^{-1}_{k} \, \Upsilon_{I}(a) \, \Phi(a) \; = \; 0 
\end{align}


\noindent
Pulling the sums out of the equations we have:



\begin{align}
&\sum_{J = 1}^{MXDOF} \, D_{J} \, \left( \int_{a}^{b} \, \psi_{I} \, \frac{\partial}{\partial x}  \Upsilon_{J} \right)  \, dx \; = \; \frac{1}{\lambda} \,\sum_{J = 0}^{DGDOF} \left[ \, C_{J} \, + \, s \, u_{J} \right] \;  \left( \int_{a}^{b} \, \psi_{I} \, \psi_{J}  \, dx \right) \nonumber \\
& \epsilon_{k}^{-1} \, \sum_{J = 1}^{MXDOF} \, D_{J} \left( \, \int_{a}^{b} \, \Upsilon_{I} \, \cdot \, \Upsilon_{J} \, dx \right) \, - \, \sum_{J = 1}^{MXDOF} \, \Phi_{J} \, \left( \int_{a}^{b} \, \frac{\partial \Upsilon_{I}}{\partial x} \, \psi_{J}  \, dx \, \right) + \, \epsilon^{-1}_{k} \, \Upsilon_{I}(b) \, \Phi(b) \, - \, \epsilon^{-1}_{k} \, \Upsilon_{I}(a) \, \Phi(a) \; = \; 0 
\end{align}


\noindent
Which we can rewrite as:

\begin{equation}
\left[ \begin{matrix}
 \epsilon^{-1} \, A_{00} & -A_{10} \\
 A_{01} & 0 \\
\end{matrix} \right]
\left[ \begin{matrix}
\textbf{D}_{DOF} \\
\boldsymbol \Phi_{DOF}
\end{matrix} \right] =
\left[ \begin{matrix}
0 \\
\frac{1}{\lambda} C  \\      
\end{matrix}\right]  (\textbf{C} -\textbf{u} )_{DOF} +
\left[ \begin{matrix}
\epsilon^{-1} \, \textbf{V} \\
0 \\
\end{matrix} \right]
\end{equation}


\vspace{2mm}

\noindent
Where all the matrices are the same as before with the addition of:


\begin{equation}
\textbf{C}(I,J) \; = \; \int_{a}^{b} \, \psi_{I} \, \psi_{J}  \, dx
\end{equation}





\end{document}
