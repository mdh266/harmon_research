\documentclass[12pt]{article}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fullpage}



\begin{document}

\title{Charge Transfer Processes at 
Semiconductor-Electrolyte  \\ Interfaces in Solar Cell Modeling}


\author{Michael Harmon \\ 
\\
\\
\textbf{Co-advisors:} Professor Irene Gamba and Professor Kui Ren \\
\\
\\
\textbf{Committee:}  \\
Professor Clint Dawson \\
Professor Omar Ghattas \\
Professor Graeme Henkelman
}



\date{May 2014}



\maketitle

\begin{center}
\textbf{\large{Abstract}}
\end{center}

\medskip


In recent years, there has been an increased need for simulation tools in designing efficient semiconductor-electrolyte solar cells to harvest sunlight for clean energy. In the typical setup, a semiconductor-electrolyte solar cell consists of four main components: the semiconductor, the electrolyte, the semiconductor-electrolyte interface and the counter electrode. Photons are absorbed when sunlight shines on the semiconductor, which generates electron-hole pairs. These electrons and holes are separated by an electric field applied across the device. The separation of the electrons and holes creates an electrical current in the cell and an accumulation of charges at the semiconductor-electrolyte interface.  At the interface, the electrons promote the generation and elimination of reductant and oxidant pairs in the electrolyte.

\medskip

In the work by He.\ et al.\ \cite{HeGaLeRe-SIAM14}, the authors proposed a macroscopic mathematical model consisting of a system of nonlinear partial differential equations for the complete description of charge transfer dynamics in such semiconductor-electrolyte systems. The model consists of a reaction-drift-diffusion-Poisson system that models the transport of electron-hole pairs in the semiconductor region and an equivalent system that describes the transport of reductant-oxidant pairs in the electrolyte region. The coupling between the semiconductor and the electrolyte is modeled through a set of interfacial reactive and current balance conditions. This macroscopic model is not the most advanced model to describe change transport in a semiconductor-electrolyte system. However, it represents a good balance between accuracy and computational cost. 

\medskip

Numerical simulations of semiconductor-electrolyte solar cells based on models such as the one constructed in~\cite{HeGaLeRe-SIAM14} are a non-trivial task. The reaction-drift-diffusion-Poisson system used to model each component of the cell is highly nonlinear. The coupling of the equations for the semiconductor with the equations for the electrolyte through the interface makes the full system stiff. Moreover, sharp potential and density gradients can develop around the interface creating further challenges.

\medskip

Discontinuous Galerkin (DG) methods are well suited to solve these reaction-convection-diffusion-type equations. DG methods have many desirable qualities including mass conservation, the ability to capture sharp gradients and have high order accuracy.  Additionally, they are suitable for parallel implementation and easily handle complex geometries and boundary conditions \cite{CoSh-SIAM1998, CoDa-MAFELAP1999}. For these reasons, I have implemented a unique local discontinuous Galerkin (LDG) method to handle the spatial component of the drift-diffusion equations that incorporates the reactive semiconductor-electrolyte interface conditions.  In order to overcome the non-linearity of the system, I have employed an iterative scheme along with explicit time stepping.  Within the numerical simulations, the potential and electric field of the system are self-consistently solved using a mixed continuous Galerkin (CG) method that ensures the continuity of the displacement electric field across the solid-liquid interface.

\medskip


In this thesis, I will discuss the implementation, testing, and results of one-dimensional simulations of the semiconductor-electrolyte interface problem.  The code base is programmed in C++ and uses object orientation to create classes of individual charge carriers and electric fields.  This allows for the code to be easily tested under different conditions and extended for various applications.  I propose work to be done on extending the code base to multiple dimensions. Different interface geometries will also be incorporated to maximize the power output and quantify the efficiency of the solar cell. Finally, I propose work on extending the numerical analysis and approach developed by Liu and Shu \cite{LiSh-SCM2010}  on LDG for the drift-diffusion with periodic boundary conditions to the full semiconductor-electrolyte system.  



\bigskip

\noindent
Contributions to the 3 areas of CSEM include:

\medskip


\noindent
\textbf{Area A:}
I plan to investigate the approximation properties of the local discontinuous Galerkin method for the semiconductor-electrolyte system developed in \cite{HeGaLeRe-SIAM14}. I plan to do this in two stages.  In the first stage, I will study the discrete Schwartz iteration for an external field.  It is necessary to obtain error estimates for the density that depends on the $L^{\infty}$ estimates of the electric field in order to obtain full stability as well as error estimates for the Gummel-Schwartz iteration.  In the second stage, I will expand these estimates to the complete coupled system.

\medskip

\noindent
\textbf{Area B:}
I propose an extension of the finite element methods used in the one dimensional simulations to two dimensions. Since the potential and charge carriers have degrees of freedom for their density and  gradient, this phase of the proposed research will address issues with storage and memory management.  Fast and efficient data structures, as well as linear solvers, will be implemented in C++.  Complex geometries will be explored through the use of efficient mesh generation software.  I will also develop a novel and efficient way to calculate interface conditions in a Gummel-Schwartz iteration of multi-phase transport in solid-liquid interfaces. Parallel implementation of the code will be explored using OpenMP and MPI on the Stampede High Performance Computing System facilitated by TACC.

\medskip


\noindent
\textbf{Area C:}
The physics of electron transport at the interface of a semiconductor and electrolyte is a relatively poorly understood problem \cite{Lewis-JPhysChemB1998}.  The numerical simulations developed in the proposed research extend the work in \cite{HeGaLeRe-SIAM14} to use higher order accurate methods and to simulate transient solutions that are efficient for higher dimensions.  It has been shown that  different geometric interface configurations effect the overall performance of semiconductor-electrolyte solar cells \cite{KeAtLe-JAP2005, FoPrFeMa-EES2011}.  The use of discontinuous Galerkin methods will allow for the exploration of these effects in a computational setting. More specifically, I will test a two-dimensional semiconductor-electrolyte solar cell with radial and planar symmetry to maximize the power output.


\bigskip
 

\bibliographystyle{ieeetr} 
\bibliography{../Harmon-Bib}


\end{document}

