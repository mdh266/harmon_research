%
%\documentclass[10pt]{report}
%\usepackage{stmaryrd}
%\usepackage{amsmath}
%\usepackage{amsfonts}
%\usepackage{amssymb}
%\usepackage{comment}
%\usepackage{mathtools}
%
%\usepackage{fullpage}
%
%\usepackage{graphicx}
%\usepackage{float}
%\usepackage[caption = false]{subfig}
%\usepackage[usenames,dvipsnames]{color}
%
%\newtheorem{theorem}{Theorem}
%
%\usepackage{enumitem}
%\usepackage{calc}
%
%\usepackage[openbib]{currvita}
%
%
%\linespread{1.5}
%
%\numberwithin{equation}{section}
%
%
%\begin{document}

\chapter{Completed Work}

\section{Numerical Results}

Results so far have been completed using a 1D drift-diffusion-Poisson solver with general boundary conditions implemented in C++.  Post-processing and visualization were implemented using Python and the Matplotlib library.  In both the CG and DG methods, we allow for arbitrary order polynomials.  A fully parallel version of the code was developed using OpenMP through the MKL, however this was removed in favor of the vectorized C++ sparse linear algebra library Eigen.\footnote{In 1D the amount of computational work was not large enough to justify the added overhead of parallelization.}  To solve the linear system resulting from the mixed CG method we used a direct solver called UMFPACK \cite{Davis-ACM2004-1, Davis-ACM2004-2, DaDu-ACM1999, DaDu-SIAM1997}. Mesh refinement around boundaries and interface was incorporated to reduce spurious oscillations in the current, however the use of this has been limited.  The use of mesh refinement with explicit time stepping was limited due to the severe time step requirement to ensure stability. 

A testing framework was introduced in order to ensure that the codebase was working properly.  This framework tests the mixed CG Poisson solver and the LDG drift-diffusion solver independently and compared their results to analytical solutions in the $L^{\infty}$ norm.  These tests included testing the Poisson solver with and without domain decomposition methods for Dirichlet boundary data.  The drift-diffusion solver was tested against cases involving Dirichlet, Neumann, Robin and mixed boundary conditions.

In order to verify that our code is working properly for the full nonlinear drift-diffusion-Poisson equations, we compare results from our simulations to a benchmark problem. The benchmark problem we chose was the simulation of a one-dimensional $n^{+}-n-n^{+}$ transistor. This device was studied by S. Liu and C.-W. Shu \cite{LiSh-SCM2010} using the LDG method to calculate the charge density and current, while the electric field was calculated from an integral formulation. The results from our simulation are in agreement with the results found by the authors.  


\medskip

We present results from simulations of a 1 micron long MESFET, with a 400 nanometer long channel.  The doping profile for this device was taken to be:

\begin{equation}
N_{D}(x) \quad = \quad \left\{
\begin{array}{cr}
10^{17} \; \left[ \, \text{cm}^{-3} \, \right] & \qquad \qquad 0 \, < \, x \, < \,300 \; [\, 
\text{nm} \, ] \\
10^{13} \; \left[ \, \text{cm}^{-3} \, \right] & \qquad \qquad 300 \; [\, 
\text{nm} \, ] \, \leq \, x \, \leq \,700 \; [\, 
\text{nm} \, ] \\
10^{17} \; \left[ \, \text{cm}^{-3} \, \right] & \qquad \qquad 700 \; [\, 
\text{nm} \, ] \, < \, x \, \leq \,100 \; [\, 
\text{nm} \, ] \\
\end{array} \right.
\end{equation}

\medskip


\noindent
Using a Silicon based device, we have that $\mu_{n} \, = \, 1500 \, \left[ \, \frac{\text{cm}^{2}}{\text{V} \, \text{s}} \, \right]$ and $\epsilon_{r} \, = \, 11.9$.  We apply a bias of $\Phi_{\text{app}} \, = \; 0.5 \, \left[ \, \text{V} \, \right]$ \cite{HeGaLeRe-SIAM14} and let the device evolve to steady state.  We use 100 elements and with first-order basis functions for DG and second-order basis functions for the electric field.  Figure \ref{LinearDensity} displays the initial and final density distributions. The initial and final electric field values are displayed in Figure \ref{LinearElec}.  

\begin{figure}[h!]
\centering
\subfloat[Initial]{\includegraphics[width=2.5in]{../Images/MESFET/LINEAR/Density0.jpg}}
\hspace{10mm}
\subfloat[Final]{\includegraphics[width=2.5in]{../Images/MESFET/LINEAR/Density1.jpg}}
\caption{Initial and final density distributions}
\label{LinearDensity}
\end{figure}

\medskip

\begin{figure}[h!]
\centering
\subfloat[Initial]{\includegraphics[width=2.5in]{../Images/MESFET/LINEAR/Elec0.jpg}}
\hspace{10mm}
\subfloat[Final]{\includegraphics[width=2.5in]{../Images/MESFET/LINEAR/Elec1.jpg}}
\caption{Initial and final electric field distributions}
\label{LinearElec}
\end{figure}


\noindent
In the final current distribution Figure \ref{Current} we notice oscillations around the channel junctions.  This appears to happen around sharp gradients in the density distribution.  This may be explained by noticing the current is made up of two parts, the drift current $J_{\text{drift}} \; = \; - \, E \, \rho_{n}$ and the diffusive current $J_{\text{diff}} \; = \; - \frac{\partial \rho_{n}}{\partial x}$.  It is observed that the drift current and the diffusive current when subtracted from one-another will produce these oscillations. We noticed when higher order basis functions are used for both the CG and DG methods, the size of the oscillations is diminished.  This can be seen in the case of Figure \ref{Current}, where we use fourth-order DG basis functions and fifth-order CG basis functions.


\begin{figure}[h!]
\centering
\subfloat[DG Order $=$ 1, CG Order $=$ 2]{\includegraphics[width=2.5in]{../Images/MESFET/LINEAR/Current1.jpg}}
\hspace{10mm}
\subfloat[DG Order $=$ 4, CG Order $=$ 5]{\includegraphics[width=2.5in]{../Images/MESFET/QUINTIC/Current1.jpg}}
\caption{Final current distributions using different orders of basis functions}
\label{Current}
\end{figure}

\medskip


In order to verify that the application of LDG with reactive interfaces is working correctly we simplify our system by setting the electric field and concentration of reductants to zero.   This reduces the problem to the three carrier diffusion equation problem over the domain $[-L, \, L]$:

\begin{spreadlines}{0.5\baselineskip}
\begin{align}
\frac{\partial \rho_{n} }{\partial t} \, - \frac{\partial^{2} \rho_{n}}{\partial x^{2}}  \quad &= \quad 0 \qquad \qquad \qquad \qquad \text{in} \qquad [\, -L, 0 \, ] \\
\frac{\partial \rho_{r} }{\partial t} \, - \frac{\partial^{2} \rho_{r}}{\partial x^{2}}  \quad &= \quad 0 \qquad \qquad \qquad \qquad \text{in} \qquad [\, 0, L \, ] \\
\frac{\partial \rho_{o} }{\partial t} \, - \frac{\partial^{2} \rho_{o}}{\partial x^{2}}  \quad &= \quad 0 \qquad \qquad \qquad \qquad \text{in} \qquad [\, 0, L \, ] 
\end{align}
\end{spreadlines}

\medskip

\noindent
The boundary conditions are taken to keep the charge carrier densities on either side of the domain constant as follows:
\begin{equation}
\rho_{n}(-L, t) \; = \; \rho_{n}^{e} \qquad \qquad \rho_{o}(L,t) \; = \; \rho^{\infty}_{o} \qquad \qquad \rho_{r}(L,t) \; = \; \rho_{r}^{\infty}
\end{equation}

\noindent
The charge carriers are now coupled through the reactive interface condition:

\begin{spreadlines}{0.5\baselineskip}
\begin{align}
\frac{\partial \rho_{n}(0,t)}{\partial x}  \; &= \; k_{et} \, \left( \, \rho_{n}(0,t) \, - \, \rho_{n}^{e} \, /2 \, \right) \, \rho_{o}(0,t) \\
\frac{\partial \rho_{r}(0,t)}{\partial x}  \; &= \;  k_{et} \, \left( \, \rho_{n}(0,t) \, - \, \rho_{n}^{e} \, /2 \, \right) \, \rho_{o}(0,t) \\
\frac{\partial \rho_{o}(0,t)}{\partial x}  \; &= \;  - \, k_{et} \, \left( \, \rho_{n}(0,t) \, - \, \rho_{n}^{e} \, /2 \, \right) \, \rho_{o}(0,t)
\end{align}
\end{spreadlines}

\medskip


\noindent
We define the current throughout the entire domain as:

\begin{spreadlines}{0.5\baselineskip}
\begin{equation}
J(x,t) \; = \; \left\{ \begin{array}{cc}
\frac{\partial \rho_{n} } {\partial x} \qquad & \qquad  -L \, \leq \, x \, \leq \, 0 \\
\frac{\partial \rho_{r} } {\partial x} \qquad & \qquad \quad  0\, \leq \, x \, \leq \, L 
\end{array}  \right. 
\end{equation}
\end{spreadlines}

\medskip


\noindent
The steady state solution for $\rho_{n}$ in this problem is:
\begin{equation}
\rho_{n} \quad = \quad \frac{\rho_{n}^{e}}{2} \, \left( \, L  \, - \, x \, \right)
\end{equation}


\medskip

We test our code for the case of the domain to be $[-\pi, \, \pi]$.We take the density values $\rho_{e} \; = \; 1.0, \, \rho_{r} \; = \; \pi$, and $\rho_{o}^{\infty} \; = \; 2.0$.  The transfer rate was chosen to be $k_{et} \; = \; 10$  We use 100 first order elements on either side of the interface.  The initial configuration for this case is shown in Figure \ref{Test_2InitialConditions}.

\begin{figure}[h!]
	\centering
	\subfloat[Density]{\includegraphics[width = 2.5in]{../Images/INTERFACE/Density0.jpg}}
	\subfloat[Current]{\includegraphics[width = 2.5in]{../Images/INTERFACE/Current0.jpg}}
	\caption{Initial distributions}
	\label{Test_2InitialConditions}
\end{figure}

\noindent
 We see that the final densities and currents of the simulation are in agreement with the analytical steady state solutions. The steady state distributions for the densities and currents are shown in Figure \ref{Test_2FinalConditions}.  In this case mass is not conserved through the interface.  Electrons and oxidants are eliminated to create reductants.  This is due to the nature of the model and is to be expected.   While simulations for the full semiconductor-electrolyte interface have been completed, the parameter values were taken to shorten the simulation length and do not yet reflect realistic scenarios.




\begin{figure}[h!]
	\centering
	\subfloat[Density]{\includegraphics[width = 2.5in]{../Images/INTERFACE/Density1.jpg}}
	\subfloat[Current ]{\includegraphics[width = 2.5in]{../Images/INTERFACE/Current1.jpg}}
	\caption{Final distributions}
	\label{Test_2FinalConditions}
\end{figure}




\medskip



\section{Stability Analysis}


We wish to prove the convergence of the numerical approximation used.  We simplify the discussion by requiring the electric field to be constant in time and only analyze the convergence of the local discontinuous Galerkin method for reactive interfaces using a parallel Schwarz method.  At present we have,

\begin{enumerate}
\item Proven semi-discrete $L^{2}$-stability the LDG method for charge transport with Shottky contacts.
\item Derived energy estimates to show stability of drift-diffusion systems with reactive interfaces.
\end{enumerate}

\noindent
These results and their derivations can be found in the appendix in sections 3, 4 and 5.

