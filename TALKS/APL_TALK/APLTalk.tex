\documentclass{beamer}

\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{xcolor}
\usepackage{float}
\usepackage{comment}%
\setbeamertemplate{caption}{\raggedright\insertcaption\par}
\usepackage{lmodern}
\beamertemplatenavigationsymbolsempty
%\usepackage{bbding}
\usepackage{pifont}
\usepackage{wasysym}

\setbeamertemplate{blocks}[rounded][shadow=false]
\addtobeamertemplate{block begin}{\pgfsetfillopacity{0.8}}{\pgfsetfillopacity{1}}
\setbeamercolor*{block title example}{fg=black,bg=blue!30}
\setbeamercolor*{block body example}{fg= black,
bg= green!10}
%

\usetheme{CambridgeUS}


\begin{document}

\title[]{Numerical Algorithms For \\ Charge Transport With Reactive Interfaces In \\ Nanostructured Photoelectrochemical Solar Cells}

\author[]{Michael Harmon \\
\smallskip
 \textbf{Supervisors:} Dr. Irene M. Gamba and Dr. Kui Ren }


\institute[]{Institute of Computational and Engineering Sciences \\ University of Texas at Austin}


\date[]{October 22nd, 2015}


\frame{\titlepage}


\begin{frame}
\frametitle{Outline Of Presentation}
\tableofcontents
\end{frame} 


\section{Motivation}

\begin{frame}
\begin{center}
\Large{\textbf{Motivation}}
\end{center}
\end{frame}



\begin{frame}
\frametitle{Photoelectrochemical (PEC) Solar Cells}

	\begin{itemize}
	\item \textbf{Energy ``storage":} Produce hydrogen fuel instead of electricity.

	\bigskip
	
	\begin{columns}
	
	\begin{column}{5cm}
	\centering
	\hspace{0.8cm} PEC Solar Cell
	\begin{figure}
		\includegraphics[scale=0.5]{./Images/PEC1.png}
	\caption{\tiny{Source: Pan Research Group.  Univ. Alabama.}}
	\end{figure}
	\end{column}
	\begin{column}{5cm}
	\centering
	Semiconductor-Electrolyte Junction Dynamics
	\begin{figure}
		\includegraphics[scale=0.5]{./Images/WaterSplitting.png}
	\caption{\tiny{Source: Electrochemical Energy Lab, M.I.T.}}
	\end{figure}	
	\end{column}
	\end{columns}
	
	\smallskip
		
	\item \textbf{Anodic Reaction}: $2 \, \text{H}_{2}\text{O} \, \rightarrow \,  \text{O}_{2} \, + \, 4 \, \text{H}^{+} \, + \, 2 \, \text{e}^{-}$.
	\smallskip	
	\item \textbf{Cathodic Reaction}: $4 \, \text{H}_{2}\text{O} \, +  \, 4 \, \text{e}^{-} \, \rightarrow \, 2 \, \text{H}_{2} \, + \, 4 \, \text{HO}^{-}$.
	\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Nanostructured Semiconductor Wires}

\begin{itemize}

\item \textbf{Low cost:} Decoupling of light absorption and carrier collection removes material purity constraints.

\medskip
%
%	\begin{columns}
%	
%	\begin{column}{5cm}
%	\centering
%	Nanostructuring
	\begin{figure}
	\includegraphics[scale=0.65]{./Images/NanoWires.jpg}
	\caption{\tiny{Source: J. Fowley, 2011.}}
	\end{figure}
%	\end{column}
	
%	\begin{column}{5cm}
%	\centering
%	Wire Growth \& Scale	
%	\begin{figure}
%		\includegraphics[scale=0.5]{../Proposal/Images/WireGrowthSize.jpeg}
%	\caption{\tiny{Source: M. Kelzenberg, 2011.}}
%	\end{figure}	
%	\end{column}
%	
%	\end{columns}

	\medskip

\item \footnotesize{A. Fujishima \& K. Honda, 1972. A. Bard, 1980. M. Gratzel, 2001.}

\smallskip

\item  \footnotesize{ B. M. Keyes, 2005. N. Lewis, 2010. J. Fowley, 2011.}
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Outline Of Research Objectives}
\begin{enumerate}
\item Focus on the heterogeneous chemical reactions of a single isolated nanowire.
\bigskip
\item Model the charge transport and chemical reactions at the solid/liquid interface on a macroscopic level.
\bigskip
\item Develop and analyze finite element based simulations of the full-semiconductor electrolyte interface.
\bigskip
\item Construct fast time stepping algorithms.
\bigskip
\item Optimize the design of nanostructured solar cells.
\end{enumerate}
\end{frame}





\section{Mathematical Modeling Of Solar Cells}
\begin{frame}
\begin{center}
\Large{\textbf{Mathematical Modeling Of Solar Cells}}
\bigskip
\begin{figure}
\includegraphics[scale=0.3]{./Images/Abstractdomaindecomp.png} 
\end{figure}
\end{center}
\end{frame}


\begin{frame}
\frametitle{Modeling Semiconductor-Electrolyte Devices}

\begin{columns}

	\begin{column}{3.5cm}
	 \textbf{Semiconductor} \\
	 $(\rho_{n})$ Electrons ($-$)
	 $(\rho_{p})$ Holes ($+$)  
	\end{column}
	
	\begin{column}{3.5cm}
	\textbf{Electrolyte} \\
	$(\rho_{r})$ Reductants ($-$)
	$(\rho_{o})$ Oxidants ($+$)
	\end{column}
	
	\begin{column}{3.5cm}
	\textbf{Interface} \\
	$\rho_{n} \, + \, \rho_{o} \, \rightarrow \, \rho_{r}$
	$\rho_{p} \, + \, \rho_{r} \, \rightarrow \, \rho_{o}$
	\end{column}

\end{columns}

\bigskip 

\begin{itemize}
	
\item \textbf{Objective:} Relate applied voltages (input) to the devices current (output):    
\begin{equation*}\Phi \quad [ \, \text{V} \, ] \, \rightarrow  \, \textbf{J} \quad [ \, \text{mA} \, \cdot \, \text{cm}^{-2} \, ]
\end{equation*}

\medskip
		  
\item Currents approximated using drift and diffusion:
\begin{equation*}
\textbf{J}_{i} \; = \; s_{i} \, \mu_{i}  \, \boldsymbol \nabla \Phi \, \rho_{i} \, - \, D_{i} \, \boldsymbol \nabla \, \rho_{i} \qquad \left[ \, \text{cm}^{-2} \, \cdot s^{-1} \, \right]
\end{equation*}
		
\medskip
		
\item \footnotesize{Charge densities $\rho \; [ \, \text{cm}^{-3} \, ]$, mobility $\mu_{i} \; \left[ \, \text{cm}^{2} \cdot \text{V}^{-1} \cdot s^{-1} \, \right]$, diffusivity  $D_{i} \; \left[ \, \text{cm}^{2}\cdot s^{-1} \, \right]$, sign of charge $s_{i}$, and  electric field $- \boldsymbol \nabla  \Phi \;  \left[ \, \text{V} \cdot \text{cm}^{-1} \, \right]$.}
\end{itemize}
		
\end{frame}






\begin{frame}
	\frametitle{Charge Transport In Semiconductor Domain }
	\fontsize{10pt}{7.2}\selectfont
		\textbf{Transport}:  Drift-diffusion equation in $\Omega_{S}$:
		\begin{align*}
		\frac{\partial \rho_{n}}{\partial t}  \, + \, \boldsymbol \nabla  \cdot  \left(  \,  \mu_{n} \boldsymbol \nabla \Phi \, \rho_{n} \, - \, D_{n} \, \boldsymbol \nabla \, \rho_{n}  \, \right) &= -\textcolor{red}{R(\rho_{n},\rho_{p})} 	+ \gamma \,  \textcolor{red}{G}  \\
	    \frac{\partial \rho_{p}}{\partial t} \, +\,  \boldsymbol \nabla \cdot \left( \, - \mu_{p}   \boldsymbol \nabla \Phi \, \rho_{p} - \, D_{p} \,  	\boldsymbol \nabla \, \rho_{p} \, \right) &= -\textcolor{red}{R(\rho_{n},\rho_{p})} + \gamma \textcolor{red}{G} 
		\end{align*}
		
		\medskip		
		
		 \textbf{Flow:} Poisson's equation in $\Omega_{S}$:
		\begin{equation*}
			-\boldsymbol \nabla \cdot \left( \, \epsilon^{S}_{r} \, \boldsymbol \nabla \Phi \right)  = \frac{q}{\epsilon_{0}} \, \left[\textcolor{red}{ \rho_{n}^{e}(\textbf{x}) \,  - \, \rho_{p}^{e}(\textbf{x}) }  - (\rho_{n} -\rho_{p})\right]   
		\end{equation*}
		
		
		
		\medskip
		
		\textbf{Boundary Conditions:} Ohmic contacts  on $\Gamma_{S}$:
		
	\begin{equation*}
	\begin{array}{ccc}
	\rho_{n}  \; = \; \rho_{n}^{e},  \qquad & \qquad  \rho_{p} \; = \; \rho_{p}^{e},  \qquad & \qquad \Phi \; = \; \Phi_{\text{bi.}} \, + \, \Phi_{\text{app.}} 
	\end{array}
	\end{equation*}
		
\end{frame}

%\begin{frame}[begin]
%	\frametitle{Charge Transport In Semiconductor Domain}
%	 \begin{enumerate}
%	 \item Photon induced generation $G(\textbf{x}) \; \left[ \, \text{cm}^{-3} \cdot s^{-1} \, \right]$.
%	 \begin{itemize}
%	 	 \item Exponential distribution used to model attenuation of photon flux.
%		\smallskip
%		\item  Illuminated with standard intensity: 100 $\text{mW} \cdot \text{cm}^{-2}$.
%	 	 \end{itemize}
%	 	 
%	 \bigskip
%	 
%	 \item Shockley-Reed-Hall Recombination $R(\rho_{n}, \rho_{p})\; \left[ \, \text{cm}^{-3} \cdot s^{-1} \, \right]$.
%	 	\begin{itemize}
%	 	\item Nonlinear sink functional.
%	 	\smallskip
%		\item Dominant mechanism in real materials, especially near material boundaries and interfaces.
%		\smallskip 	
%	 	\end{itemize}
%
%	 \end{enumerate}
%	 
%	 \medskip
%	 
%	\footnotesize{W. Shockley, 1950. W. Van Roosbroeck, 1952. P. Markowich, C. Ringhofer, C. Schmeiser, 1990. J. Nelson, 2003.}
%
%\end{frame}


\begin{frame}
	\frametitle{Charge Transport In Electrolyte Domain}
	\fontsize{10pt}{7.2}\selectfont
	
		\textbf{Transport}: drift-diffusion equation $\Omega_{E}$:
		\begin{align*}
		\frac{\partial \rho_{r}}{\partial t}  \, + \, \boldsymbol \nabla  \cdot   \left(  \, \mu_{r}  \boldsymbol \nabla \Phi \, \rho_{r} \, - \,  D_{r}  \, \boldsymbol \nabla \, \rho_{r}  \, \right) &= 0  \\
	    \frac{\partial \rho_{o}}{\partial t} \, +\,  \boldsymbol \nabla \cdot \left( \, -  \mu_{o}   \boldsymbol \nabla \Phi \, \rho_{o} - \, D_{o} \,  	\boldsymbol \nabla \, \rho_{o} \, \right) &= 0 
		\end{align*}
		
				
		\medskip
		
		\textcolor{blue}{\textbf{Heterogeneous reactions}: Reductants and oxidants are created and eliminated \underline{only} at the solid-liquid interface.}
		
		\bigskip		
		
		 \textbf{Flow}: Poisson's equation in $\Omega_{E}$:
		
		\begin{equation*}
		-\boldsymbol \nabla \cdot \left( \, \epsilon^{E}_{r} \, \boldsymbol \nabla \Phi \right) = -\frac{q}{\epsilon_{0}} \,  (\rho_{r} -\rho_{o}) 
		\end{equation*}
		
		\medskip
		
		\textcolor{blue}{Electrolyte is charge neutral}.
		
		\medskip
		
				\medskip
		
		\textbf{Boundary Conditions:} Bulk density values on $\Gamma_{E}$:
		
	\begin{equation*}
	\begin{array}{ccc}
	\rho_{r}  \; = \; \rho_{r}^{\infty},  \qquad & \qquad  \rho_{o} \; = \; \rho_{o}^{\infty},  \qquad & \qquad \Phi \; = \; \Phi^{\infty}
	\end{array}
	\end{equation*}
		

\end{frame}


\begin{frame}
	\frametitle{Reactive Interface Conditions}
	\begin{itemize}
	\item \footnotesize{Semiconductor current conditions on $\Sigma_{S}$},
	\begin{align*} 
 \left(   \mu_{n}  \boldsymbol \nabla \Phi  \rho_{n} - D_{n}   \boldsymbol \nabla  \rho_{n}  \right) \cdot \boldsymbol \eta_{\Sigma} \; &= \; k_{et}  (  \rho_{n}  -  \rho_{n}^{e}  )  \rho_{o}  \\ 
 \left(-  \mu_{p}\boldsymbol \nabla \Phi \rho_{p} -  D_{p} 	\boldsymbol \nabla \rho_{p}  \right) \cdot \boldsymbol \eta_{\Sigma} \; &= \; k_{ht} (  \rho_{p}  -  \rho_{p}^{e} )  \rho_{r}  
	\end{align*}
	\vspace{2mm}
	\item \footnotesize{ Electrolyte current conditions on $\Sigma_{E}$},
	\begin{align*}  
 \left(    \mu_{r} \boldsymbol \nabla \Phi \rho_{r}  -  D_{r} \boldsymbol \nabla  \rho_{r}  \right) \cdot \boldsymbol \eta_{\Sigma} \;  &=
\;  k_{ht}   (\rho_{p}  - \rho_{p}^{e}) \rho_{r}  - k_{et}  (\rho_{n}  - \rho_{n}^{e})  \rho_{o}   \\
\left(-  \mu_{o}  \boldsymbol \nabla \Phi  \rho_{o} -  D_{o} 	\boldsymbol \nabla \rho_{o} \right) \cdot \boldsymbol \eta_{\Sigma} \; &=  \;
- k_{ht} (\rho_{p}  - \rho_{p}^{e} ) \rho_{r} +  k_{et} (\rho_{n} - \rho_{p}^{e})  \rho_{o} 
	\end{align*}
	\end{itemize}
	\begin{columns}
	\begin{column}{7cm}
	\begin{itemize}
	\item \footnotesize{Electrostratics on $\Sigma$},
	\end{itemize}
	\begin{equation*}
\llbracket \, \Phi \, \rrbracket_{\Sigma} \, = \, 0
	\end{equation*}
	\begin{equation*}
	\llbracket \, \epsilon_{r} \, \boldsymbol \nabla \, \Phi \, \rrbracket_{\Sigma} \, = \, 0
	\end{equation*}
	\vspace{5mm}
	
	\footnotesize{Y. He, I. M. Gamba, H.C. Lee, K. Ren, 2015.}
	\end{column}
	\begin{column}{5cm}
	\begin{figure}
	\includegraphics[scale=0.4]{./Images/WaterSplitting.png}
	\caption{\tiny{Source: Electrochemical Energy Lab, M.I.T.}}
	\end{figure}
	\end{column}
	\end{columns}



\end{frame}
%
%\begin{frame}
%	\frametitle{Schottky Approximation}
%	\begin{itemize}
%	\item Recombination velocities: 
%	\begin{align}
%	v_{n} \, &= \, k_{et} \, \rho_{o} \; \left[ \, \text{cm}^{2} \, \cdot \, s^{-1} \, \right]   \\
%	v_{p} \, &= \, k_{ht} \, \rho_{r} \; \left[ \, \text{cm}^{2} \, \cdot \, s^{-1} \, \right] 
%	\end{align*}
%	\smallskip
%	\item Schottky Approximation On $\Sigma_{S}$:
%	\begin{align} 
%\textbf{J}_{n} \cdot \boldsymbol \eta_{\Sigma} \; &= \; v_{n} \, ( \, \rho_{n} \, - \, \rho_{n}^{e} \, ) \\ 
%	\smallskip
%\textbf{J}_{p} \cdot \boldsymbol \eta_{\Sigma} \; &= \; v_{p} \, ( \, \rho_{p} \, - \, \rho_{p}^{e} \, ) 
%	\end{align*}
%	\medskip
%	\footnotesize{B. M. Keyes, 2005. N. Lewis, 2010. J. Fowley, 2011.}
%	\end{itemize}
%	\begin{itemize}
%	\item Inaccurate when concentration in semiconductor and electrolyte are comparable and on sub-micron scales.
%	\medskip
%	\footnotesize{Y. He, et. al., 2014.}
%	\end{itemize}
%\end{frame}





\section{Numerical Methods \& Algorithms}

\begin{frame}
\begin{center}
\Large{\textbf{Numerical Methods \& Algorithms}}
\end{center}
\end{frame}



%\begin{frame}
%\frametitle{Galerkin Finite Element Methods}
%
%Handles complex geometries, high accuracy, well-developed theory.
%
%\begin{figure}
%\centering
%\includegraphics[scale=0.2]{./Images/WorldQuad.png}
%\caption{\tiny{Source: http://www.argusone.com/MeshGeneration.html}}
%\end{figure}
%\small{ \begin{equation*}
%\begin{array}{cccccc}
%\text{Strong Form} & & \text{Weak Form} & & \text{Fin. Elem. Approx.} \\
%-\nabla^{2} u \, = \, f & \rightarrow & \sum\limits_{e}  \int_{\Omega_{e}} \nabla v \cdot \nabla u  \, dx \, = \,  \sum\limits_{e} \int_{\Omega_{e}} v \,  f \, dx
%& \rightarrow & Ax \, = \,  b 
%% \\u \in C^{2}(\Omega)  & \rightarrow & u \in H^{1}_{0}(\Omega) & \rightarrow &  u \in V_{h}
%\end{array}
%\end{equation*}}
%\end{frame}



\begin{frame}
\frametitle{Mixed Finite Element Method}
\begin{itemize}
\item \textbf{Pros:}  Obtain both $\Phi$ and $\boldsymbol \nabla \Phi$ and easily handles interface conditions.
\item \textbf{Cons:} More unknowns and  leads to indefinite matrix.
\smallskip

\item \textbf{Poisson's equation:} (non-dimensional)
\begin{align*}
- \boldsymbol \nabla \, \cdot \, \left( \lambda^{2}  \, \boldsymbol \nabla \, \Phi \right) \, &= \, f && \text{in}  \quad \Omega \\
\Phi \; &= \; \Phi_{D} &&  \text{on} \quad \Gamma \\
\llbracket \,  \lambda^{2} \, \boldsymbol \nabla \, \Phi \, \rrbracket \, &= \, 0 && \text{on} \quad \Sigma
\end{align*}

\smallskip

\item \textbf{Mixed formulation:}
\begin{align*}
\boldsymbol \nabla \, \cdot \, \textbf{D} \; &=  \; f && \text{in}  \quad \Omega \\
 \textbf{D} \; + \;  \lambda^{2} \, \boldsymbol \nabla \, \Phi \; &= \; 0 && \text{in}  \quad \Omega \\
\Phi \; &= \; \Phi_{D} &&  \text{on} \quad \Gamma \\
\llbracket \, \textbf{D} \, \rrbracket \;  &= \; 0 && \text{on} \quad \Sigma
\end{align*}
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Mixed Finite Element Method}

\begingroup
\footnotesize
\begin{exampleblock}{Weak Formulation}
Find $(\, \Phi \, , \, \textbf{D} \, ) \, \in \left( \, \text{W}_{h} \, \times \, [ \, 0 , \, T \, ] , \, \textbf{V}^{d} \, \times \,  [ \, 0 , \, T \, ] \, \right)$ such that:
\begin{align*}
 \left( \,  \textbf{p} \, , \, \left( \ \lambda^{2} \ \right)^{-1} \, \textbf{D} \,  \right)_{\Omega} \,  - \, \left( \, \boldsymbol \nabla \, \cdot \, \textbf{p}  \, , \,  \Phi  \, \right)_{\Omega}\;  &= \; -  \, \langle \,  \textbf{p} \, , \, \Phi_{D} \, \rangle_{\Gamma_{D}}   \\
- \, \left( \, v \, , \, \boldsymbol \nabla  \, \cdot \textbf{D} \, \right)_{\Omega} \;  &= \; - \, \left( \,  v \, , \, f   \, \right)_{\Omega}  
\end{align*} 


\medskip

For all $(\, v \, , \,  \textbf{p} \, ) \, \in \, W_{h} \, 
\times\, \textbf{V}_{h}^{d}$.
\end{exampleblock}

\smallskip

\textbf{Function spaces}:
\begin{equation*}
\text{W}_{h} = \left\{ \, v \,
\in L^{2}(\Omega):  \; v \, \vert_{\Omega_{e}} \, \in \mathcal{P}^{k}(\Omega_{e}) \, \right\}
\end{equation*}
\begin{equation*}
\textbf{V}_{h}^{d} =  RT^{k} 
\end{equation*}

\smallskip

\begin{exampleblock}{
Saddle point problem: (Symmetric indefinite matrix)}

\begin{equation*}
\left[ \begin{matrix}
A & B^{T} \\
B & 0 \\
\end{matrix} \right]
\, \left[ \begin{matrix}\
\textbf{d} \\
\boldsymbol \phi \\
\end{matrix}
\right] \; = \;
\left[ 
\begin{matrix}
\textbf{F}_{1} \\
\textbf{F}_{2}
\end{matrix} 
\right]
\end{equation*}
\end{exampleblock}

\endgroup


\end{frame}

\begin{frame}
\frametitle{Local Discontinuous Galerkin Method}
\begin{itemize}

\item \textbf{Pros:} Numerically stable and approximates densities and currents.
\item \textbf{Cons:} More unknowns and indefinite matrix.
\smallskip

\item \textbf{Transport problem:} (non-dimensional)
\begin{align*}
u_{t} \;  
+ \;   \boldsymbol \nabla \, \cdot  \,  \mu  \left(\,  s  \, \boldsymbol \nabla \Phi 
\, u \, - \,\boldsymbol \nabla u \, \right) \; &= \; S(u) 
&& \text{in} \quad  \Omega   \\
 u \; &= \; u_{D} &&  \text{on} \quad  \Gamma    \\ 
\mu  \left( \, s \, \boldsymbol \nabla \Phi \, u \, - \, \boldsymbol \nabla u \, \right) \, \cdot \,  \boldsymbol \eta \; &= \; K ( u ) &&  \text{on} \quad  \Sigma
\end{align*}

\smallskip

\item \textbf{Mixed Formulation:}
\begin{align*}
u_{t} \, + \, \nabla \cdot \, \textbf{q} \; &= \; S(u) &&  \text{in} \quad  \Omega   \\
\mu^{-1}  \textbf{q} \,  -   \, s \,  \boldsymbol \nabla \Phi  \, u \, + \,  \boldsymbol \nabla u \; &= \; 0 && \text{in} \quad  \Omega \\
u \; &= \; u_{D} && \text{on} \quad  \Gamma    \\ 
\mu^{-1} \textbf{q} \, \cdot  \,  \boldsymbol \eta \; &= \;  K(u) &&  \text{on} \quad  \Sigma 
\end{align*}
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Local Discontinuous Galerkin Methods}
\begin{exampleblock}{Weak Formulation}
\begingroup
\footnotesize
\noindent
Find $(u, \textbf{q}) \in \left( \ C \left( [0,T], W_{h} \right) \times \textbf{W}_{h}^{d} \times [0,T] \ \right)$ such that,
\begin{spreadlines}{0.75\baselineskip}
\begin{align*}
&\left(  v \, , \,  u_{t}  \right)
-
\left( \boldsymbol \nabla v  \, ,  \textbf{q}  \right)
 +
\langle  \llbracket \,  v \, \rrbracket \, , \,  \{ \, \textbf{q} \, \}   - \tau \llbracket u \rrbracket  \rangle_{\mathcal{E}_{h}^{0}}
+   
\langle   v\, ,  \, \textbf{q} \rangle_{\Gamma } 
\ = \  
- \textcolor{blue}{
\langle   v, \, K ( u )    \rangle_{\Sigma} }  
+ 
\left( v , \, S(u)  \right) \nonumber \\
&\left( \textbf{p} \, , \, \textbf{q} \right)
-
\left(  \boldsymbol \nabla \cdot \textbf{p} \, , \,  u \right)
+ 
\textcolor{red}{\left(  s \textbf{p}  \cdot \boldsymbol \nabla \Phi \, , \,  u \right)}
-  
\langle \, \llbracket \,  \textbf{p} \, \rrbracket \, ,   \, \{ \,  u \, \} 
\rangle_{\mathcal{E}^{0}_{h}}  
+
\langle   \textbf{p} \, , \, u   \rangle_{\Sigma } 
\ = \
 - 
\langle  \textbf{p}  \, , \,  u_{D}  \rangle_{ \Gamma } 
\nonumber 
\end{align*}
\noindent
For all $(v,\textbf{p}) \in W_{h} \times \textbf{W}_{h}^{d}$.
\end{spreadlines}
\endgroup
\end{exampleblock}
\smallskip

\begin{itemize}
\item Repeat LDG formulation for each carrier.
\smallskip
\item Matrix-vector form depends on time stepping method.  
\smallskip
\item \textbf{Problem:} Stiff nonlinear system.
\smallskip
\item \textbf{Solution:} Implicit-explicit (IMEX) Schwarz time stepping methods.
\end{itemize}
\end{frame}





%\begin{frame}
%\frametitle{$L^{2}$ Error Plots For 1-D Linear Problems}
%
%Testing individual solvers on manufactured solution.
%
%\begin{columns}
%\begin{column}{6cm}
%\begin{figure}
%\includegraphics[scale=0.15]{./Images/Mixed_Errors.png} 
%\caption{For Poisson's equation.}
%\end{figure}
%\end{column}
%\begin{column}{6cm}
%\begin{figure}
%\includegraphics[scale=0.15]{./Images/LDG_Errors.png} 
%\caption{For drift-diffusion equation.}
%\end{figure}
%\end{column}
%\end{columns}
%\end{frame}




\begin{frame}
\frametitle{Time Stepping: IMEX Schwarz Algorithms}
\begin{itemize}
\item \textbf{Problem:} 
\end{itemize}

\medskip

\centering
\begin{figure}
\includegraphics[scale=0.35]{./Images/problemdecomp.jpg} 
\end{figure}

\medskip

\begin{itemize}
\item \textbf{How do we pass information between domains?}
\end{itemize}

\end{frame}






\begin{frame}
\frametitle{Alternating Schwarz Algorithm}

\begin{figure}
\centering
\includegraphics[scale=0.55]{./Images/alternating_gummel_schwarz.jpg}  
\end{figure}

\end{frame}





\begin{frame}
\frametitle{Parallel Schwarz Algorithm}

\begin{figure}
\centering
\includegraphics[scale=0.5]{./Images/parallel_gummel_schwarz.jpg}  
\end{figure}
\end{frame}


\begin{frame}
\frametitle{Performance Results On 1D Code}
\footnotesize
Time in seconds and percentage of total CPU time of key subroutines in each algorithm.
\footnotesize
\begin{table}[h!]
\scriptsize
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
Algorithm & Tot. &  Drift & LDG Fact. & Recomb. & Sol. LDG  & Sol. MX \\
\hline
A-Imp. & 7,639  & 5,159 (68\%) &  1,741 (23\%) & 181 (2\%) & 237 (3\%) & 65 (1\%) \\ 
\hline
AA-IMEX & 2,506  & 61 (2\%)  & 1,723 (69\%)& 176 (7\%) & 234 (9\%) & 64 (3\%) \\ 
\hline
PU-IMEX & 703  & 73 (10\%) & $<1$ (0\%) & 213 (30\%) & 293 (42\%) & 80 (11\%)  \\ 
\hline
CTPU-IMEX & 169  & 19 (11\%) & $<1$ (0\%) & 45 (27\%) & 75 (45\%) & 19 (11\%) \\
\hline
CTPU-OMP & 151  & 20 (13\%) & $<1$ (0\%) & 17 (12\%) & 82 (54\%) & 20 (13\%)\\
\hline
\end{tabular}
\end{table}
\begin{itemize}
\item \textbf{Goal:} Speed up code so most of CPU time is used by linear solvers.
\item \textbf{Result:} $50\times$ speed up.
\end{itemize}

\footnotesize
\begin{exampleblock}{Linear Systems For Algorithms}
\begin{columns}
\begin{column}{3cm}
\centering
\textbf{ A-Imp.}
\medskip
$\left[ \begin{matrix}
\frac{1}{\Delta t_{k}} M +C - \textcolor{blue}{K} & D^{T} \\
D - \textcolor{red}{E} & A \\
\end{matrix} \right]$
\end{column}
\begin{column}{3cm}
\centering
\textbf{A-IMEX}
\medskip
$\left[ \begin{matrix}
\frac{1}{\Delta t_{k}} M + C - \textcolor{blue}{K} & D^{T} \\
D  & A \\
\end{matrix} \right]$
\end{column}
\begin{column}{3cm}
\centering
\textbf{PU-IMEX}
\medskip
$\left[ \begin{matrix}
\frac{1}{\Delta t} M + C& D^{T} \\
D  & A \\
\end{matrix} \right]$
\end{column}
\end{columns}
\end{exampleblock}

\end{frame}


\begin{frame}
\frametitle{PECS: Photo-Electrochemical Cell Simultor}
\textbf{Features:}
\begin{itemize}
\item Modern object oriented design.
\smallskip
\item Supports simulations in 1D and 2D (extendable to 3D).
\smallskip
\item Fully tested.
\smallskip
\item Well-documented with support for doxygen.
\smallskip
\end{itemize}
\bigskip
\textbf{Challanges:}
\begin{itemize}
\item Domain decomposition necessitated use of 3 triangulations.
\smallskip
\item Refactored the deal.II library's interface to Trilinos' direct sovlers.
\smallskip
\item Direct solvers limit scaling and require large amounts of memory.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{PECS: Photo-Electrochemical Cell Simultor}
\textbf{1-D PECS}
\begin{itemize}
\item Dependencies: C++, CMake, Eigen, GNU Scientific Library, Boost.
\smallskip
\item Parallelization: OpenMP.
\smallskip
\item Linear Solver: SparseLU.
\end{itemize}

\bigskip

\textbf{2-D PECS}
\begin{itemize}
\item Dependencies: C++11, CMake, deal.ii.
\smallskip
\item Parallelization: Intel Thread Building Blocks (TBB).
\smallskip
\item Linear Solver: UMFPACK.
\end{itemize}

\bigskip

\textbf{2-D Parallel PECS}
\begin{itemize}
\item Dependencies: C++,11 CMake, deal.ii, Trilinos, P4est.
\smallskip
\item Parallelization: Hybrid MPI \& TBB.
\smallskip
\item Linear Solver: User defined through Trilinos Amesos Package..
\end{itemize}
\end{frame}







\section{Numerical Results}


\begin{frame}
\frametitle{Numerical Results}

\begin{center}
\Large{\textbf{Numerical Results}}
\end{center}

\end{frame}





\begin{frame}
\frametitle{Measures Of Solar Cell Performance}
\begin{columns}
\begin{column}{5cm}
\begin{figure}
\includegraphics[scale=0.6]{./Images/SolarIVCurve.jpg} 
\end{figure}
\end{column}
\hspace{0.5cm}
\begin{column}{5cm}
\begin{itemize}
\item Efficiency: 
\begin{equation*}
\eta_{\text{eff}} \; = \; \frac{J_{M} \ \Phi_{M}}{100 \, [\text{mW cm}^{-2}]}
\end{equation*}
\medskip
\item Fill Factor:
\begin{equation*}
ff \; = \; \frac{J_{M} \ \Phi_{M}}{J_{SC} \  \Phi_{OC}}
\end{equation*}
\end{itemize}
\end{column}
\end{columns}
\end{frame}



\begin{frame}
\frametitle{Material Parameter Values}
\textbf{ n-type Silicon Parameter Values:}
\begin{small}
\begin{table}
\begin{tabular}{|cc|}
\hline
$\rho_{n}^{e} = 2\times 10^{16} \, [\text{cm}^{-3}] $ & $ \rho_{p}^{e} = 0 \, [\text{cm}^{-3}] $ \\
\hline
 $\mu_{n} = 1350 \, [\text{V cm s}^{-1}]$ & $\mu_{p} = 480 \, [\text{V cm s}^{-1}]$ \\
\hline
$k_{et} = 10^{-19} \, [\text{cm}^{4} \text{s}^{-1} ]$ &
$k_{ht} = 10^{-16} \, [\text{cm}^{4} \text{s}^{-1} ]$ 
 \\
\hline
$\epsilon_{r}^{S} = 11.9$ & \\
\hline
\end{tabular}
\end{table}
\end{small}

\textbf{Electrolyte Parameter Values:}
\begin{small}
\begin{table}
\begin{tabular}{|cc|}
\hline
$\rho_{r}^{\infty} = 5\times 10^{16} \, [\text{cm}^{-3}] $ & $  \rho_{o}^{\infty} = 4\times 10^{16}  \, [\text{cm}^{-3} ]$ \\
\hline
$\mu_{r} = 1 \, [\text{V cm s}^{-1}]$ & $\mu_{o} = 1 \, [\text{V cm s}^{-1}]$ \\
\hline
 $\epsilon_{r}^{E} = 1000 $  & \\
\hline
\end{tabular}
\end{table}
\end{small}

\end{frame}


\begin{frame}
\frametitle{2 Micron Device With $\Phi_{\text{app.}} = 0$}
\begin{figure}[h!]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.16]{./Images/MICRO_DARK_DENSITY.png}
&
\includegraphics[scale=0.16]{./Images/MICRO_ILLUM_DENSITY.png} \\
\includegraphics[scale=0.16]{./Images/MICRO_DARK_CURRENT.png}
&
\includegraphics[scale=0.16]{./Images/MICRO_ILLUM_CURRENT.png}
\end{tabular}
\end{figure}

\end{frame}


\begin{frame}

\frametitle{400 Nanometer Device With $\Phi_{\text{app.}} = 0$}
\begin{figure}[h!]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.16]{./Images/NANO_DARK_DENSITY.png}
&
\includegraphics[scale=0.16]{./Images/NANO_ILLUM_DENSITY.png} \\
\includegraphics[scale=0.16]{./Images/NANO_DARK_CURRENT.png}
&
\includegraphics[scale=0.16]{./Images/NANO_ILLUM_CURRENT.png}
\end{tabular}
\end{figure}

\end{frame}



\begin{frame}
\frametitle{Effects Of Domain Size On Performance}

\textbf{ n-type Silicon Parameter Values:}
\begin{small}
\begin{table}
\begin{tabular}{|cc|}
\hline
$\rho_{n}^{e} = 2\times 10^{16} \, [\text{cm}^{-3}] $ & $ \rho_{p}^{e} = 0 \, [\text{cm}^{-3}] $ \\
\hline
 $\mu_{n} = 1350 \, [\text{V cm s}^{-1}]$ & $\mu_{p} = 480 \, [\text{V cm s}^{-1}]$ \\
\hline
$k_{et} = 10^{-19} \, [\text{cm}^{4} \text{s}^{-1} ]$ &
$k_{ht} = 10^{-14} \, [\text{cm}^{4} \text{s}^{-1} ]$ 
 \\
\hline
$\epsilon_{r}^{S} = 11.9$ & \\
\hline
\end{tabular}
\end{table}
\end{small}

\textbf{Electrolyte Parameter Values:}
\begin{small}
\begin{table}
\begin{tabular}{|cc|}
\hline
$\rho_{r}^{\infty} = 30 \times 10^{16} \, [\text{cm}^{-3}] $ & $  \rho_{o}^{\infty} = 29 \times 10^{16}  \, [\text{cm}^{-3}] $ \\
\hline
$\mu_{r} = 1 \, [\text{V cm s}^{-1}]$ & $\mu_{o} = 1 \, [\text{V cm s}^{-1}]$ \\
\hline
 $\epsilon_{r}^{E} = 1000 $  & \\
\hline
\end{tabular}
\end{table}
\end{small}

\end{frame}

\begin{frame}
\frametitle{Effects Of Device Size On Performance}
\begin{figure}
\centering
\includegraphics[scale=0.33]{./Images/Micro_vs_Nano_w_units.png} 
\end{figure}
\begin{itemize}
\item 2 $\mu$m device: $\eta_{\text{eff}} \ = \ 4.3\%, \ ff  = 0.524$.
\smallskip
\item 400 nm device: $\eta_{\text{eff}} \ = \ 3.1\%, \ ff 
 = 0.411$.
\medskip
\item \textbf{Currents due to minority carrier transfer is effected by size.}
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Effects Of Domain Size On Electric Fields}
\begin{figure}[h!]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.15]{./Images/elec_micro.jpeg}
&
\includegraphics[scale=0.15]{./Images/elec_nano.jpeg}
\end{tabular}
\end{figure}
\textbf{The depletion region is larger than the nanostructured device!}
\end{frame}



\begin{frame}
\frametitle{Improving Efficiency With Minority Transfer Rates}
\begin{columns}
\begin{column}{6cm}
\begin{figure}
\centering
\includegraphics[scale=0.3]{./Images/micro_transfer_rates.png} 
\end{figure}
\end{column}
\hspace{0.25cm}
\begin{column}{3.75cm}
\begin{itemize} 
\begin{footnotesize}
\item \textcolor{red}{Fast:}  \\
$k_{ht} =  10^{-12} \, [\text{cm}^{4} \text{s}^{-1} ]$ \\
			$\eta_{\text{eff}} =  6.3\%$  \\
			$ff = 0.671$
\bigskip
\item \textcolor{green}{Medium:} \\
$k_{ht} =  10^{-14} \, [\text{cm}^{4} \text{s}^{-1} ]$ \\
			$\eta_{\text{eff}} = 4.3\%$\\
			$ff = 0.524$ \\
\bigskip
\item \textcolor{blue}{Slow:} \\$k_{ht} =  10^{-16} \, [\text{cm}^{4} \text{s}^{-1} ]$ 
			$\eta_{\text{eff}} = 2.2\%$ \\
			$ff = 0.280$
\end{footnotesize}
\end{itemize}
\end{column}
\end{columns}
\bigskip
\textbf{Faster minority transfer rates improves performance at microscale.}
\end{frame}

\begin{frame}
\frametitle{Improving Efficiency With Minority Transfer Rates}
\begin{columns}
\begin{column}{5cm}
\begin{figure}
\includegraphics[scale=0.17]{./Images/nano_transfer_rates.jpeg} 
\end{figure}
\end{column}
\hspace{0.25cm}
\begin{column}{3.75cm}
\begin{itemize} 
\begin{footnotesize}
\item \textcolor{red}{Fast:}  \\
$k_{ht} =  10^{-12} \, [\text{cm}^{4} \text{s}^{-1} ]$ \\
			$\eta_{\text{eff}} =  4.8\%$  \\
			$ff = 0.561$
\bigskip
\item \textcolor{green}{Medium:} \\
$k_{ht} =  10^{-14} \, [\text{cm}^{4} \text{s}^{-1} ]$ \\
			$\eta_{\text{eff}} = 3.1\%$\\
			$ff = 0.411$ \\
\end{footnotesize}
\end{itemize}
\end{column}
\end{columns}
\medskip
\begin{itemize}
\item Similar results at nanoscale, but still room for improvement.
\smallskip
\item \textbf{Choosing faster minority transfer rates is non-physical.}
\end{itemize}



\end{frame}

\begin{frame}
\frametitle{Improving Efficiency With Non-Uniform Doping}
\begin{figure}[h!]
Compare uniform and non-uniform majority doping profiles with $k_{ht} = 10^{-12} \ [\text{cm}^{4} \ \text{s}^{-1}]$
\bigskip

\centering
\begin{tabular}{cc}
\includegraphics[scale=0.15]{./Images/uniform_doping.jpeg}
&
\includegraphics[scale=0.15]{./Images/non_uniform_doping.jpeg}
\end{tabular}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Improving Efficiency With Non-Uniform Doping}
Electric fields with uniform and non-uniform majority doping profiles.
\medskip
\begin{figure}[h!]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.15]{./Images/elec_nano_kht_12.jpeg}
&
\includegraphics[scale=0.15]{./Images/elec_nano_kht_12_step_doping.jpeg}
\end{tabular}
\end{figure}


\textbf{Depletion region is now within the device.}

\end{frame}

\begin{frame}
\frametitle{Improving Efficiency With Non-Uniform Doping}
\begin{columns}
\begin{column}{6cm}
\begin{figure}
\centering
\includegraphics[scale=0.16]{./Images/IV_doping.jpeg} 
\end{figure}
\end{column}
\hspace{0.25cm}
\begin{column}{4cm}
\begin{itemize} 
\begin{small}
\item \textcolor{red}{Uniform Doping:}  \\
			$\eta_{\text{eff}} =  4.8\%$  \\
			$ff = 0.561$
\bigskip
\item \textcolor{green}{Non-Uniform Doping:} \\
			$\eta_{\text{eff}} = 4.9\%$\\
			$ff = 0.634$ \\
\end{small}
\end{itemize}
\end{column}
\end{columns}
\bigskip
\begin{itemize}
\item \textbf{Non-uniform doping improves fill factor, but also reduces $\Phi_{\text{OC}}$.}
\smallskip
\item \textbf{400 nm device still has smaller  $\textbf{J}_{\text{SC}}$ than 2 $\mu$m device.}
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Future Directions}
\begin{columns}
\begin{column}{3cm}
\begin{figure}
\centering
\includegraphics[scale=0.5]{./Images/Cylinder.jpg} 
\caption{\tiny{Source: J. Fowley, et. al., 2001}}
\end{figure}
\end{column}
\begin{column}{7cm}
\begin{itemize}
\item Complete the convergence study of underlying numerical methods.
\bigskip
\item Finish 2-D code using deal.ii library.
\bigskip
\item Explore the effect interface geometry has on energy conversion efficiency.
\end{itemize}
\end{column}
\end{columns}
\end{frame}








\begin{frame}
\begin{center}
\Large{\textbf{Thank You!}}
\end{center}
\end{frame}









\end{document}