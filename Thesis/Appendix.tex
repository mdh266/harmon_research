\chapter{Appendix}

\section{LDG Analysis Appendix}

\subsection{Lipschitz Property of Recombination}
\label{sec:LipschitzRecombination}



\noindent
\textbf{Proof of Thoerem~\ref{thm:SRH_Lipschitz}:} The proof of the Lipschitz property of the function~\eqref{eq:Shockley_Read_Hall_Recombination} comes from its multilinearity.  We write this as a general function,
\begin{equation}
R(x, y) \; \coloneqq \; \frac{A  \; - \; x \; y }{C  \; + \; \alpha \; x \; + \; \beta \; y }.
\end{equation}
\noindent
Where $x \, = \, \rho_{n}, \; y \, = \, \rho_{p}, \, A \, = \, \rho_{i}^{2}, \; C \, = \, \tau_{n} \, \rho_{n}^{t} \, + \, \tau_{p} \, \rho_{p}^{t}, \; \alpha \, = \, \tau_{n}, \; \text{and} \; \beta \, = \, \tau_{p}$.  We now look at the difference,
\begin{spreadlines}{0.75\baselineskip}
\begin{equation}
\begin{aligned}
&R(x_{1}  ,  y_{1} ) 
 -  R(x_{2},  y_{2}) &  \\
&=  
\frac{A   -  x_{1}  y_{1} }{C   + \alpha x_{1}  +  \beta  y_{1} } 
- 
\frac{A - x_{2}  y_{2} }{C   +  \alpha  x_{2}  + \beta y_{2} }\\
&=  
\frac{(A  \alpha  +  \beta y_{1}  y_{2}  + C  y_{1})(x_{2}  -  x_{1})  +  (A  \beta  +  \alpha  x_{1} x_{2}  + C  x_{2})(y_{2}  -  y_{1})}
{C^{2}  +  C  \alpha ( x_{1}  +  x_{2}) + C  \beta (y_{1} +  y_{2})  + \alpha \beta ( x_{1}  y_{2}  +  x_{2}  y_{1} )  + 
 \alpha^{2} x_{1}  x_{2}  + \beta^{2}  y_{1}  y_{2} }
\end{aligned}
\end{equation}
\end{spreadlines}


\noindent
Fixing $y_{1} \, = \, y_{2} \, = \, y$ then,
\begin{equation}
\lim_{y \rightarrow  \infty} 
\vert  R(x_{1}  , y ) 
-  R(x_{2}, y ) \vert
 \leq  \frac{1}{\alpha}  \vert x_{1} - x_{2}  \vert,
\end{equation} 

\noindent
and doing the same for $x$,
\begin{equation}
\lim_{x  \rightarrow  \infty} 
\vert  R(x  ,  y_{1} ) 
-  R(x,  y_{2} )  \vert
\; \leq \; \frac{1}{\beta} \vert  y_{1}  -  y_{2}  \vert.
\end{equation} 


\noindent
This gives us,
\begin{equation}
\vert R(x_{1}  , y_{1} ) -  R(x_{2},  y_{2})  \vert
\; \leq \; \frac{1}{\alpha}  \vert x_{1}  - x_{2} \vert 
+ \frac{1}{\beta} \vert  y_{1} -  y_{2}  \vert.
\end{equation}


\noindent
For the case of Shockley-Reed-Hall recombination with the form~\eqref{eq:Shockley_Read_Hall_Recombination} we obtain, 
\begin{equation}
\vert  R(\rho_{n}^{1}  ,  \rho_{p}^{1} )  - R(\rho_{n}^{1},  \rho_{p}^{2})  \vert^{2}
\; \leq \; \frac{1}{\tau_{n}^{2}}   \vert  \rho_{n}^{1}  - \rho_{n}^{2}  \vert^{2} 
 +  \frac{1}{\tau_{p}^{2}}  \vert  \rho_{p}^{1} - \rho_{p}^{2} \vert^{2}.
\end{equation}

\noindent
Integrating over the domain $\Omega_{S}$ produces the desired results.
\begin{flushright}
$ \square $
\end{flushright}



\subsection{Derivation Of Primal Form Of LDG Method}
\label{sec:Derivation_Primal}

In this section we derive the primal form of the LDG formulation discussed presented in Chapter~\ref{ch:Analysis}. To simplify the presentation of the discretization, we only consider the drift-diffusion equation for the electrons in the system~\eqref{eq:ElectronSystemPDE}-~\eqref{eq:ElectronSystemDirichlet}. The primal form of the LDG method for other charge carriers are derived in the same way.  The semidiscrete LDG approximation of~\eqref{eq:ElectronSystemPDE}-~\eqref{eq:ElectronSystemDirichlet} is,

\vspace{2mm}

\noindent
Find  $\rho_{n,h}(\cdot, t) \in W_{h,k}^{S}$ and $\textbf{q}_{n,h}(\cdot, t) \in \textbf{W}_{h,k}^{S}$ such that,
\begin{align}
\left(  w ,  \partial_{t}   \rho_{n,h}  \right)
+
\mathcal{L}_{n,S}(w, \textbf{w}; \rho_{n,h}, \textbf{q}_{n,h}) 
& +
\langle w , I_{et}(\mathcal{M}(\rho_{n,h} - \rho_n^e ), \, \mathcal{M}( \rho_{o,h})) \rangle_{\mathcal{E}_{\Sigma}^{S}} \nonumber \\
& \ = \ 
\left( w ,  \tilde{R}(\rho_{n,h}, \rho_{p,h} ) \right)_{\Omega_{S}} 
+
\langle w, \sigma_{n} \rho_{n}^{e} \rangle_{\mathcal{E}_{D}^{S}}
\label{eq:LDG_1_This} \\
\mathcal{A}_{n,S}(w, \textbf{w}; \rho_{n,h}, \textbf{q}_{n,h}) 
-
& \left(w , \alpha_{n} D_n^{-1} \mu_{n} \textbf{E} \rho_{n,h} \right)_{\Omega_{S}}
 \ = \ 
- \langle \textbf{n} \cdot \textbf{w}, \rho_{n}^{e} \rangle_{\mathcal{E}_{D}^{S}} \label{eq:LDG_2_This}
\end{align}
\noindent
for all $(w, \textbf{w}) \in W_{h,k}^{S} \times \textbf{W}_{h,k}^{S}$ and all $t\in(0,T)$. The quadlinear forms $\mathcal{L}_{n,S}$ and $\mathcal{A}_{n,S}$ are defined as,
\begin{spreadlines}{0.5\baselineskip}
\begin{align}
\mathcal{L}_{n,S} \, &= \,
-
\left(\nabla w, \textbf{q}_{n,h} \right)_{\Omega_S}
+
\langle \llbracket w \rrbracket,  \sigma_{n} \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_{h}^{i,S}}
+
\langle \llbracket w \rrbracket, 
\left\{\textbf{q}_{n,h} \right\} - \llbracket \textbf{q}_{n,h} \rrbracket \boldsymbol \beta \rangle_{\mathcal{E}_{h}^{i,S}} 
\nonumber \\
& \qquad + 
\langle w, \textbf{n} \cdot \textbf{q}_{n,h} \rangle_{\mathcal{E}_{D}^{S}}
+
\langle  w, \sigma_{n} \rho_{n,h} \rangle_{\mathcal{E}_{D}^{S}} , \label{eq:LDG_L_This}\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mathcal{A}_{n,S} \, &= \,  \left(\textbf{w}, D_n^{-1} \textbf{q}_{n,h} \right)_{\Omega_S}
-
\left(\nabla \cdot \textbf{w}, \rho_{n,h} \right)_{\Omega_S} 
+ 
\langle  \llbracket \textbf{w} \rrbracket, \left\{\rho_{n,h} \right\} + \boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_h^{i,S}} \nonumber \\
& \qquad +
\langle w, \rho_{n,h} \rangle_{\mathcal{E}_{N}^{S} \cup \mathcal{E}_{\Sigma}^{S}}.
\label{eq:LDG_A_This}
\end{align}
\end{spreadlines}


\noindent
Substituting in~\eqref{eq:LDG_A_This} into~\eqref{eq:LDG_2_This} we rearrange the terms to obtain the equation,
\begin{spreadlines}{0.5\baselineskip}
\begin{equation}
\begin{aligned}
\left(\textbf{w}, D_n^{-1} \textbf{q}_{n,h} \right)_{\Omega_S}
&-
\left(\textbf{w} ,   \alpha_{n}D_n^{-1} \mu_{n} \textbf{E} \rho_{n,h} \right)_{\Omega_{S}}
 -
\left(\nabla \cdot \textbf{w}, \rho_{n,h} \right)_{\Omega_S} 
\\
&+ \langle  \llbracket \textbf{w} \rrbracket, \left\{\rho_{n,h} \right\} + \boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_h^{i,S}} 
+
\langle \textbf{n}\cdot \textbf{w}, \rho_{n,h} \rangle_{\mathcal{E}_{N}^{S} \cup \mathcal{E}_{\Sigma}^{S}} \\
&+
\langle \textbf{n} \cdot \textbf{w}, \rho_{n}^{e} \rangle_{\mathcal{E}_{D}^{S}}
\ = \ 0.
\end{aligned}
\label{eq:Q_1}
\end{equation}
\end{spreadlines}

\noindent
Using Lemma~\ref{lem:IBP_BROKEN_SOBOLEV} on the third term in~\eqref{eq:Q_1} yields,
\begin{spreadlines}{0.5\baselineskip}
\begin{equation}
\begin{aligned}
\left(\textbf{w}, D_n^{-1} \textbf{q}_{n,h} \right)_{\Omega_S}
&-
\left(\textbf{w} , \alpha_{n}  D_{n}^{-1} \mu_{n} \textbf{E} \rho_{n,h} \right)_{\Omega_{S}}
 +
\left( \textbf{w}, \nabla_{h} \rho_{n,h} \right)_{\Omega_S}  
 - 
\langle  \llbracket \textbf{w} \rrbracket,  \left\{\rho_{n,h} \right\} \rangle_{\mathcal{E}_h^{i,S}} \\
& - 
\langle  \left\{ \textbf{w} \right\},  \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_h^{i,S}}
-
\langle \textbf{n}\cdot \textbf{w}, \rho_{n,h} \rangle_{\mathcal{E}_{D}^{S} \cup \mathcal{E}_{N}^{S} \cup \mathcal{E}_{\Sigma}^{S}}
\\
&+ \langle  \llbracket \textbf{w} \rrbracket,  \left\{\rho_{n,h} \right\} + \boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_h^{i,S}} 
+
\langle \textbf{n}\cdot \textbf{w}, \rho_{n,h} \rangle_{\mathcal{E}_{N}^{S} \cup \mathcal{E}_{\Sigma}^{S}} \\
&+
\langle \textbf{n} \cdot \textbf{w}, \rho_{n}^{e} \rangle_{\mathcal{E}_{D}^{S}}
\ = \ 0.
\end{aligned}
\label{eq:Q_2}
\end{equation}
\end{spreadlines}

\noindent
Upon simplifying the above we have,
\begin{spreadlines}{0.5\baselineskip}
\begin{equation}
\begin{aligned}
\left(\textbf{w}, D_n^{-1} \textbf{q}_{n,h} \right)_{\Omega_S}
&-
\left(\textbf{w} , \alpha_{n} \mu_{n}  \textbf{E} \rho_{n,h} \right)_{\Omega_{S}}
+
\left( \textbf{w}, \nabla_{h} \rho_{n,h} \right)_{\Omega_S}  
+
\langle  \llbracket \textbf{w} \rrbracket,\boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_h^{i,S}} \\
&
- 
\langle  \left\{ \textbf{w} \right\},  \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_h^{i,S}}
-
\langle \textbf{n} \cdot \textbf{w}, (\rho_{n,h} - \rho_{n}^{e}) \rangle_{\mathcal{E}_{D}^{S}}
\ = \ 0.
\end{aligned}
\label{eq:Q_3}
\end{equation}
\end{spreadlines}


\noindent
Using the lift operators~\eqref{eq:r_On_Interior},~\eqref{eq:l_On_Interior}, and~\eqref{eq:r_D_On_Boundary} then~\eqref{eq:Q_3} becomes,
\begin{spreadlines}{0.5\baselineskip}
\begin{equation}
\begin{aligned}
\left(\textbf{w}, D_n^{-1} \textbf{q}_{n,h} \right)_{\Omega_S}
&-
\left(\textbf{w} , \alpha_{n} \mu_{n} \textbf{E} \rho_{n,h} \right)_{\Omega_{S}}
+
\left( \textbf{w}, \nabla_{h} \rho_{n,h} \right)_{\Omega_S}  \\
& 
-
\left( \textbf{w} ,  \textbf{l}^{S}(\boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket)  \right)_{\Omega_{S}} 
- 
\left( \textbf{w} ,  \textbf{r}^{S}(\llbracket \rho_{n,h} \rrbracket)  \right)_{\Omega_{S}} \\
&
-
\left( \textbf{w} ,  \textbf{r}_{D}^{S}( \rho_{n,h} - \rho_{n}^{e} )  \right)_{\Omega_{S}}
\ = \ 0.
\end{aligned}
\end{equation}
\end{spreadlines}

\noindent
Rearranging terms yields,
\begin{equation}
\begin{aligned}
( \textbf{w}, D_n^{-1} \textbf{q}_{n,h} 
-
\alpha_{n} \mu_{n} \textbf{E} \rho_{n,h} 
+
\nabla_{h} \rho_{n,h} 
& 
- 
\textbf{r}^{S}(\llbracket \rho_{n,h} \rrbracket) 
-
\textbf{l}^{S}(\boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket) 
\\
&-
\textbf{r}_{D}^{S}(\rho_{n,h} - \rho_{n}^{e} )  )_{\Omega_{S}} 
\ = \ 0.
\end{aligned}
\label{eq:Q_4}
\end{equation}

\noindent
Noting that~\eqref{eq:Q_4} holds for all $\textbf{w} \in \textbf{W}_{h,k}^{S}$ then,
\begin{equation}
\begin{aligned}
\textbf{q}_{n,h} 
\ = \
& \alpha_{n} \mu_{n} \textbf{E} \rho_{n,h} \nonumber \\
&+
D_{n} 
\left(
-
\nabla_{h} \rho_{n,h} 
+ 
\textbf{r}^{S}(\llbracket \rho_{n,h} \rrbracket)
+
\textbf{l}^{S}(\boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket) 
+
\textbf{r}_{D}^{S}(\rho_{n,h} - \rho_{n}^{e} 
\right) \label{eq:Q_h_ID}
\end{aligned}
\end{equation}

\noindent
in $\Omega_{S}$ and all $t\in(0,T)$.  


\vspace{2mm}

Now we return to~\eqref{eq:LDG_L_This}. Substituting in the definitions of the lift operators~\eqref{eq:r_On_Interior},~\eqref{eq:l_On_Interior}, and~\eqref{eq:r_D_On_Boundary} into~\eqref{eq:LDG_L_This} to obtain,
\begin{spreadlines}{0.5\baselineskip}
\begin{equation}
\begin{aligned}
\mathcal{L}_{n,S} \, &= \,
-
\left(\nabla_{h} w, \textbf{q}_{n,h} \right)_{\Omega_S}
+
\langle \llbracket w \rrbracket,  \sigma_{n} \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_{h}^{i,S}}
+
\langle \llbracket w \rrbracket, 
\left\{\textbf{q}_{n,h} \right\} - \llbracket \textbf{q}_{n,h} \rrbracket \boldsymbol \beta \rangle_{\mathcal{E}_{h}^{i,S}} 
\nonumber \\
& \qquad + 
\langle w, \textbf{n} \cdot \textbf{q}_{n,h} \rangle_{\mathcal{E}_{D}^{S}}
+
\langle  w, \sigma_{n} \rho_{n,h} \rangle_{\mathcal{E}_{D}^{S}}  \\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
&= \,
-
\left(\nabla_{h} w, \textbf{q}_{n,h} \right)_{\Omega_S}
+
\left(\textbf{r}^{S} (\llbracket w \rrbracket ), \textbf{q}_{n,h} \right)_{\Omega_S}
+
\left(\textbf{l}^{S} (\boldsymbol \beta \cdot \llbracket w \rrbracket ), \textbf{q}_{n,h} \right)_{\Omega_S}
\\
& \qquad + 
(  \textbf{r}_{D}^{S} (w), \textbf{q}_{n,h} )_{\Omega_{S}}
+
\langle \llbracket w \rrbracket,  \sigma_{n} \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_{h}^{i,S}}
+
\langle  w, \sigma_{n} \rho_{n,h} \rangle_{\mathcal{E}_{D}^{S}}  \\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
&= \,
(-\nabla_{h} w
+
\textbf{r}^{S} (\llbracket w \rrbracket )
+
\textbf{l}^{S} (\boldsymbol \beta \cdot \llbracket w \rrbracket )
+
\textbf{r}_{D}^{S} (w), \textbf{q}_{n,h} )_{\Omega_{S}}\\
& \qquad 
+
\langle \llbracket w \rrbracket,  \sigma_{n} \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_{h}^{i,S}}
+
\langle  w, \sigma_{n} \rho_{n,h} \rangle_{\mathcal{E}_{D}^{S}}.
\end{aligned}
\end{equation}
\end{spreadlines}


\noindent
Substituting this in to~\eqref{eq:LDG_1_This} yields,
\begin{equation}
\begin{aligned}
\left( w ,  \partial_{t}   \rho_{n,h}  \right)
+
(-\nabla_{h} w
+
\textbf{r}^{S} (\llbracket w \rrbracket )
&+
\textbf{l}^{S} (\boldsymbol \beta \cdot \llbracket w \rrbracket ) 
+
\textbf{r}_{D}^{S} (w), \textbf{q}_{n,h} )_{\Omega_{S}} \\
& +
\langle w , I_{et}(\rho_{n,h} - \rho_n^e , \,  \rho_{o,h}) \rangle_{\mathcal{E}_{\Sigma}^{S}} \nonumber \\
& \ = \ 
\left( w ,  \tilde{R}(\rho_{n,h}, \rho_{p,h} ) \right)_{\Omega_{S}} 
+
\langle  w, \sigma_{n} \rho_{n}^{e} \rangle_{\mathcal{E}_{D}^{S}}
\end{aligned}
\end{equation}

\noindent
Now substituting~\eqref{eq:Q_h_ID} into the above we arrive at the primal form of~\eqref{eq:LDG_1_This}-~\eqref{eq:LDG_2_This},
\begin{equation}
\begin{aligned}
\left( w ,  \partial_{t}   \rho_{n,h}  \right)
&+
B_{n,S} (w,\rho_{n,h})  
+
\langle w , I_{et}(\mathcal{M}(\rho_{n,h} - \rho_n^e ), \, \mathcal{M}( \rho_{o,h})) \rangle_{\mathcal{E}_{\Sigma}^{S}} \\
& \ = \ l
\left( w ,  \tilde{R}(\rho_{n,h}, \rho_{p,h} ) \right)_{\Omega_{S}} 
+
\langle  w, \sigma_{n} \rho_{n}^{e} \rangle_{\mathcal{E}_{D}^{S}} \\
& \qquad -
(D_{n} (-\nabla_{h} w
+
\textbf{r}^{S} (\llbracket w \rrbracket )
+
\textbf{l}^{S} (\boldsymbol \beta \cdot \llbracket w \rrbracket)
+
\textbf{r}_{D}^{S} (w)), \textbf{r}_{D}^{S} (\rho_{n}^{e}) )_{\Omega_{S}},
\end{aligned}
\end{equation}

\noindent
where we have defined the bilinear form $B_{n,S}: W_{h,k}^{S} \times W_{h,k} ^{S} \rightarrow \mathbb{R} $ for all $t \in (0,T)$ as, 
\begin{spreadlines}{0.5\baselineskip}
\begin{equation}
\label{eq:BilinearFormElectrons}
\begin{aligned}
B_{n,S} & (w,\rho_{n,h}) \; = \; \\
&(-\nabla_{h} w
+
\textbf{r}^{S} (\llbracket w \rrbracket )
+
\textbf{l}^{S} (\boldsymbol \beta \cdot \llbracket w \rrbracket) 
+
\textbf{r}_{D}^{S} (w),  \\
& \quad D_{n} (-
\nabla_{h} \rho_{n,h} 
+ 
\textbf{r}^{S}(\llbracket \rho_{n,h} \rrbracket)
+
\textbf{l}^{S}(\boldsymbol \beta \cdot \llbracket \rho_{n,h} \rrbracket) 
+
\textbf{r}_{D}^{S} (\rho_{n,h}) ) \,  )_{\Omega_{S}}
\\
& \quad - (\nabla_{h} w
-
\textbf{r}^{S} (\llbracket w \rrbracket)
-
\textbf{l}^{S} (\boldsymbol \beta \cdot \llbracket w \rrbracket ) 
-
\textbf{r}_{D}^{S} (w), 
 \alpha_{n} \mu_{n} \textbf{E} \rho_{n,h} \,  )_{\Omega_{S}} \\
& \quad 
+
\langle \llbracket w \rrbracket,  \sigma_{n} \llbracket \rho_{n,h} \rrbracket \rangle_{\mathcal{E}_{h}^{i,S}}
+
\langle  w, \sigma_{n} \rho_{n,h} \rangle_{\mathcal{E}_{D}^{S}}.
\end{aligned}
\end{equation}
\end{spreadlines}

