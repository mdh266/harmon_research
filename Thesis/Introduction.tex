%\documentclass[10pt]{report}
%\usepackage{stmaryrd}
%\usepackage{amsmath}
%\usepackage{amsfonts}
%\usepackage{amssymb}
%\usepackage{comment}
%\usepackage{mathtools}
%\usepackage{psfig}
%
%\usepackage{fullpage}
%
%\usepackage{graphicx}
%\usepackage{float}
%\usepackage[caption = false]{subfig}
%\usepackage[usenames,dvipsnames]{color}
%
%\newtheorem{theorem}{Theorem}
%
%\usepackage{enumitem}
%\usepackage{calc}
%
%\usepackage[openbib]{currvita}
%
%
%\linespread{1.5}
%
%\numberwithin{equation}{section}
%
%
%\begin{document}



\chapter{Introduction}


\section{Background and Motivation}
Large-scale utilization of photovoltaic (PV) devices, or solar cells, has been hampered for years due to high costs and lack of energy storage mechanisms.  Photoelectrochemical solar cells (PECs), also known as semiconductor-electrolyte solar cells, are an attractive alternative to conventional solid state PV devices.  PECs such as those depicted in Figure \ref{f:PEC1} are able to directly convert solar energy into hydrogen fuel.  The hydrogen fuel can then be used at a later time to generate electricity. In the typical setup depicted in Figure \ref{f:PEC1}, a PEC consists of four main components: the solid semiconductor electrode, the liquid electrolyte component, the semiconductor-electrolyte interface and the counter (metal or semiconductor) electrode.  When sunlight shines on the semiconductor component, photons are absorbed and generate electron-hole pairs. These electrons and holes are separated by a built-in electric field within the semiconductor. The separation of the electrons and holes leads to an electrical current in the cell and the accumulation of charges at the semiconductor-electrolyte interface.  At the interface, the photo-generated electrons or holes induce a chemical reaction at the semiconductor electrode.  A similar chemical reaction also occurs at the counter electrode. These chemical reactions create or eliminate reductant-oxidant (redox) species in the electrolyte.  The transport of the produced redox pairs through the electrolyte results in a continuous current across the device.



\begin{figure}[htb]
\begin{center}
\includegraphics[scale=0.55]{./Images/PEC.eps}
\caption{Typical set up of a photoelectrochemical solar cell.  Here $h\nu$ is a photon, $e^{-}$ and $h^{+}$ are the photo-generated electron-hole pair, and $A/A^{-}$ are the redox species~\cite{Lewis-JPhysChemB1998}.
}
\end{center}
\label{f:PEC1}		 
\end{figure}


Research on PECs has traditionally focused on planar cell designs, but recently there has been interest in cell designs that use thin nanostructured wires such as those depicted in Figure \ref{f:PEC2}.  In a planar device (Figure \ref{f:PEC2} A), photo-generated electrons and holes are collected in directions parallel to photon absorption. In order for PECs' to achieve sufficient energy conversion efficiencies to be commercially viable the electron/hole diffusion length ($L_{D}$) (the average distance an electron/hole can be travel without being eliminated) must be larger than the absorption length ($1/\alpha$) (the average distance a photon will penetrate the semiconductor crystal before generating an electron-hole pair).  This constraint necessitates the use of expensive, high quality crystals  that either have large diffusion lengths or small absorption lengths. In PECs that use a nanowire design (Figure \ref{f:PEC2} B) the electron/hole collection and photon absorption directions are decoupled, thereby alleviating the need for high quality crystals to attain sufficient energy conversion efficiencies.

\begin{figure}[htb]
\begin{center}
\includegraphics[scale=0.30]{./Images/planar_vs_wire.eps} 
\caption{Comparison of photon absorption and carrier collection in planar (A) and nanowire (B) solar cells~\cite{WaWaMcBoMiSaLe-ChemRev2010}. }
\label{f:PEC2}
\end{center}
\end{figure}




The physical mechanisms of charge creation, elimination and transport in the semiconductor and the electrolyte components are well-understood. The most popular mathematical model used to describe these processes is a macroscopic reactive-flow-transport system of partial differential equations.  In the semiconductor community this system is known as the drift-diffusion-Poisson equations \cite{MaRiSc-Book1990}, while in the electrochemistry community it is called the Plank-Nernst-Poisson equations \cite{BaFa-Book2001}.  The physics of charge transfer between the components of the PEC is far less understood, and the appropriate mathematical model to describe the chemical reactions is still under debate \cite{Lewis-JPhysChemB1998,BaThAj-PhysRevE2004,Memming-Book2015,KeHaLu-JPhysChem2015}.  However, it is well-documented that the overall performance of photoelectrochemical solar cells is highly dependent on the chemical reactions at the solid-liquid interface \cite{Lewis-JPhysChemB1998}. Therefore a through understanding of the dynamics of charge transfer at the semiconductor-electrolyte junction is essential for designing efficient photoelectrochemical solar cells.

The standard model of the interfacial reactions uses the product of electron-oxidants and hole-reductant densities at the interface. The nonlinear coupling of electron-hole pairs and reductant-oxidant pairs across the interface poses serious challenges from the mathematical and computational perspective. In most previous studies of PECs, researchers avoided the complications of the coupled nonlinear  interface conditions by using the so-called ``Schottky approximation."  In this approximation, one linearizes the interface conditions by treating the density of reductants and oxidants as constants.  Simulations based on this procedure, while valid in some instances, have been shown to produce results which deviate from the results produced by simulations that use the nonlinear coupled interface conditions \cite{HeGaLeRe-SIAM15}. In order to accurately quantify the performance of PECs simulations that use the coupled nonlinear reactive interface conditions need to be conducted. 

The main challenges in constructing a numerical algorithm that produces a reliable simulation of PECs are due to the nonlinearities arising, not only, from the coupling of the corresponding drift diffusion (transport) equations to the Poisson equation in both the semiconductor region and the electrolyte region, respectively, but also, with the nonlinear reactive interface conditions on semiconductor-electrolyte interface.  While there have been many contributions in the last forty years on the approximations to  solution of a single  drift-diffusion-Poisson system in the semiconductor region, there is no previous work, to the best of our knowledge, that deals with the whole coupled system that incorporates the nonlinear interface conditions for photoelectrochemical solar cells.  In addition, the evolution problem under consideration is effectively multi-scale  in the sense that the evolution of the system in the semiconductor and the corresponding one in the electrolyte evolve at different time scales due to the quantitative scaling differences in the relevant  physical parameters such as mobilities or characteristic charge densities.  The nonlinearity and multi-scale nature of the semiconductor-electrolyte interface place severe constraints on our choice of discretization strategy.


In particular, in order to ensure computational stability, the size of the time step (chosen for any time discretized scheme) is dominated by the value in the fast-varying component, that is, the semiconductor component. Using small time steps in the numerical simulations for the problem in the whole domain results in regions of stiffness caused by boundary layer formation where sharp transitions in  densities and  electric potential occur (i.e. near interfaces). This problem can be tackled by using fine enough meshes around the interface that would resolve these boundary layers. Such considerations limit even further the time step sizes on the  employed computational method.


In this dissertation we propose, implement and analyze numerical algorithms for the simulation of semiconductor-electrolyte solar cells on the mathematical model developed in \cite{HeGaLeRe-SIAM15}.  To simplify the presentation we neglect the dynamics of the counter electrode and only consider the interaction of the semiconductor and the electrolyte.  Spatial discetizations of the drift-diffusion-Poisson equations are based on local discontinuous Galerkin (LDG) methods and mixed finite element methods respectively.  To alleviate the stiffness of the equations we develop and analyze implicit-explicit (IMEX) time stepping routines.  We then analyze our numerical approximations and prove their convergence under mesh refinement.  Finally, we present results from numerical experiments in order to develop a strategy for optimizing solar cell design at the nano-scale.  In the next section we briefly review previous studies in this research domain.  

\section{Literature Review}

\subsection{Photoelectrochemical Solar Cells}

The first modern (silicon) solar cell was invented in the 1954 by Chapin, Fuller and Pearson \cite{Nelson-Book2003}, however, research in using solar cells for terrestrial energy production remained minimal until the energy crisis of the 1970's.  Since that time the field of photovoltaics has been greatly boosted by the research of semiconductor devices and the simulations tools developed to analyze them. The majority of these simulations have used the drift-diffusion-Poisson equations to model the transport of electrons and holes in semiconductors. The drift-diffusion-Poisson equations was first proposed by Van Roosbeck in 1950 \cite{Roosbeck-BSTJ950} and has remained the standard model for semiconductor devices since it represents a good balance between accuracy and computational efficiency \cite{MaRiSc-Book1990}. However, the drift-diffusion-Poisson equations still pose many challenges from the computational perspective.  Indeed the system is notorious for its stiffness, nonlinearity, and the locality of its physical behavior \cite{Selberherr-Book1984,MaRiSc-Book1990,CaPaBo-VLSIDesign2000}. With the introduction of VLSI-circuits and the decreasing size of solid state devices, there has been a increased need for more complicated models which incorporate quantum effects \cite{MaRiSc-Book1990,VaGo-Book2006,Jungel-Book2009}.  However, the inclusion of quantum effects through more advanced models is not necessary for most photovoltaic devices~\cite{Nelson-Book2003}.  


The first solid-liquid junction solar cell was invented by Becquerel in 1839 and consisted of platinum electrodes and silver chloride dissolved in a acidic solution \cite{Nelson-Book2003}.   Electrochemical systems such as the one developed by Becquerel were later analyzed by  Helmholtz Gouy, Debye, Stern and Chapman by treating them as electrical circuits and fitting model parameters with experimental results~\cite{BaFa-Book2001,NeAl-Book2004,BaThAj-PhysRevE2004,Memming-Book2015}.  In this theory, the formation of boundary layers of charge in the electrolytes are treated as parallel-plate capacitors.  While this discovery produced great insight into the basic properties of electrochemical systems, models that  require less ad-hoc assumptions were desired.  Researchers now model the transport of ions in electrolytes by using the Planck-Nernst-Poisson (PNP) equations \cite{PaJe-SIAM1997,Eisenberg-SM2000,BiFuBa-RJE2012,HoLiLiEi-JPC2012}.  Since the PNP equations are equivalent to the drift-diffusion-Poisson equations they share many of the same mathematical and computational difficulties. Additionally, in electrochemical systems there is still debate on how to appropriately describe the chemical reactions at the electrode-electrolyte interface as boundary conditions to the PNP equations~\cite{Lewis-JPhysChemB1998,BaThAj-PhysRevE2004,Memming-Book2015,KeHaLu-JPhysChem2015}.

The chemical reactions at electrode-electrolyte interfaces involve the transfer of electrons between the solid and liquid.  The classical theory of electron transfer between electrodes and electrolytes was developed in the 1960's by Marcus \cite{Marcus-JChemPhys1956,Marcus-AnnRevPhysChem1964}, Hush~\cite{Hush-TrFarSoc1961}, and Geirscher~\cite{Gerischer-JElecSoc1966,Geirscher-SurSci1969}.   In the early 1970's Honda and Fujishima performed the first study to demonstrate that a semiconductor (in this case TiO$_{2}$) can be used to perform electrolysis on water~\cite{HoFu-Nat1972}. This discovery sparked interest in utilizing semiconductor-electrodes to produce hydrogen fuel from water.  In the last few decades there has been substantial effort to better understand the fundamental science of electron transfer at semiconductor electrode-liquid interfaces~\cite{Lewis-ACR1990,Gerischer-ElecActa1990,Lewis-ARPC1991,NoMe-JPhysChem1996,PoLe-JPhyChemB1997,Lewis-JPhysChemB1998,GaMar-JPC2000,GaGeMar-JPC2000,SiGuMuAlFi-JAP2009}.

Parallel to the research in electron transfer theory has been the effort to develop semiconductor-electrolyte (photoelectrochemical) solar cells that directly convert solar energy into hydrogen fuel.  Numerous device designs which use various semiconductor materials and electrolytes have been persued~\cite{Johnson-IEEE1981,Gratzel-Nature2001,BaFa-Book2001,FoPrFeMa-EES2011,McLeGr-ChemMat20013,Memming-Book2015}.   Recent theoretical efforts in semiconductor-electrochemistry have focused on modeling and simulation of devices using the drift-diffusion-Poisson equations with the goal of optimizing device performance. Particular interest has been focused on using nanostuctured wires to enhance carrier collection and therefore energy conversion efficiency~\cite{KeAtLe-JAP2005,FoPrFeMa-EES2011,WaBe-APL2011,DeJo-CompPhyComm2012,McLeGr-ChemMat20013,SaStGrBrAtLe-EnEnvSci2014,MiFuSh-PRL2014}. In most of these studies the investigators used the Schottky approximation which neglects the interaction of the semiconductor with the electrolyte altogether. The first study to perform simulations using the fully coupled nonlinear semiconductor-electrolyte interface was completed by He. et. al. \cite{HeGaLeRe-SIAM15}. In this study the authors demonstrated that using the Schottky approximation produces discrepancies when the concentration of redox species is comparable to the density of electron and holes.  The model that the authors introduce in  \cite{HeGaLeRe-SIAM15} is the model we shall adopt in this dissertation and will be discussed in Chapter~\ref{ch:Model}.


\subsection{Numerical Approximations}




The drift-diffusion-Poisson equations are an example of a more general class of mathematical problems called reactive-flow-transport systems. Over the last few decades significant effort has been invested in the numerical discretization of these problems. In addition to research in semiconductor and electrolytes, such systems play an active role in research of oil-recovery, contaminant transport and air-quality.  There are numerous discretization schemes that have been investigated on reactive-flow-transport problems~\cite{BaRoFi-SIAM1983,DoGaSq-MAC1986,GaSq-SIAM1989,JeKe-SIAM1991,ChCo-NumMath1995,Chen-SIAM1995,ArBrDaSaWaWh-JCAM1996,RiWh-CommNumMathEng2001,DaSuWh-CMAME2004,SuWh-AppNumMath2005,BaHe-JCP2013}. In this dissertation we will approximate solutions to our mathematical model using a hybrid spatial discretization.  To be specific we use a mixed finite element method (MFEM) to solve the Poisson equation and a discontinuous Galerkin (DG) method to solve the drift-diffusion equations.  In \cite{DaSuWh-CMAME2004}, the authors show that such a choice of numerical discretizations is a compatible choice of algorithms.  Compatibility of algorithms implies that the numerical method are locally mass conservative.  This is an extremely important property for numerical methods to have when simulating reactive-flow-transport problems \cite{DaSuWh-CMAME2004,Riviere-Book2008}.  DG methods and the MFEM both belong to a class of numerical methods called finite elements methods (FEM).  Finite element methods have been used extensively throughout science and engineering to approximation solutions to mathematical models involving partial differential equations~\cite{BeCaOd-Book1981,Snowden-Book1986,Markowich-Book1986,Johnson-Book1987}.  The finite element method relies on a weak formulation of the partial differential equation and the introduction of a mesh for the computational domain. Approximations to solutions of the weak formulation are obtained by expressing the solution as a linear combination of polynomials that are local to each element in the mesh.


The mixed finite element method (MFEM) is used to approximate solutions of Poisson's equation where both the primary variable and its gradient are needed.  This is achieved by rewriting the second order differential equation as two first order equations and introducing a weak formulation. The MFEM is an appealing method since it is mass conservative and produces approximations to gradients that are more accurate than solving for the primary variable using a traditional FEM and post processing to approximate its gradient~\cite{BrDoMa-NumMath1985,DaSuWh-CMAME2004,BeSc-Book2008}. The existence of solutions to the weak formulation of the mixed method is guaranteed by the Babuska-Brezzi or \textit{inf-sup} condition \cite{Babuska-NumMath1973,Brezzi-RAIRO1974}. However, it is well known that stability of the continuum problem does not guarantee stability of the finite element approximation \cite{BeSc-Book2008,BoBrFo-Book2013}.  Stable approximations to solutions of the Poisson equation by a mixed finite element method were first proposed by Raviart and Thomas \cite{RaTh-LectureNotes1989}. In their study, the authors used the Raviart-Thomas elements to approximate the gradient and discontinuous polynomials elements to approximate the primary variable. In addition, they proved that this choice of function spaces results in $\mathcal{O}(h^{k+1})$ (where $h$ is the maximum diameter of a mesh element) convergence rates for the approximation of both variables.  Since their initial discovery, mixed finite element methods have been used extensively in modeling subsurface flow and computational fluid dynamics \cite{DoEwWh-RAIRO1983,BrDoMa-NumMath1985,GlWh-SymposiumDomainDecomposition1988,ArWheZh-SIAM1996,ArBrDaSaWaWh-JCAM1996,BoBrFo-Book2013}.

Discontinuous Galerkin (DG) methods were first proposed by Reed and Hill \cite{ReHi-LASL1973} as a method for solving the neutron transport problem and later analyzed by Lesaint and Raviart \cite{LeRa-Book1974}.  The application of DG methods to more general hyperbolic problems was investigated by Cockburn and Shu in the seminal works on Runge-Kutta DG methods \cite{CoSh-JCP1989,CoSh-MC1990,CoSh-MC1991,CoSh-JCP1998}. The discontinuous Galerkin method is similar to the traditional finite element method (FEM) in its use of a weak formulation.  However, traditional FEM uses polynomial basis functions that are continuous across elements, while, DG methods use local polynomial basis functions that are discontinuous across elements in the mesh. Due to the discontinuous nature, DG approximations are essentially double valued on the element edges.  The uniqueness of DG approximations is enforced through the introduction of flux terms to the weak formulation.  The numerical fluxes also have the advantage of enhancing the stability and accuracy of the method~\cite{HeWa-Book2010}. DG methods have many desirable qualities including being locally mass conservative, the ability to capture sharp gradients and also have high order accuracy~\cite{Castillo-SIAM2002,Riviere-Book2008,HeWa-Book2010}.  Additionally, they are suitable for parallel implementation and can easily handle complex geometries.  One drawback of DG method compared to the traditional FEM is that DG methods have an increase in the number of unknowns or degrees of freedom.



The local discontinuous Galerkin (LDG) method is a numerical discretization which approximates higher order differential equations by rewriting them as systems of first order equations. In this way the LDG method is similar to the mixed finite element method. The key difference between the two methods is that the LDG method uses discontinuous polynomials basis functions for all the variables one is approximating.  The solution's regularity is enforced by penalizing the discontinuities in the solution across element faces.  The first application of the local discontinuous Galerkin method to parabolic problems was implemented on the Navier-Stokes equations by Bassi and Rebay \cite{BaRe-JCP1997}.  This method was later analyzed by Cockburn, Shu, Dawson and others for the general case of convection-diffusion equations \cite{CoSh-SIAM1998,CoDa-MAFELAP1999,CoSh-JCP2001,CaCoScsC-MathComp2002}. It was shown that in multiple dimensions the LDG method provides approximations to  densities (or more general primary variables) that converge with the rate $\mathcal{O}(h^{k})$ \cite{CoDa-MAFELAP1999}.  The LDG method is particularly useful for the case of convection dominated transport where there are step gradients in the solution. In such instances, traditional Galerkin finite element methods produce spurious oscillations in the computed solutions while DG methods remain stable~\cite{Johnson-Book1987}.  


The extension of the LDG method from parabolic problems to elliptic problems was proposed soon after its initial introduction \cite{CaCoPeSc-SIAM2000,CaCoPeSc-CommNumMethodEng2002,CoKanPeSc-SIAM2002,Kanschat-SIAM2003}. For elliptic problems, the convergence of approximations to variable gradients was proven to have a rate of $\mathcal{O}(h^{k})$. The convergence rate of approximations to the primary variable was shown to depend on the penalty parameter.  For the case where the penalty is of $\mathcal{O}(1)$ and $\mathcal{O}(h^{-1})$ the convergence rates were proven to be $O(h^{k+1/2})$ and $\mathcal{O}(h^{k+1})$ respectively \cite{CaCoPeSc-SIAM2000}.  The LDG method was subsequently unified with the so-called primal DG methods for solving general elliptic equations in the seminal work of Arnold et. al. \cite{ArBrCoMa-SJNA2002}.  The LDG method shares many of the attractive qualities of traditional DG methods. In addition to these, for the case of transport of charge carriers the LDG method provides approximations to both the charge carrier densities and their current fluxes.


Numerical methods which have a large number of unknowns, such as the MFEM and LDG method, require efficient computational routines for solving large systems of equations. Many numerical methods have been proposed to speed-up traditional computations. The two methods we investigate in this dissertation are non-overlapping domain decompositions and implicit-explicit (IMEX) time stepping techniques.  The first domain decomposition method was introduced by Schwarz in 1870 \cite{Schwarz-1870} for solving an elliptic problem on an irregular domain. Schwarz partitioned the domain into two over-lapping subdomains.  He solved the original problem by iteratively solving the solution on each subdomain and passing information between the two subdomains.  Such techniques are called Schwarz methods and have received much investigation in their application to parallel computations \cite{MiQuSa-JCP1995,QuVa-Book1999,Gander-ETNA2008,Mathew-Book2008}.  

In the late 1980's and early 1990's Lions investigated the theoretical foundation of Schwarz methods and their application to parallel computations. He was able to prove the convergence of overlapping Schwarz methods for linear elliptic problems \cite{Lions-DDM1989,Lions-FISDDM1988} and later extended this work to non-overlapping Schwartz methods \cite{Lions-TISDDM1990}. Since the 1990's interest in using domain decomposition methods for parallel computations has been replaced by the utilization of distributed linear solvers \cite{ToWi-Book2005}.  However, domain decomposition methods still remain useful as preconditioners \cite{ToWi-Book2005,Gander-ETNA2008} as well as in applications to partitioned analysis in multiphysics problems such as fluid-structure interaction \cite{GhLi-JCP1995,GhLi-IJCFD1998,FePaFa-CompMethAppMechEng1998} and fluid-fluid interaction \cite{CoHoLa-SIAM2009,CoHoLa-SIAM2012}.


Implicit-explicit (IMEX) or semi-implicit time stepping methods have been traditionally applied to convection-diffusion equations \cite{AsRuWe-SIAM1995,AsRuSp-ANM1997,LaTr-AppNumMath2012,Trenchea-JCAM2012}. The choice of underlying spatial discritization of the differential equation is of little consequence to the time marching procedure.  However, the use of IMEX time stepping in conjunction with LDG spatial discretizations on convection-diffusion equations was recently introduced by H. Wang et. al. \cite{WaShuZh-SIAM2015,WaShuZh-AMC2016}. The theory of IMEX time stepping is based on the observation that the time step required for stability of explicit methods on convective problems is $\Delta t = \mathcal{O}(h)$. In contrast, the time step for stability of explicit methods on diffusive problems is $\Delta t = \mathcal{O}(h^{2})$.  A constraint of $\mathcal{O}(h^{2})$ is quite severe, especially for problems that have boundary layers and where locally refined meshes are required.  In order to alleviate the constraint of using small time steps for convection-diffusion equations, one often treats the diffusive term using implicit time stepping and the convective term with explicit time stepping.  This allows one to use time steps that are on the order of $\mathcal{O}(h)$ for the convection-diffusion problems and can substantially reduce over all CPU run time of simulations. Furthermore, in convection-diffusion systems, the convection field is often a nonlinearly coupled term.  We will show in Chapter~\ref{ch:Results} that for our problem significant savings in CPU time can be achieved by treating the convective term explicitly instead of implicitly our in time stepping routines.


The extension of IMEX methods from convection-diffusion equations to problems involving the decomposition of parabolic problems across multiple domains was proposed by Dawson, et. al. \cite{DaDuDu-MathComp1991,DaDu-MathComp1992}.  The authors utilized an overlapping domain decomposition where the overlapping portion was updated using explicit time stepping.  The non-overlapping portions of the domain decomposition were then updated utilizing implicit time stepping.  IMEX decoupling procedures using non-overlapping domain decompositions for linearly coupled parabolic problems was investigated by Connors et. al. \cite{CoHoLa-SIAM2009}. This work was later extended to nonlinear coupled Navier-Stokes equations for fluid-fluid interaction \cite{CoHoLa-SIAM2012}.  We adopt similar time stepping routines to \cite{CoHoLa-SIAM2009} as well as techniques developed for the simulation of two-phase flow \cite{ShCa-SPE1959,StGa-SPE1961,Coats-SPE2000,Coats-SPE2003,CHHULI-TransPorusMed2004} to the simulation of semiconductor-electrolyte interfaces.  In Chapter \ref{ch:NumApprox} we will further review these methods and introduce new improvements for our application problem.


\section{Summary of Contributions}

This dissertation discusses the modeling and simulation of the transport and the transfer of charges across a semiconductor-electrolyte interface.  Software is developed in one dimension using the Eigen numerical linear algebra library and in two dimensions using the deal.II library.  Both codes are designed for easy of use and utilize shared-memory paralellization to speed-up computations.  Analysis of the underlying numerical approximations is performed and both codes are verified using a test-suite.  Specifically in this dissertation we will discuss the following contributions:


\begin{itemize}
\item Derivation and discussion of the mathematical modeling of charge transport and transfer in photoelectrochemcial solar cells.  The relationship between the macroscopic model used for computation and the Geirscher model of electron transfer in terms of energy levels is fully explained.  


\item Development of local discontinuous Galerkin methods and mixed finite element methods for the spatial discritization of drift-diffusion-Poisson systems which are coupled through a reactive interface.


\item Development and analysis of specifically tailored implicit-explicit-Schwarz time stepping routines to alleviate the stiffness of the model system. We remark that the utilization of non-iterative techniques was necessitated in order to guarantee existence and uniqueness of solutions.


\item Theoretical analysis of the local discontinuous Galerkin (LDG) method applied to transport problems with reactive interfaces. Specifically, we
introduce the primal form of the LDG method and prove its consistency as well as the coercivity and continuity of its bilinear forms.  We then use these properties to prove semidiscrete error estimates for the LDG method in primal form. 

\item We perform numerical experiments to discover the dependence of solar cell performance on device parameters and develop at strategy to maximize power output for nanoscale devices. 
\end{itemize}


\section{Outline of Dissertation}


The rest of this dissertation is as follows.  A review and discussion of the mathematical model of 
photoelectrochemical solar cells is provided in Chapter \ref{ch:Model}. In Chapter \ref{ch:NumApprox} 
we introduce the numerical methods and code base used to simulate the dynamics of the reactive semiconductor-electrolyte interface in photoelectrochemical solar cells.  In Chapter \ref{ch:Analysis} we analyze the local discontinuous Galerkin (LDG) approximations to the drift-diffusion equations
and prove semidiscrete error estimates. In Chapter \ref{ch:Results} we conduct studies on the convergence of our numerical approximations and discuss the performance of our algorithms. Additionally, we present results of some numerical experiments to analyze the performance of PECs under various scenarios.  Finally, in Chapter \ref{ch:Future} we review the completed work and offer some directions for future research projects.
%
%\bibliographystyle{ieeetr}
%\bibliography{../Harmon-Bib}


%
%\end{document}
