#!/usr/bin/env/python

import matplotlib.pyplot as plt
import numpy as np

f = open('LDG_Errors.dat')
datafile = f.readlines()
f.close()

N = len(datafile)
h = np.zeros(N)
eu1 = np.zeros(N)
eu2 = np.zeros(N)
eq1 = np.zeros(N)
eq2 = np.zeros(N)
exact_1 = np.zeros(N)
exact_2 = np.zeros(N)


for i in range(0,N):
	h[i] = float(datafile[i].split()[0])
	eu1[i] = float(datafile[i].split()[3])
	eq1[i] = float(datafile[i].split()[4])
	eu2[i] = float(datafile[i].split()[5])
	eq2[i] = float(datafile[i].split()[6])
	exact_1[i] = 2.5 * h[i] * h[i]
	exact_2[i] = h[i] * h[i] * h[i]


slope_1, intersept = np.polyfit(np.log(h),np.log(eu1),1)
print slope_1
slope_2, intersept = np.polyfit(np.log(h),np.log(eq1),1)
print slope_2
slope_3, intersept = np.polyfit(np.log(h),np.log(eu2),1)
print slope_3
slope_4, intersept = np.polyfit(np.log(h),np.log(eq2),1)
print slope_4

msz=15
tsz=25
plt.figure(figsize=(10,9), linewidth=3)
plt.clf()
plt.loglog(h, eu1, 'b^-',label="r$ u - u_{h}, \, k=1$",markersize=msz,linewidth=2)
plt.loglog(h, eu2, 'bs-',label="r$ u- u_{h}, \, k=2$",markersize=msz,linewidth=2)
plt.loglog(h, eq1, 'ro-', label="r$ q- q_{h}, \, k=1$",markersize=msz,linewidth=2)
plt.loglog(h, eq2, 'rv-', label="r$ q- q_{h}, \, k=2$",markersize=msz,linewidth=2)
plt.loglog(h, exact_1, 'k-', label='slope = 2', linewidth=2) 
plt.loglog(h, exact_2, 'k--',  label='slope = 3', linewidth=2) 
plt.ylabel(r"$\log(\Vert \cdot \Vert_{L^{2}})$", fontsize=tsz)
plt.yticks(fontsize=15)
plt.xlabel(r"$ \log(h)$", fontsize=tsz)
plt.xticks(fontsize=15)
plt.legend(('$ u- u_{h}, \, k=1$', '$ u- u_{h}, \, k=2$','$ q- q_{h}, \, k=1$', '$ q- q_{h}, \,k=2$', 'slope = 2','slope = 3'),loc=2, fontsize=tsz)
plt.title('Convergence Plots LDG Method', fontsize=tsz)
plt.ylim([1e-10,1e4])

plt.show()
