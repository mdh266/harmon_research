#!/usr/bin/env/python



import matplotlib.pyplot as plt
import numpy as np
import math


f = open('Mixed_Errors.dat')
datafile = f.readlines()
f.close()

N = len(datafile)
h = np.zeros(N)
eu1 = np.zeros(N)
eu2 = np.zeros(N)
eq1 = np.zeros(N)
eq2 = np.zeros(N)
exact_1 = np.zeros(N)
exact_2 = np.zeros(N)
exact_3 = np.zeros(N)


for i in range(0,N):
	h[i] = float(datafile[i].split()[0])
	eu1[i] = float(datafile[i].split()[3])
	eq1[i] = float(datafile[i].split()[4])
	eu2[i] = float(datafile[i].split()[5])
	eq2[i] = float(datafile[i].split()[6])
	exact_1[i] = 2e-1 * pow(h[i],2)
	exact_2[i] = 1e-3 * pow(h[i],3) 
	exact_3[i] = 1e-4 * pow(h[i],4)

slope_1, intersept = np.polyfit(np.log(h),np.log(eu1),1)
print slope_1
slope_2, intersept = np.polyfit(np.log(h),np.log(eq1),1)
print slope_2
slope_3, intersept = np.polyfit(np.log(h),np.log(eu2),1)
print slope_3
slope_4, intersept = np.polyfit(np.log(h),np.log(eq2),1)
print slope_4

msz = 15
tsz = 25
plt.figure(figsize=(10,9), linewidth=3)
plt.clf()
plt.loglog(h, eu1, 'b^-',label="r$ \Phi - \Phi_{h}, \, k=1$",markersize=msz,linewidth=2)
plt.loglog(h, eu2, 'bs-',label="r$ \Phi- \Phi_{h}, \, k=2$",markersize=msz,linewidth=2)
plt.loglog(h, eq1, 'ro-', label=r'$\nabla \Phi - \nabla \Phi_{h}, \, k=1$',markersize=msz, linewidth=2)
plt.loglog(h, eq2, 'rv-', label=r'$\nabla \Phi - \nabla \Phi_{h}, \, k=2$',markersize=msz,linewidth=2)
plt.loglog(h, exact_1, 'k-', label='slope = 2', linewidth=2) 
plt.plot(h, exact_2, 'k--',  label='slope = 3', linewidth=2) 
plt.plot(h, exact_3, 'k:',  label='slope = 4', linewidth=2) 
plt.ylabel(r"$\log(\Vert \cdot  \Vert_{L^{2}})$", fontsize=tsz)
plt.yticks(fontsize=15)
plt.xlabel(r"$ \log(h)$", fontsize=tsz)
plt.xticks(fontsize=15)
plt.legend(('$ \Phi - \Phi_{h}, \, k=1$', '$ \Phi - \Phi_{h}, \, k=2$',r'$\nabla \Phi - \nabla \Phi_{h}, \, k=1$', r'$ \nabla \Phi - \nabla \Phi_{h}, \,k=2$', 'slope = 2','slope = 3', 'slope = 4'),loc=2, fontsize=20)
plt.title('Convergence Plots Mixed Method', fontsize=tsz)
plt.ylim([10-15,5e3])
plt.show()
