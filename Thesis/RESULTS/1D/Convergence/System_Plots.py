#!/usr/bin/env/python

import matplotlib.pyplot as plt
import numpy as np

f = open('System_errors.dat')
datafile = f.readlines()
f.close()

N = len(datafile)
h = np.zeros(N)
eu = np.zeros(N)
eq = np.zeros(N)
exact_1 = np.zeros(N)
exact_2 = np.zeros(N)


for i in range(0,N):
	h[i] = float(datafile[i].split()[0])
	eu[i] = float(datafile[i].split()[1])
	eq[i] = float(datafile[i].split()[2])
	exact_1[i] = 3.5 * h[i] * h[i]
	exact_2[i] = 1e1 * h[i] * h[i] * h[i]

slope_1, intersept = np.polyfit(np.log(h),np.log(eu),1)
print slope_1
slope_2, intersept = np.polyfit(np.log(h),np.log(eq),1)
print slope_2
msz = 15
plt.figure(figsize=(10,9), linewidth=3)
plt.clf()
plt.loglog(h, eu, 'bo-',label='k = 1',markersize=msz,linewidth=2)
plt.loglog(h, eq, 'r-^', label='k = 2',markersize=msz,linewidth=2)
plt.loglog(h, exact_1, 'k-', label='slope = 2', linewidth=2) 
plt.loglog(h, exact_2, 'k--',  label='slope = 3', linewidth=2) 
plt.ylabel(r"$ \log(\Vert \rho_{n} - \rho_{n,h} \Vert_{L^{2}})$", fontsize=25)
plt.yticks(fontsize=15)
plt.xlabel(r"$ \log(h)$", fontsize=25)
plt.xticks(fontsize=15)
plt.legend(('k = 1','k = 2', 'slope = 2', 'slope = 3'),loc=2, fontsize=20)
plt.title('Convergence Plots For Full System', fontsize=25)
plt.ylim([1e-6,1e-1])
plt.show()
