#!/usr/bin/env/python

import matplotlib.pyplot as plt
import numpy as np
import math

f = open('Interface_LDG_Deg_1.dat')
datafile = f.readlines()
f.close()
#f = open('IMEX_No_Neumann_Deg_2.dat')
#datafile2 = f.readlines()
#f.close()

N = len(datafile) - 1
h = np.zeros(N)
eu1 = np.zeros(N)
eu2 = np.zeros(N)
eq1 = np.zeros(N)
eq2 = np.zeros(N)
exact_1 = np.zeros(N)
exact_2 = np.zeros(N)
exact_3 = np.zeros(N)


for i in range(0,N):
	h[i] = float(datafile[i+1].split()[0])
	eu1[i] = float(datafile[i+1].split()[3])
	eq1[i] = float(datafile[i+1].split()[5])
	eu2[i] = float(datafile[i+1].split()[7])
	eq2[i] = float(datafile[i+1].split()[9])
	exact_1[i] = 1.5e0 * pow(h[i],1)
	exact_2[i] = 2e-1 * pow(h[i],2) 
	exact_3[i] = 3e-1 * pow(h[i],3)

slope_1, intersept = np.polyfit(np.log(h),np.log(eu1),1)
print slope_1
slope_2, intersept = np.polyfit(np.log(h),np.log(eq1),1)
print slope_2
#slope_3, intersept = np.polyfit(np.log(h),np.log(eu2),1)
#print slope_3
#slope_4, intersept = np.polyfit(np.log(h),np.log(eq2),1)
#print slope_4

msz=15
plt.figure(figsize=(10,9), linewidth=3)
plt.clf()
plt.loglog(h, eu1, 'b^-',label="r$ u - u_{h}, \, k=1$",markersize=msz,linewidth=2)
plt.loglog(h, eu2, 'bs-',label="r$ v- v_{h}, \, k=1$",markersize=msz,linewidth=2)
plt.loglog(h, eq1, 'rv-', label=r'$\nabla u - \nabla u_{h}, \, k=1$', markersize=msz,linewidth=2)
plt.loglog(h, eq2, 'ro-', label=r'$\nabla v  - \nabla v_{h}, \, k=1$', markersize=msz,linewidth=2)
plt.loglog(h, exact_1, 'k-', label='slope = 2', linewidth=2) 
plt.plot(h, exact_2, 'k--',  label='slope = 3', linewidth=2) 
#plt.plot(h, exact_3, 'k:',  label='slope = 4', linewidth=2) 
plt.ylabel(r"$\log(\Vert \cdot  \Vert_{L^{2}})$", fontsize=25)
plt.yticks(fontsize=15)
plt.xlabel(r"$ \log(h)$", fontsize=25)
plt.xticks(fontsize=15)
plt.legend((r'$ u - u_{h}, \, k=1$', r'$\nabla u  - \nabla u_{h}, \, k=1$', r'$v - v_{h}, \, k=1$', r'$\nabla v  - \nabla v_{h}, \, k=1$','slope = 1','slope = 2'),loc=2, fontsize=25)
plt.title('Convergence Plots LDG-PU-IMEX', fontsize=25)
plt.ylim([2e-5,1.5e4])
plt.xlim([1e-2,1])
plt.show()



#, 'slope = 3'
